#include "ESP.h"
#include "CDrawManager.h"
#include "COffsets.h"
//===================================================================================
CRadar gRadar;

#define RADARSIZE 140
int iCenterX;
//===================================================================================
void CRadar::DrawRadarBlip( CBaseEntity *pBaseEntity, DWORD dwTeamColor )
{
	int iClass = gOffsets.iGetPlayerClass( pBaseEntity );
	try
	{
		CBaseEntity *pLocalEntity = GetBaseEntity(me);

		if (pLocalEntity == nullptr)
			return;

		if (gOffsets.iGetTeamNum(pBaseEntity) == gPlayerVars.iTeamNum )
			return;

		Vector vLocalOrigin = pLocalEntity->GetAbsOrigin();
		Vector vEnemyOrigin = pBaseEntity->GetAbsOrigin();

		float flDeltaX  = vEnemyOrigin.x - vLocalOrigin.x;
		float flDeltaY  = vEnemyOrigin.y - vLocalOrigin.y;

		Vector vecViewAngles;
		gInts.Engine->GetViewAngles(vecViewAngles);

		float flYaw = ( vecViewAngles.y ) * ( PI / 180.0 );
		float flMainViewAngles_CosYaw = cos( flYaw );
		float flMainViewAngles_SinYaw = sin( flYaw );

		float x =  flDeltaY * ( -flMainViewAngles_CosYaw ) + flDeltaX * flMainViewAngles_SinYaw;
		float y =  flDeltaX * ( -flMainViewAngles_CosYaw ) - flDeltaY * flMainViewAngles_SinYaw;

		float flRange = 2000;

		if( fabs( x ) > flRange || fabs( y ) > flRange )
		{ 
			if(y>x)
			{
				if(y>-x) 
				{
					x = flRange * x / y;
					y = flRange;
				}  
				else  
				{
					y = -flRange * y / x; 
					x = -flRange; 
				}
			} 
			else 
			{
				if( y > -x ) 
				{
					y = flRange * y / x; 
					x = flRange; 
				}
				else
				{
					x = -flRange * x / y;
					y = -flRange;
				}
			}
		}

		int	iScreenX = iCenterX + int( x / flRange * RADARSIZE );
		int iScreenY = RADARSIZE + int( y / flRange * RADARSIZE );
		int iLocalTeamNum = gOffsets.iGetTeamNum( pLocalEntity );
		int iPlayerCond = gOffsets.iGetPlayerCond( pBaseEntity );
		int iTeamNum = gOffsets.iGetTeamNum( pBaseEntity );

		const char* RADARBlip = "";
		DWORD dwColor = 0xFFFFFFEE;

		float underXDelta = 0, underYDelta = -2;
		float upperXDelta = 0, upperYDelta = -2;

		switch (iClass)
		{
			case TF2_Sniper:
			{
				RADARBlip = "8";
				dwColor = 0x32CD32FF;
				upperXDelta = -1;
				break;
			}
			case TF2_Spy:
			{
				RADARBlip = "S";
				if ( (iPlayerCond & TFCond_Cloaked) == 0 )
				{
					dwColor = 0x0026FFFF;
				}
				underYDelta = -1;
				upperYDelta = -3;
				break;
			}
			case TF2_Engineer:
			{
				RADARBlip = "E";
				dwColor = 0xE7B53BFF;
				break;
			}
			case TF2_Medic:
			{
				RADARBlip = "M";
				dwColor = 0x5885A2FF;
				underYDelta = -1;
				break;
			}
			case TF2_Demoman:
			{
				RADARBlip = "D";
				dwColor = 0xFF69B4FF;
				break;
			}
			case TF2_Pyro:
			{
				RADARBlip = "3";
				dwColor = 0xFF0000FF;
				upperXDelta = -1;
				underXDelta = -1;
				break;
			}
			case TF2_Heavy:
			{
				RADARBlip = "H";
				dwColor = 0xF0E68CFF;
				break;
			}
			case TF2_Scout:
			{
				RADARBlip = "1";
				dwColor = 0x729E42FF;
				break;
			}
			case TF2_Soldier:
			{
				RADARBlip = "2";
				dwColor = 0xCF7336FF;
				upperXDelta = -1;
				break;
			}
		}

		Vector vecLocalWorldspace, vecPlayerWorldspace;
		pLocalEntity->GetWorldSpaceCenter(vecLocalWorldspace);
		pBaseEntity->GetWorldSpaceCenter(vecPlayerWorldspace);
		Vector vecScreen;
		vecScreen.x = iScreenX;	
		vecScreen.y = iScreenY;
		if( vecLocalWorldspace.z > vecPlayerWorldspace.z + 80 )
		{
			gDrawManager->DrawString( iScreenX + underXDelta, iScreenY + underYDelta, 0xFFFFFFFF, L"_" );
		}
		else if( vecLocalWorldspace.z < vecPlayerWorldspace.z - 80 )
		{
			gDrawManager->DrawString( iScreenX + upperXDelta, iScreenY + upperYDelta, 0xFFFFFFFF, L"�" );
		}

		gDrawManager->DrawString( iScreenX, iScreenY - 2, dwColor, RADARBlip );
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed RADAR Call %i", iClass);
	}
}
//===================================================================================
void CRadar::DrawRadarBack( )
{
	try
	{
		iCenterX = RADARSIZE;

		if (gPlayerVars.iClass == TF2_Engineer)
		{
			iCenterX += 392;
		}

		gDrawManager->DrawRect(iCenterX - RADARSIZE, 0, 2 * RADARSIZE + 2, 2 * RADARSIZE + 2, 0x1E1E1E20);

		DWORD dwTeamColor;

		switch (gPlayerVars.iTeamNum)
		{
		case TEAM_RED:
		{
			dwTeamColor = 0xFF800050;
			break;
		}
		case TEAM_BLUE:
		{
			dwTeamColor = 0x0080FF50;
			break;
		}
		default:
		{
			dwTeamColor = 0xFFFFFF50;
		}
		}

		gDrawManager->DrawRect(iCenterX, RADARSIZE - (RADARSIZE - 10), 1, 2 * (RADARSIZE - 10), dwTeamColor);
		gDrawManager->DrawRect(iCenterX - (RADARSIZE - 10), RADARSIZE, 2 * (RADARSIZE - 10), 1, dwTeamColor);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed DrawRadarBack");
	}
}
//===================================================================================