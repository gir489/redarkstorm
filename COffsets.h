#pragma once
//===================================================================================
#include "SDK.h"

#define MakePtr( Type, dwBase, dwOffset ) ((Type*)((DWORD)dwBase + (DWORD)dwOffset))
//===================================================================================
class COffsets
{
public:
	void DumpAllNetvars()
	{
		ClientClass* pClass = gInts.Client->GetAllClasses();
		do
		{
			RecvTable* pRecvTable = pClass->Table;
			if (!pRecvTable)
				continue;

			gBaseAPI.LogToFile("Table '%s' found at: [0x%.8X]", pRecvTable->GetName(), (DWORD)pRecvTable);
			for (int i = 0; i < pRecvTable->GetNumProps(); i++)
			{
				RecvProp* pRecvProp = pRecvTable->GetProp(i);
				if (!pRecvProp)
					continue;

				int x = pRecvProp->GetOffset();
				if (x == 0)
				{
					gBaseAPI.LogToFile("\tProperty '%s' found at: [0x%.8X]", pRecvProp->GetName(), (DWORD)pRecvProp);
					if (strcmp(pRecvProp->GetName(), "baseclass"))
					{
						int tableOffset;
						RecvProp* p = pFindProp(pRecvTable, pRecvProp->GetName(), &tableOffset);
						RecvTable* subTable = p->GetDataTable();
						if (!subTable)
							continue;
						for (int i = 0; i < subTable->GetNumProps(); i++)
						{
							RecvProp* subProp = subTable->GetProp(i);
							if (!subProp)
								continue;
							gBaseAPI.LogToFile("\t\tProperty '%s' offset: [0x%.2X]", subProp->GetName(), subProp->GetOffset());
						}
					}
				}
				else
				{
					gBaseAPI.LogToFile("\tProperty '%s' offset: [0x%.2X]", pRecvProp->GetName(), pRecvProp->GetOffset());
				}
			}
			pClass = pClass->pNextClass;
		} while (pClass);
	}
private:
	RecvTable* pFindTable(const char* szTableName)
	{
		ClientClass* pClass = gInts.Client->GetAllClasses();
		do
		{
			RecvTable* pRecvTable = pClass->Table;
			if(! pRecvTable )
				continue;
			if(!strcmp( pRecvTable->GetName(), szTableName ))
			{
				#ifdef _DEBUG
				gBaseAPI.LogToFile( "Table '%s' found at: [0x%.8X]", szTableName, (DWORD)pRecvTable );
				#endif
				return pRecvTable;
			}
			pClass = pClass->pNextClass;
		}
		while ( pClass );
		gBaseAPI.LogToFile( "Table '%s' NOT found", szTableName );
		return NULL;
	}
	RecvProp* pFindProp(RecvTable* pRecvTable, const char* szPropertyName, int* x)
	{
		if(! pRecvTable) return NULL;
		for(int i = 0; i < pRecvTable->GetNumProps(); i++ )
		{
			RecvProp* pRecvProp = pRecvTable->GetProp(i);
			if(! pRecvProp )
				continue;
			if(!strcmp( pRecvProp->GetName(), szPropertyName ))
			{
				*x = pRecvProp->GetOffset();
				#ifdef _DEBUG
				if (*x == 0)
				{
					gBaseAPI.LogToFile( "Property '%s' found at: [0x%.8X]", szPropertyName , (DWORD)pRecvProp );
				}
				else
				{
					gBaseAPI.LogToFile( "Property '%s' offset: [0x%.2X]", szPropertyName , *x );
				}
				#endif
				return pRecvProp;
			}
		}
		gBaseAPI.LogToFile( "Property '%s' NOT found", szPropertyName );
		return NULL;
	}
	int iFindOffset(RecvTable* pRecvTable, const char* szPropertyName)
	{
		int a = NULL;
		pFindProp(pRecvTable,szPropertyName,&a);
		return a;
	}

	int offset_DT_TFPlayer__m_Shared,
		offset_DT_TFPlayer__m_PlayerClass,
		offset_DT_TFPlayer__m_nWaterLevel,
		offset_DT_TFPlayer__m_bIsABot,
		offset_DT_TFPlayer__m_hItem,
		offset_DT_TFPlayer__m_bGlowEnabled,
		offset_DT_TFPlayerShared__m_nPlayerCond,
		offset_DT_TFPlayerShared__m_nPlayerCondEx,
		offset_DT_TFPlayerShared__m_nPlayerCondEx2,
		offset_DT_TFPlayerShared__m_nDisguiseTeam,
		offset_DT_TFPlayerShared__m_bShieldEquipped,
		offset_DT_TFPlayerShared__m_iRevengeCrits,
		offset_DT_TFPlayerShared__m_nDisguiseClass,
		offset_DT_TFPlayerShared__m_bFeignDeathReady,
		//offset_DT_TFPlayerShared__m_iTauntIndex,
		//offset_DT_TFPlayerShared__m_nNumHealers,
		offset_DT_TFPlayerClassShared__m_iClass,
		offset_DT_LocalPlayerExclusive__m_vecViewOffset,
		offset_DT_LocalPlayerExclusive__m_iAmmo,
		offset_DT_LocalPlayerExclusive__m_nTickBase,
		offset_DT_LocalPlayerExclusive__m_vecVelocity,

		offset_DT_CaptureFlag__m_nFlagStatus,
		offset_DT_CaptureFlag__m_hPrevOwner,
		offset_DT_CaptureFlag__m_nType,
		offset_DT_CaptureFlag__m_flResetTime,

		offset_DT_BaseAttributableItem__m_AttributeManager,
		offset_DT_AttributeContainer__m_Item,
		offset_DT_ScriptCreatedItem__m_iItemDefinitionIndex,

		offset_DT_BaseObject__m_iHealth,
		offset_DT_BaseObject__m_iMaxHealth,
		offset_DT_BaseObject__m_bHasSapper,
		offset_DT_BaseObject__m_bBuilding,
		offset_DT_BaseObject__m_bMiniBuilding,
		offset_DT_BaseObject__m_flPercentageConstructed,
		offset_DT_BaseObject__m_bDisabled,
		offset_DT_BaseObject__m_iUpgradeLevel,
		offset_DT_BaseObject__m_iUpgradeMetal,
		offset_DT_BaseObject__m_iObjectMode,

		offset_DT_ObjectDispenser__m_iAmmoMetal,

		offset_DT_TFPlayerResource__m_iTotalScore,
		offset_DT_TFPlayerResource__m_iMaxHealth,
		offset_DT_TFPlayerResource__m_flNextRespawnTime,
		offset_DT_TFPlayerResource__m_iPlayerClass,

		offset_DT_WeaponMinigun__m_iWeaponState,

		offset_DT_ObjectSentrygun__m_iAmmoShells,
		offset_DT_ObjectSentrygun__m_iAmmoRockets,
		offset_DT_ObjectSentrygun__m_iState,
		offset_DT_ObjectSentrygun__m_bPlayerControlled,
		offset_DT_ObjectSentrygun__SentrygunLocalData,
		offset_DT_SentrygunLocalData__m_iKills,
		offset_DT_SentrygunLocalData__m_iAssists,

		offset_DT_ObjectTeleporter__m_iState,

		offset_DT_TeamplayRoundBasedRules__m_iRoundState,
		//offset_DT_TFGameRulesProxy__m_bPlayingMannVsMachine,

		offset_DT_NonLocalTFWeaponMedigunData__m_flChargeLevel,
		offset_DT_WeaponMedigun__m_bHealing,
		
		offset_DT_BaseCombatCharacter__m_hActiveWeapon,
		offset_DT_BaseCombatCharacter__m_hMyWeapons,

		offset_DT_TFWeaponKnife__m_bReadyToBackstab,
		
		offset_DT_BaseEntity__m_iTeamNum,
		offset_DT_BaseEntity__m_flSimulationTime,
		offset_DT_BaseEntity__m_hOwnerEntity,

		offset_DT_BasePlayer__m_lifeState,
		offset_DT_BasePlayer__m_fFlags,
		offset_DT_BasePlayer__m_iHealth,

		offset_DT_BaseGrenade__m_hThrower,
		offset_DT_TFProjectile_Pipebomb__m_bDefensiveBomb,
		offset_DT_TFProjectile_Pipebomb__m_bTouched,
		offset_DT_TFProjectile_Pipebomb__m_iType,
		
		offset_DT_PipebombLauncherLocalData__m_iPipebombCount,
		offset_DT_PipebombLauncherLocalData__m_flChargeBeginTime,
		
		offset_DT_TFProjectile_Arrow__m_bCritical,
		offset_DT_TFProjectile_Arrow__m_bArrowAlight,
		offset_DT_TFProjectile_EnergyBall__m_bChargedShot,
		offset_DT_TFProjectile_Flare__m_bCritical,
		offset_DT_TFProjectile_Rocket__m_bCritical,
		offset_DT_TFWeaponBaseGrenadeProj__m_bCritical,

		offfset_DT_TFBaseRocket__m_hLauncher,
		
		offset_DT_LocalWeaponData__m_iClip1,
		offset_DT_LocalWeaponData__m_iPrimaryAmmoType,
		offset_DT_LocalWeaponData__m_flNextPrimaryAttack,
		offset_DT_LocalWeaponData__m_flReloadPriorNextFire,
		offset_DT_LocalWeaponData__m_flLastFireTime;

public:

	void Initialize()
	{
		#ifdef _DEBUG
		try
		{
			//gBaseAPI.LogToFile( /*---[BEGIN INIT OFFSETS]---*/XorStr<0xDB,27,0xF4CE7279>("\xF6\xF1\xF0\x85\x9D\xA5\xA6\xAB\xAD\xC4\xAC\xA8\xAE\xBC\xC9\xA5\xAD\xAA\xBE\xAB\xBB\xA3\xAC\xDF\xDE\xD9"+0xF4CE7279).s );
			#ifdef NETVAR
			DumpAllNetvars();
			#endif
		#endif

		RecvTable* tbl = NULL;
		RecvProp* p = NULL;

		tbl = pFindTable( "DT_BasePlayer" );
		offset_DT_BasePlayer__m_lifeState = iFindOffset( tbl, "m_lifeState");
		offset_DT_BasePlayer__m_fFlags = iFindOffset(tbl, "m_fFlags");
		offset_DT_BasePlayer__m_iHealth = iFindOffset( tbl, "m_iHealth");
		p = pFindProp(tbl, "localdata", &offset_DT_LocalPlayerExclusive__m_vecViewOffset );
		offset_DT_LocalPlayerExclusive__m_vecViewOffset = iFindOffset( p->GetDataTable(), "m_vecViewOffset[0]");
		offset_DT_LocalPlayerExclusive__m_nTickBase = iFindOffset( p->GetDataTable(), "m_nTickBase" );
		offset_DT_LocalPlayerExclusive__m_iAmmo = iFindOffset(p->GetDataTable(), "m_iAmmo");
		offset_DT_LocalPlayerExclusive__m_vecVelocity = iFindOffset( p->GetDataTable(), "m_vecVelocity[0]" );

		tbl = pFindTable("DT_BaseEntity");
		offset_DT_BaseEntity__m_iTeamNum = iFindOffset( tbl, "m_iTeamNum");
		offset_DT_BaseEntity__m_flSimulationTime = iFindOffset( tbl, "m_flSimulationTime");
		offset_DT_BaseEntity__m_hOwnerEntity = iFindOffset(tbl, "m_hOwnerEntity");

		tbl = pFindTable("DT_TFWeaponKnife");
		offset_DT_TFWeaponKnife__m_bReadyToBackstab = iFindOffset( tbl, "m_bReadyToBackstab");

		tbl = pFindTable("DT_WeaponPipebombLauncher");
		p = pFindProp( tbl, "PipebombLauncherLocalData", &offset_DT_PipebombLauncherLocalData__m_iPipebombCount);
		offset_DT_PipebombLauncherLocalData__m_flChargeBeginTime = iFindOffset( p->GetDataTable(), "m_flChargeBeginTime" );
		offset_DT_PipebombLauncherLocalData__m_iPipebombCount = iFindOffset( p->GetDataTable(), "m_iPipebombCount");

		tbl = pFindTable("DT_TFBaseRocket");
		offfset_DT_TFBaseRocket__m_hLauncher = iFindOffset(tbl, "m_hLauncher");

		tbl = pFindTable( "DT_BaseCombatWeapon");
		p = pFindProp(tbl, "LocalWeaponData", &offset_DT_LocalWeaponData__m_iPrimaryAmmoType );
		offset_DT_LocalWeaponData__m_iClip1 = iFindOffset(p->GetDataTable(), "m_iClip1");
		offset_DT_LocalWeaponData__m_iPrimaryAmmoType = iFindOffset(p->GetDataTable(), "m_iPrimaryAmmoType");
		p = pFindProp(tbl, "LocalActiveWeaponData", &offset_DT_LocalWeaponData__m_flNextPrimaryAttack );
		offset_DT_LocalWeaponData__m_flNextPrimaryAttack = iFindOffset(p->GetDataTable(), "m_flNextPrimaryAttack");
		tbl = pFindTable("DT_TFWeaponBase");
		p = pFindProp(tbl, "LocalActiveTFWeaponData", &offset_DT_LocalWeaponData__m_flLastFireTime );
		offset_DT_LocalWeaponData__m_flReloadPriorNextFire = iFindOffset(p->GetDataTable(), "m_flReloadPriorNextFire");
		offset_DT_LocalWeaponData__m_flLastFireTime = iFindOffset(p->GetDataTable(), "m_flLastFireTime");
		
		tbl = pFindTable("DT_TFProjectile_Arrow");
		offset_DT_TFProjectile_Arrow__m_bCritical = iFindOffset(tbl, "m_bCritical");
		offset_DT_TFProjectile_Arrow__m_bArrowAlight = iFindOffset(tbl, "m_bArrowAlight");

		tbl = pFindTable("DT_TFProjectile_EnergyBall");
		offset_DT_TFProjectile_EnergyBall__m_bChargedShot = iFindOffset(tbl, "m_bChargedShot");

		tbl = pFindTable("DT_TFProjectile_Flare");
		offset_DT_TFProjectile_Flare__m_bCritical = iFindOffset(tbl, "m_bCritical");

		tbl = pFindTable("DT_TFProjectile_Rocket");
		offset_DT_TFProjectile_Rocket__m_bCritical = iFindOffset(tbl, "m_bCritical");

		tbl = pFindTable("DT_TFWeaponBaseGrenadeProj");
		offset_DT_TFWeaponBaseGrenadeProj__m_bCritical = iFindOffset(tbl, "m_bCritical");
		
		tbl = pFindTable("DT_BaseCombatCharacter");
		offset_DT_BaseCombatCharacter__m_hActiveWeapon = iFindOffset( tbl, "m_hActiveWeapon");
		offset_DT_BaseCombatCharacter__m_hMyWeapons= iFindOffset( tbl, "m_hMyWeapons");

		tbl = pFindTable("DT_ObjectTeleporter");
		offset_DT_ObjectTeleporter__m_iState = iFindOffset( tbl, "m_iState");

		tbl = pFindTable("DT_ObjectSentrygun");
		offset_DT_ObjectSentrygun__m_iAmmoShells = iFindOffset( tbl, "m_iAmmoShells");
		offset_DT_ObjectSentrygun__m_iAmmoRockets = iFindOffset( tbl, "m_iAmmoRockets");
		offset_DT_ObjectSentrygun__m_iState = iFindOffset( tbl, "m_iState");
		offset_DT_ObjectSentrygun__m_bPlayerControlled = iFindOffset( tbl, "m_bPlayerControlled");
		p = pFindProp( tbl, "SentrygunLocalData", &offset_DT_ObjectSentrygun__SentrygunLocalData);
		offset_DT_SentrygunLocalData__m_iKills = iFindOffset( p->GetDataTable(), "m_iKills");
		offset_DT_SentrygunLocalData__m_iAssists = iFindOffset( p->GetDataTable(), "m_iAssists" );

		tbl = pFindTable("DT_ObjectDispenser");
		offset_DT_ObjectDispenser__m_iAmmoMetal = iFindOffset( tbl, "m_iAmmoMetal");

		tbl = pFindTable("DT_TFPlayerResource");
		offset_DT_TFPlayerResource__m_iTotalScore = iFindOffset(tbl, "m_iTotalScore");
		offset_DT_TFPlayerResource__m_iMaxHealth = iFindOffset(tbl, "m_iMaxHealth");
		offset_DT_TFPlayerResource__m_flNextRespawnTime = iFindOffset(tbl, "m_flNextRespawnTime");
		offset_DT_TFPlayerResource__m_iPlayerClass = iFindOffset(tbl, "m_iPlayerClass");

		tbl = pFindTable("DT_BaseGrenade");
		offset_DT_BaseGrenade__m_hThrower = iFindOffset( tbl, "m_hThrower");

		tbl = pFindTable("DT_WeaponMedigun");
		offset_DT_WeaponMedigun__m_bHealing = iFindOffset(tbl, "m_bHealing");
		p = pFindProp( tbl, "NonLocalTFWeaponMedigunData", &offset_DT_NonLocalTFWeaponMedigunData__m_flChargeLevel);
		offset_DT_NonLocalTFWeaponMedigunData__m_flChargeLevel = iFindOffset( p->GetDataTable(), "m_flChargeLevel");

		tbl = pFindTable("DT_TFProjectile_Pipebomb");
		offset_DT_TFProjectile_Pipebomb__m_bDefensiveBomb = iFindOffset(tbl, "m_bDefensiveBomb");
		offset_DT_TFProjectile_Pipebomb__m_bTouched = iFindOffset(tbl, "m_bTouched");
		offset_DT_TFProjectile_Pipebomb__m_iType = iFindOffset(tbl, "m_iType");

		tbl = pFindTable("DT_BaseObject");
		offset_DT_BaseObject__m_iHealth = iFindOffset( tbl, "m_iHealth");
		offset_DT_BaseObject__m_iMaxHealth = iFindOffset( tbl, "m_iMaxHealth");
		offset_DT_BaseObject__m_bHasSapper = iFindOffset( tbl, "m_bHasSapper");
		offset_DT_BaseObject__m_bBuilding = iFindOffset( tbl, "m_bBuilding");
		offset_DT_BaseObject__m_bDisabled = iFindOffset( tbl, "m_bDisabled");
		offset_DT_BaseObject__m_flPercentageConstructed = iFindOffset( tbl, "m_flPercentageConstructed");
		offset_DT_BaseObject__m_bMiniBuilding = iFindOffset( tbl, "m_bMiniBuilding");
		offset_DT_BaseObject__m_iUpgradeLevel = iFindOffset( tbl, "m_iUpgradeLevel");
		offset_DT_BaseObject__m_iUpgradeMetal = iFindOffset( tbl, "m_iUpgradeMetal");
		offset_DT_BaseObject__m_iObjectMode = iFindOffset( tbl, "m_iObjectMode" );

		tbl = pFindTable("DT_TeamplayRoundBasedRulesProxy");
		p = pFindProp( tbl, "teamplayroundbased_gamerules_data", &offset_DT_TeamplayRoundBasedRules__m_iRoundState );
		tbl = p->GetDataTable();
		offset_DT_TeamplayRoundBasedRules__m_iRoundState = iFindOffset( tbl, "m_iRoundState");

		//tbl = pFindTable("DT_TFGameRulesProxy");
		//p = pFindProp( tbl, "tf_gamerules_data", &offset_DT_TFGameRulesProxy__m_bPlayingMannVsMachine );
		//tbl = p->GetDataTable();
		//offset_DT_TFGameRulesProxy__m_bPlayingMannVsMachine = iFindOffset( tbl, "m_bPlayingMannVsMachine");

		tbl = pFindTable("DT_TFPlayer");
		offset_DT_TFPlayer__m_nWaterLevel = iFindOffset( tbl, "m_nWaterLevel");
		offset_DT_TFPlayer__m_bIsABot = iFindOffset( tbl, "m_bIsABot");
		offset_DT_TFPlayer__m_hItem = iFindOffset( tbl, "m_hItem" );
		offset_DT_TFPlayer__m_bGlowEnabled = iFindOffset( tbl, "m_bGlowEnabled");
		p = pFindProp( tbl, "m_PlayerClass", &offset_DT_TFPlayer__m_PlayerClass);
		offset_DT_TFPlayerClassShared__m_iClass = iFindOffset( p->GetDataTable(), "m_iClass");

		p = pFindProp( tbl, "m_Shared", &offset_DT_TFPlayer__m_Shared);
		tbl = p->GetDataTable();
		offset_DT_TFPlayerShared__m_nPlayerCond = iFindOffset( tbl, "m_nPlayerCond");
		offset_DT_TFPlayerShared__m_nPlayerCondEx = iFindOffset( tbl, "m_nPlayerCondEx" );
		offset_DT_TFPlayerShared__m_nPlayerCondEx2 = iFindOffset( tbl, "m_nPlayerCondEx2" );
		offset_DT_TFPlayerShared__m_nDisguiseTeam = iFindOffset( tbl, "m_nDisguiseTeam");
		offset_DT_TFPlayerShared__m_bShieldEquipped = iFindOffset( tbl, "m_bShieldEquipped");
		offset_DT_TFPlayerShared__m_iRevengeCrits = iFindOffset( tbl, "m_iRevengeCrits");
		offset_DT_TFPlayerShared__m_nDisguiseClass = iFindOffset( tbl, "m_nDisguiseClass");
		offset_DT_TFPlayerShared__m_bFeignDeathReady = iFindOffset( tbl, "m_bFeignDeathReady");
		//offset_DT_TFPlayerShared__m_iTauntIndex = iFindOffset( tbl, "m_iTauntIndex");
		//offset_DT_TFPlayerShared__m_nNumHealers = iFindOffset( tbl, "m_nNumHealers");

		extern void new_BaseViewModel_flPlaybackRate( const CRecvProxyData *pData, void *pStruct, void *pOut );
		tbl = pFindTable("DT_BaseViewModel");
		p = pFindProp(tbl, "m_flPlaybackRate", &offset_DT_BaseAttributableItem__m_AttributeManager);
		p->SetProxyFn( new_BaseViewModel_flPlaybackRate );

		tbl = pFindTable("DT_WeaponMinigun");
		offset_DT_WeaponMinigun__m_iWeaponState = iFindOffset( tbl, "m_iWeaponState");		

		tbl = pFindTable("DT_CaptureFlag");
		offset_DT_CaptureFlag__m_nType = iFindOffset( tbl, "m_nType");
		offset_DT_CaptureFlag__m_nFlagStatus = iFindOffset( tbl, "m_nFlagStatus");
		offset_DT_CaptureFlag__m_hPrevOwner = iFindOffset( tbl, "m_hPrevOwner");
		offset_DT_CaptureFlag__m_flResetTime = iFindOffset( tbl, "m_flResetTime");

		tbl = pFindTable("DT_EconEntity");
		p = pFindProp( tbl, "m_AttributeManager", &offset_DT_BaseAttributableItem__m_AttributeManager);
		p = pFindProp( p->GetDataTable(), "m_Item", &offset_DT_AttributeContainer__m_Item);
		offset_DT_ScriptCreatedItem__m_iItemDefinitionIndex = iFindOffset(p->GetDataTable(), "m_iItemDefinitionIndex");
		
		#ifdef _DEBUG
		//gBaseAPI.LogToFile( /*---[END INIT OFFSETS]---*/XorStr<0x98,25,0x029AAE28>("\xB5\xB4\xB7\xC0\xD9\xD3\xDA\xBF\xE9\xEF\xEB\xF7\x84\xEA\xE0\xE1\xFB\xEC\xFE\xF8\xF1\x80\x83\x82"+0x029AAE28).s );
		}
		catch(...)
		{
			gBaseAPI.LogToFile("Failed NetVars");
		}
		#endif
	}
	inline void RemovePlayerCondition( CBaseEntity* pBaseEntity, int iCondition )
	{
		*MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nPlayerCond) &= ~iCondition;
	}
	inline void AddPlayerCondition( CBaseEntity* pBaseEntity, int iCondition )
	{
		*MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nPlayerCond) |= iCondition;
	}
	inline bool bGetKnifeReadyToBackstab(CBaseEntity *pBaseCombatWeapon)
	{
		return *MakePtr(bool, pBaseCombatWeapon, offset_DT_TFWeaponKnife__m_bReadyToBackstab);
	}
	inline CBaseEntity* pGetBaseCombatWeapon ( CBaseEntity* pEntity, int index )
	{
		int hMyWeapon = *MakePtr(int, pEntity, offset_DT_BaseCombatCharacter__m_hMyWeapons + index*4 );
		return (CBaseEntity *) (gInts.EntList->GetClientEntityFromHandle( hMyWeapon ));
	}
	inline CBaseEntity* pGetBaseCombatActiveWeapon ( CBaseEntity* pEntity )
	{
		int hActiveWeapon = *MakePtr(int, pEntity, offset_DT_BaseCombatCharacter__m_hActiveWeapon );
		return (CBaseEntity *) (gInts.EntList->GetClientEntityFromHandle( hActiveWeapon ));
	}
	inline float flGetChargeTime(CBaseEntity* pBaseCombatWeapon)
	{
		float flCharge = *MakePtr(float, pBaseCombatWeapon, offset_DT_PipebombLauncherLocalData__m_flChargeBeginTime);
		//if (flCharge > 0.0f ) return g_pGlobals->curtime - flCharge;
		return 0.0f;
	}
	inline float flGetChargeBeginTime(CBaseEntity* pBaseCombatWeapon)
	{
		return *MakePtr(float, pBaseCombatWeapon, offset_DT_PipebombLauncherLocalData__m_flChargeBeginTime);
	}
	inline int iGetCurrentClipAmmo(CBaseEntity* pBaseCombatWeapon)
	{
		return *MakePtr(int, pBaseCombatWeapon, offset_DT_LocalWeaponData__m_iClip1 );
	}
	inline float flGetLastFireTime(CBaseEntity* pBaseWeapon)
	{
		return *MakePtr(float, pBaseWeapon, offset_DT_LocalWeaponData__m_flLastFireTime );
	}
	inline float flGetReloadPriorNextFire(CBaseEntity* pBaseWeapon)
	{
		return *MakePtr(float, pBaseWeapon, offset_DT_LocalWeaponData__m_flReloadPriorNextFire );
	}
	inline float flGetNextPrimaryAttack(CBaseEntity* pBaseWeapon)
	{
		return *MakePtr(float, pBaseWeapon, offset_DT_LocalWeaponData__m_flNextPrimaryAttack );
	}
	/*inline int COffsets::iGetTauntIndex(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_iTauntIndex );
	}*/
	/*inline int COffsets::iGetNumHealers(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nNumHealers );
	}*/
	inline int iGetPlayerCond(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nPlayerCond);
	}
	inline int iGetPlayerCondEx(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nPlayerCondEx);
	}
	inline int iGetPlayerCondEx2(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nPlayerCondEx2);
	}
	inline bool bIsHoldingDeadRinger (CBaseEntity* pBaseEntity)
	{
		return (*MakePtr(bool, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_bFeignDeathReady) || (iGetPlayerCond(pBaseEntity) & TFCond_DeadRingered ) );
	}
	inline int iGetPlayerClass(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_PlayerClass + offset_DT_TFPlayerClassShared__m_iClass);
	}
	inline int iGetPlayerDisguiseClass(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nDisguiseClass);
	}
	inline int iGetRevengeCrits(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_iRevengeCrits);
	}
	inline int iGetPlayerDisgisieTeam(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_nDisguiseTeam);
	}
	inline int iGetTeleporterState(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectTeleporter__m_iState);
	}
	inline bool bIsScottishResistanceBomb(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Pipebomb__m_bDefensiveBomb);
	}
	inline int iGetFlagType(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_CaptureFlag__m_nType);
	}
	inline bool bIsBotPlayer(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFPlayer__m_bIsABot);
	}
	inline int iGetFlags(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BasePlayer__m_fFlags);
	}
	inline void SetFlags(CBaseEntity* pBaseEntity, int flags)
	{
		*MakePtr(int, pBaseEntity, offset_DT_BasePlayer__m_fFlags) = flags;
	}
	inline int iGetHealth(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BasePlayer__m_iHealth);
	}
	inline int iGetWaterLevel(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_nWaterLevel);
	}
	inline bool bGetIsAlive(CBaseEntity *pBaseEntity)
	{
		return LIFE_ALIVE == *MakePtr(BYTE, pBaseEntity, offset_DT_BasePlayer__m_lifeState);
	}
	inline int iGetTeamNum(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseEntity__m_iTeamNum);
	}
	inline bool bIsTouched(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Pipebomb__m_bTouched);
	}
	inline bool bIsArrowCrit(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Arrow__m_bCritical);
	}
	inline bool bIsArrowAlight(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Arrow__m_bArrowAlight);
	}
	inline int iGetRoundState( PDWORD dwGameRules )
	{
		return *MakePtr(int, *dwGameRules, offset_DT_TeamplayRoundBasedRules__m_iRoundState );
	}
	inline bool bHasObject( CBaseEntity* pBaseEntity )
	{
		return (*MakePtr(int, pBaseEntity, offset_DT_TFPlayer__m_hItem ) != -1 );
	}
	inline Vector GetEyePosition(CBaseEntity *pBaseEntity)
	{
		Vector vecViewOffset = *( Vector* )( ( DWORD )pBaseEntity + offset_DT_LocalPlayerExclusive__m_vecViewOffset );
		return pBaseEntity->GetAbsOrigin() + vecViewOffset;
	}
	/*inline Vector COffsets::GetPunch(CBaseEntity *pBaseEntity)
	{
		return *( Vector* )( ( DWORD )pBaseEntity + 0xDE0 );
	}*/
	inline bool bIsEnergyCharged(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_EnergyBall__m_bChargedShot);
	}
	inline bool bIsFlareCrit(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Flare__m_bCritical);
	}
	inline bool bIsGrenadeCrit(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFWeaponBaseGrenadeProj__m_bCritical);
	}
	inline bool bIsRocketCrit(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFProjectile_Rocket__m_bCritical);
	}
	inline bool bIsGlowEnabled (CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFPlayer__m_bGlowEnabled);
	}
	/*inline bool COffsets::bIsPlayingMvM (PDWORD pGameRules)
	{
		return *MakePtr(bool, *pGameRules, offset_DT_TFGameRulesProxy__m_bPlayingMannVsMachine);
	}*/
	inline void SetGlow (CBaseEntity *pBaseEntity, bool bIsGlowEnabled)
	{
		*MakePtr(bool, pBaseEntity, offset_DT_TFPlayer__m_bGlowEnabled) = bIsGlowEnabled;
	}
	inline int iGetSentrygunState(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectSentrygun__m_iState);
	}
	inline int iGetSentrygunKills(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_SentrygunLocalData__m_iKills);
	}
	inline int iGetSentryAssists(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_SentrygunLocalData__m_iAssists);
	}
	inline int iGetSentrygunAmmoRockets(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectSentrygun__m_iAmmoRockets);
	}
	inline int iGetSentrygunAmmoShells(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectSentrygun__m_iAmmoShells);
	}
	inline bool bGetSentryPlayerControlled(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectSentrygun__m_bPlayerControlled);
	}
	inline int iGetAmmo( CBaseEntity* pBaseEntity, CBaseEntity* pBaseWeapon )
	{
		return *MakePtr(int, pBaseEntity, ( offset_DT_LocalPlayerExclusive__m_iAmmo + ( 4 * iGetAmmoType(pBaseWeapon) ) ) );
	}
	inline float flGetSimulationTime( CBaseEntity* pBaseEntity )
	{
		return *MakePtr(float, pBaseEntity, offset_DT_BaseEntity__m_flSimulationTime );
	}
	inline int iGetItemIndex(CBaseEntity* pBaseEntity)
	{
		if (pBaseEntity == NULL)
			return -1;

		int iID = *MakePtr(int, pBaseEntity, offset_DT_BaseAttributableItem__m_AttributeManager + offset_DT_AttributeContainer__m_Item + offset_DT_ScriptCreatedItem__m_iItemDefinitionIndex );	

		if (iID > 1500)
		{
			return -1;
		}
		else
		{
			return iID;
		}
	}
	inline bool bIsRocketJumperRocket( CBaseEntity* pRocket )
	{
		CBaseEntity* pLauncher = (CBaseEntity*)gInts.EntList->GetClientEntityFromHandle(*MakePtr(int, pRocket, offfset_DT_TFBaseRocket__m_hLauncher));

		return ( pLauncher != NULL && iGetItemIndex(pLauncher) == WPN_RocketJumper );
	}
	inline bool bIsMedigunHealing(CBaseEntity* pWeapon)
	{
		return (pWeapon != NULL) && (*MakePtr(bool, pWeapon, offset_DT_WeaponMedigun__m_bHealing));
	}
	inline DWORD dwGetCritBoostedAddress( CBaseEntity *pBaseEntity )
	{
		return (DWORD)pBaseEntity + offset_DT_TFPlayer__m_Shared;
	}
	inline int iGetAmmoType(CBaseEntity *pBaseCombatWeapon )
	{
		return *MakePtr(int, pBaseCombatWeapon, offset_DT_LocalWeaponData__m_iPrimaryAmmoType);
	}
	inline int iGetDispenserAmmoMetal(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_ObjectDispenser__m_iAmmoMetal);
	}
	inline int iGetBuildingUpgradeMetal(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseObject__m_iUpgradeMetal);
	}
	inline float flGetMedigunCharge(CBaseEntity* pBaseEntity)
	{
		static int iMedigunOffset = offset_DT_BaseCombatCharacter__m_hMyWeapons + 4;
		int h = *MakePtr(int, pBaseEntity, iMedigunOffset );
		CBaseEntity* pMedigun = (CBaseEntity*)gInts.EntList->GetClientEntityFromHandle(h);
		if (pMedigun)
		{
			return (*MakePtr(float, pMedigun, offset_DT_NonLocalTFWeaponMedigunData__m_flChargeLevel) * 100);
		}
		else
		{
			return 0;
		}
	}
	inline void RemoveWeaponState( CBaseEntity *pBaseEntity )
	{
		*MakePtr(int, pBaseEntity, offset_DT_WeaponMinigun__m_iWeaponState) = 0;
	}
	inline int iGetBuildingUpgradeLevel(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseObject__m_iUpgradeLevel);
	}
	inline bool bIsBuildingMini(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_BaseObject__m_bMiniBuilding);
	}
	inline int iGetObjectMode(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseObject__m_iObjectMode);
	}
	inline bool bIsBuildingDisabled(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_BaseObject__m_bDisabled);
	}
	inline float flGetBuildingPercentageConstructed(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(float, pBaseEntity, offset_DT_BaseObject__m_flPercentageConstructed);
	}
	inline int iGetPipebombCount(CBaseEntity *pBaseWeapon)
	{
		return *MakePtr(int, pBaseWeapon, offset_DT_PipebombLauncherLocalData__m_iPipebombCount);
	}
	inline bool bGetBuildingIsBuilding(CBaseEntity *pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_BaseObject__m_bBuilding);
	}
	inline int iGetBuildingHealth(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseObject__m_iHealth);
	}
	inline int iGetBuildingMaxHealth(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_BaseObject__m_iMaxHealth);
	}
	inline bool bGetBuildingHasSapper(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_BaseObject__m_bHasSapper);
	}
	inline int iGetFlagStatus(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_CaptureFlag__m_nFlagStatus);
	}
	inline bool bHasShield(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(bool, pBaseEntity, offset_DT_TFPlayer__m_Shared + offset_DT_TFPlayerShared__m_bShieldEquipped);
	}
	inline CBaseEntity* GetIntelligenceOwner(CBaseEntity* pBaseEntity)
	{
		int hPrevOwner = *MakePtr(int, pBaseEntity, offset_DT_CaptureFlag__m_hPrevOwner);
		return (CBaseEntity*)(gInts.EntList->GetClientEntityFromHandle(hPrevOwner));
	}
	inline float flGetFlagResetTime(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(float, pBaseEntity, offset_DT_CaptureFlag__m_flResetTime);
	}
	inline int iGetStickyType(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_TFProjectile_Pipebomb__m_iType);
	}
	inline Vector vecVelocity(CBaseEntity* pBaseEntity)
	{
		return *(Vector*)((DWORD)pBaseEntity + offset_DT_LocalPlayerExclusive__m_vecVelocity);
	}
	inline int iGetTickBase(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(int, pBaseEntity, offset_DT_LocalPlayerExclusive__m_nTickBase);
	}
	inline void SetVecVelocity(CBaseEntity* pBaseEntity, Vector& vecToCopy)
	{
		*(Vector*)((DWORD)pBaseEntity + offset_DT_LocalPlayerExclusive__m_vecVelocity) = vecToCopy;
	}
	inline float GetSpawnTime(CBaseEntity* pBaseEntity)
	{
		return *MakePtr(float, pBaseEntity, dwflSpawnTimeOffset);
	}
	inline void SetSpawnTime(CBaseEntity* pBaseEntity, float flTime)
	{
		*MakePtr(float, pBaseEntity, dwflSpawnTimeOffset) = flTime;
	}
	inline CBaseEntity* GetOwnerEntity(CBaseEntity* pBaseEntity)
	{
		int h = *MakePtr(int, pBaseEntity, offset_DT_BaseEntity__m_hOwnerEntity);
		return (CBaseEntity*)(gInts.EntList->GetClientEntityFromHandle(h));
	}
	inline CBaseEntity* GetThrowerEntity(CBaseEntity* pBaseEntity)
	{
		int h = *MakePtr(int, pBaseEntity, offset_DT_BaseGrenade__m_hThrower);
		return (CBaseEntity*)(gInts.EntList->GetClientEntityFromHandle(h));
	}
	inline int GetScore(int entIndex)
	{
		DWORD address = *dwTFPlayerResource;
		return *(PINT)(address + (offset_DT_TFPlayerResource__m_iTotalScore + entIndex * 4));
	}
	inline int GetMaxHealth(CBaseEntity* pBaseEntity)
	{
		DWORD address = *dwTFPlayerResource;
		return *(PINT)(address + (offset_DT_TFPlayerResource__m_iMaxHealth + pBaseEntity->GetIndex() * 4));
	}
	inline float GetNextRespawnTime(int entIndex)
	{
		DWORD address = *dwTFPlayerResource;
		return *(PFLOAT)(address + (offset_DT_TFPlayerResource__m_flNextRespawnTime + entIndex * 4));
	}
	inline int GetClassFromResource(int entIndex)
	{
		DWORD address = *dwTFPlayerResource;
		return *(PINT)(address + (offset_DT_TFPlayerResource__m_iPlayerClass + entIndex * 4));
	}
	PDWORD pdwMoveHelper, dwGlobalSeed, dwGameRules, dwMoveHelper, dwTFPlayerResource;
	DWORD dwCurrentCommandOffset, dwIsDuelingLocalPlayerFn, dwCritBoostedFn, dwMD5PseudoRandom, dwWriteUserCmd, dwEstAbsVelocity, dwGetLerpFn, dwRandomFloat, dwRandomInt, dwRandomSeed, dwGetSlotItem, dwPureReturn, dwCalcPlayerViewReturn, dwGetGameResources, dwflSpawnTimeOffset, dwShowScoresOn, dwShowScoresOff, dwExit, dwWellGoodInitM8, dwSetKeyValuesName, dwmpCommandsOffset;
	CMoveData MoveData;
	int iResistToSelect;
};
extern COffsets gOffsets;
//===================================================================================