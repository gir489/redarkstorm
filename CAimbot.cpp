#include "CAimbot.h"
#include "COffsets.h"
#include "ControlVariables.h"
#include "WeaponList.h"
#include "Client.h"
//====================================================================================
CAimbot gAimbot;
#ifdef _DEBUG
char chTriggerbotMessage[255];
#endif
//===================================================================================
void CAimbot::SinCos(float radians, float *sine, float *cosine)
{
	*sine = sin(radians);
	*cosine = cos(radians);
}
//===================================================================================
void CAimbot::AngleVector( Vector& Angles, Vector *in )
{
	float sp, sy, cp, cy;
	
	SinCos( DEG2RAD( Angles[1] ), &sy, &cy );
	SinCos( DEG2RAD( Angles[0] ), &sp, &cp );
	
	in->x = cp*cy;
	in->y = cp*sy;
	in->z = -sp;
}
//===================================================================================
void CAimbot::VectorAngles( Vector &forward, Vector &angles )
{
	float tmp, yaw, pitch;
	
	if (forward[1] == 0 && forward[0] == 0)
	{
		yaw = 0;
		if (forward[2] > 0)
			pitch = 270;
		else
			pitch = 90;
	}
	else
	{
		yaw = (atan2(forward[1], forward[0]) * 180 / M_PI );
		if (yaw < 0)
			yaw += 360;

		tmp = sqrt(forward[0] * forward[0] + forward[1] * forward[1]);
		pitch = (atan2(-forward[2], tmp) * 180 / M_PI );
		if (pitch < 0)
			pitch += 360;
	}
	
	angles[0] = pitch;
	angles[1] = yaw;
	angles[2] = 0;
}
//===================================================================================
void CAimbot::VectorAngles( const Vector &forward, const Vector &pseudoup, Vector &angles )
{
	Vector left;

	left.x = pseudoup.y*forward.z - pseudoup.z*forward.y;
	left.y = pseudoup.z*forward.x - pseudoup.x*forward.z;
	left.z = pseudoup.x*forward.y - pseudoup.y*forward.x;

	left.NormalizeInPlace();

	float xyDist = sqrt(forward[0] * forward[0] + forward[1] * forward[1]);

	if ( xyDist > 0.001f )
	{
		angles[1] = RAD2DEG( atan2f( forward[1], forward[0] ) );
		angles[0] = RAD2DEG( atan2f( -forward[2], xyDist ) );
		angles[2] = RAD2DEG( atan2f( left[2], ((left[1] * forward[0]) - (left[0] * forward[1])) ) );
	}
	else
	{
		angles[1] = RAD2DEG( atan2f( -left[0], left[1] ) );
		angles[0] = RAD2DEG( atan2f( -forward[2], xyDist ) );
		angles[2] = 0;
	}	
}
//===================================================================================
void CAimbot::AngleVectors( Vector& angles, Vector *forward, Vector *right, Vector *up )
{	
	float sr, sp, sy, cr, cp, cy;

	SinCos( DEG2RAD( angles[0] ), &sp, &cp );
	SinCos( DEG2RAD( angles[1] ), &sy, &cy );
	SinCos( DEG2RAD( angles[2] ), &sr, &cr );

	if (forward)
	{
		forward->x = cp*cy;
		forward->y = cp*sy;
		forward->z = -sp;
	}

	if (right)
	{
		right->x = (-1*sr*sp*cy+-1*cr*-sy);
		right->y = (-1*sr*sp*sy+-1*cr*cy);
		right->z = -1*sr*cp;
	}

	if (up)
	{
		up->x = (cr*sp*cy+-sr*-sy);
		up->y = (cr*sp*sy+-sr*cy);
		up->z = cr*cp;
	}
}
//===================================================================================
void CAimbot::VectorTransform( Vector& in1, const matrix3x4 &in2, Vector &out )
{
	out[0] = (in1[0]*in2[0][0] + in1[1]*in2[0][1] + in1[2]*in2[0][2]) + in2[0][3];
	out[1] = (in1[0]*in2[1][0] + in1[1]*in2[1][1] + in1[2]*in2[1][2]) + in2[1][3];
	out[2] = (in1[0]*in2[2][0] + in1[1]*in2[2][1] + in1[2]*in2[2][2]) + in2[2][3];
}
//===================================================================================
float CAimbot::flGetDistance( Vector vOrigin )
{
	if ( vOrigin.IsZero() )
		return 0;

	Vector vDistance = vOrigin - gPlayerVars.vecEyePos;

	float flDistance = sqrt(vDistance.Length());

	if( flDistance < 0.1f )
		return 0.1f;

	return flDistance;
}
//===================================================================================
float CAimbot::flGetDistance( Vector vStart, Vector vEnd )
{
	Vector vDelta = vStart - vEnd;

	float fDistance = sqrt(vDelta.Length());

	if( fDistance < 0.1f )
		return 0.1f;

	return fDistance;
}
//===================================================================================
float	CAimbot::flGetFOV( Vector vOrigin ) 
{
	try
	{
		CBaseEntity* pBaseEntity = GetBaseEntity(me);

		Vector qAim, vecAim;
		gInts.Engine->GetViewAngles(qAim);

		AngleVector( qAim, &vecAim );

		Vector vecEyeDiff = vOrigin - gPlayerVars.vecEyePos;
		vecEyeDiff.NormalizeInPlace();

		float flDotProduct = vecAim.Dot(vecEyeDiff);
		float flFoV = RAD2DEG( acos( flDotProduct ) );

		if (flFoV > 0)
		{
			return flFoV;
		}
		else
		{
			return 0;
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed flGetFOV");
		return 0;
	}
}
//===================================================================================
int CAimbot::iGetMeleeDistance( )
{
	/*if (gPlayerVars.iClass == TF2_Heavy)
	{
		switch (gPlayerVars.iWeaponID)
		{
			case WPN_Fists:
			case WPN_NewFists:
			case WPN_ApocoFists:
			case WPN_HolidayPunch:
			{
				if (gPlayerVars.iPlayerCond& TFCond_Taunting)
				{
					return 22;
				}
			}
		}
	}*/
	return 13;
}
//===================================================================================
CAimbot::CAimbot( void )
{
	m_nTarget = -1;
}
//===================================================================================
void CAimbot::DropTarget( void )
{
	m_nTarget = -1;
}
//===================================================================================
void CAimbot::SetTarget( int iIndex )
{
	m_nTarget = iIndex;
}
//===================================================================================
bool CAimbot::bHasTarget( void )
{
	return m_nTarget > -1;
}
//===================================================================================
bool	CAimbot::bCheckTeam( int iIndex )
{
	switch ( CV (aim_team) )
	{
		case 1:
			return ( gPlayerVars.iTeamNum != gOffsets.iGetTeamNum( GetBaseEntity(iIndex) ) );
		case 2:
			return ( gPlayerVars.iTeamNum == gOffsets.iGetTeamNum( GetBaseEntity(iIndex) ) );
		case 0:
			return ( gPlayerVars.iTeamNum > 1 );
	}
	return false;
}
//===================================================================================
bool	CAimbot::bIsVisible( Vector& vecEnemy, CBaseEntity* pBaseEntity ) 
{
	trace_t pTrace;
	Ray_t pRay;
	TraceFilter filter;
	
	pRay.Init( gPlayerVars.vecEyePos, vecEnemy );
	gInts.Trace->TraceRay( pRay, MASK_TF2, &filter, &pTrace );
	
	if ( pTrace.m_pEnt )
	{
		if ( gOffsets.iGetTeamNum(pTrace.m_pEnt) == gPlayerVars.iTeamNum )
		{
			gInts.Trace->TraceRay( pRay, (MASK_TF2|CONTENTS_HITBOX), &filter, &pTrace );
		}
		return ( pTrace.m_pEnt == pBaseEntity );
	}
	return false;
}
//===================================================================================
int bIsHeadshotWeapon( int iWeaponID )
{
	if (gMiscFuncs.bIsKey(CV(aim_key)))
	{
		switch (iWeaponID)
		{
		case WPN_SniperRifle:
		case WPN_NewSniperRifle:
		case WPN_Bazaarbargain:
		case WPN_Machina:
		case WPN_FestiveSniperRifle:
		case WPN_HitmanHeatmaker:
		case WPN_AWP:
		case WPN_BotSniperRifleS:
		case WPN_BotSniperRifleG:
		case WPN_BotSniperRifleR:
		case WPN_BotSniperRifleB:
		case WPN_BotSniperRifleC:
		case WPN_BotSniperRifleD:
		case WPN_BotSniperRifleES:
		case WPN_BotSniperRifleEG:
			if (gPlayerVars.iPlayerCond & TFCond_Zoomed)
				return 1;
			else
				return 0;
		case WPN_Ambassador:
		case WPN_ClassicSniperRifle:
		case WPN_FestiveAmbassador:
		case WPN_Huntsman:
		case WPN_FestiveHuntsman:
			return 2;
		default:
			return false;
		}
	}
	else
	{
		return false;
	}
}
//===================================================================================
int iFixEngineerHitbox( int iClassID, int iHitbox )
{
	if ( iHitbox == HITBOX_HEAD )
	{
		return HITBOX_HEAD;
	}
	return ( iClassID == TF2_Engineer ) ? iHitbox - 1 : iHitbox;
}
//===================================================================================
int iGetArmBone( int iClass, int iHitboxToConvert )
{
	if ( iHitboxToConvert == HITBOX_LOWERLEFTARM )
	{
		switch ( iClass )
		{
			case TF2_Scout:
				return 16;
			case TF2_Soldier:
				return 17;
			case TF2_Pyro:
				return 9;
			case TF2_Demoman:
			case TF2_Engineer:
				return 13;
			case TF2_Heavy:
				return 14;
			case TF2_Medic:
			case TF2_Sniper:
				return 12;
			case TF2_Spy:
				return 10;
		}
	}
	else
	{
		switch ( iClass )
		{
			case TF2_Scout:
				return 25;
			case TF2_Soldier:
				return 9;
			case TF2_Pyro:
				return 13;
			case TF2_Demoman:
			case TF2_Medic:
			case TF2_Spy:
				return 14;
			case TF2_Heavy:
				return 7;
			case TF2_Engineer:
				return 16;
			case TF2_Sniper:
				return 20;
		}
	}
	return 0;
}
//===================================================================================
Vector CAimbot::vecGetHitbox( CBaseEntity* pBaseEntity, int iHitbox )
{
	Vector vHitbox;
	if (pBaseEntity == NULL || pBaseEntity->GetIndex() == 0)
	{
		return vHitbox;
	}

	DWORD *pStudioHdr = (PDWORD)gInts.ModelInfo->GetStudiomodel( pBaseEntity->GetModel( ) );

	if ( !pBaseEntity->SetupBones( pBoneToWorld, 128, 0x100, gOffsets.flGetSimulationTime(pBaseEntity) ) )
	{
		return vHitbox;
	}

	int HitboxSetIndex = *( int* )( ( DWORD )pStudioHdr + 0xB0 );
	mstudiohitboxset_t* pSet = ( mstudiohitboxset_t* )( ( ( PBYTE )pStudioHdr ) + HitboxSetIndex );

	pbox = pSet->pHitbox(iHitbox);

	VectorTransform( pbox->bbmin, pBoneToWorld[ pbox->bone ], vMin );
	VectorTransform( pbox->bbmax, pBoneToWorld[ pbox->bone ], vMax );

	vHitbox = ( vMin + vMax ) / 2;

	return vHitbox;
}
//===================================================================================
bool	CAimbot::bIsTargetSpot( int iIndex )
{

	int iLine = __LINE__;
	try
	{

	int iHitbox;
	CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

	if( pBaseEntity == NULL )
		return false;

	if ( gOffsets.bGetIsAlive(pBaseEntity) == false )
		return false;

	Vector qCurrentAngle, vHitbox;

	gInts.Engine->GetViewAngles( qCurrentAngle );


	iLine = __LINE__;


	if ( bIsHeadshotWeapon(gPlayerVars.iWeaponID) )
	{
		vHitbox = vecGetHitbox(pBaseEntity, HITBOX_HEAD);

		int iClass = gOffsets.iGetPlayerClass(pBaseEntity);

		switch (iClass)
		{
			case TF2_Pyro:
			{
				vHitbox.z += 2;
				break;
			}
			case TF2_Demoman:
			case TF2_Soldier:
			{
				vHitbox.z += 1;
				break;
			}
			case 0:
				return false;
		}

		if ( bIsVisible(vHitbox, pBaseEntity ) )
		{
			gOffsets.SetVecVelocity(pBaseEntity, vHitbox);
			flAimDistance[iIndex] = flGetDistance( vHitbox );
			if ( gOffsets.bIsHoldingDeadRinger( GetBaseEntity(iIndex) ) && gPlayerVars.bHasMeleeWeapon == false )
			{
				flAimDistance[iIndex] += 8192;
			}
			flAimFOV[ iIndex ] = flGetFOV( vHitbox );
			return true;
		}
	}
	else
	{
		int iCounter = 0;
		int iClass = gOffsets.iGetPlayerClass(pBaseEntity);

		if (iClass == 0)
		{
			return false;
		}
		int iMax = 4;

		if (CV(aim_foot))
		{
			iMax = 6;
		}

		int iHitboxs[] = { HITBOX_MIDDLECHEST, HITBOX_HEAD, HITBOX_LOWERLEFTARM, HITBOX_LOWERRIGHTARM, HITBOX_LEFTKNEE, HITBOX_RIGHTKNEE };

		Vector qCurrentAngle;
		gInts.Engine->GetViewAngles( qCurrentAngle );

		Vector vHitbox;

		DWORD *pStudioHdr = gInts.ModelInfo->GetStudiomodel( pBaseEntity->GetModel( ) );
	
		if ( pBaseEntity->SetupBones( pBoneToWorld, 128, 0x100, gOffsets.flGetSimulationTime(pBaseEntity) ) == false )
			return false;
		 
		while ( iCounter < iMax )
		{
			iHitbox = iHitboxs[iCounter];
			if ( (iHitbox == HITBOX_LOWERLEFTARM || iHitbox == HITBOX_LOWERRIGHTARM ) )
			{
				int iBone = iGetArmBone(iClass, iHitbox);
				vHitbox.x = pBoneToWorld[iBone][0][3];
				vHitbox.y = pBoneToWorld[iBone][1][3];
				vHitbox.z = pBoneToWorld[iBone][2][3];
			}
			else
			{
				int HitboxSetIndex = *( int* )( ( DWORD )pStudioHdr + 0xB0 );
				mstudiohitboxset_t* pSet = ( mstudiohitboxset_t* )( ( ( PBYTE )pStudioHdr ) + HitboxSetIndex );

				pbox = pSet->pHitbox(iFixEngineerHitbox( iClass, iHitbox ));

				VectorTransform( pbox->bbmin, pBoneToWorld[ pbox->bone ], vMin );
				VectorTransform( pbox->bbmax, pBoneToWorld[ pbox->bone ], vMax );
				vHitbox = ( vMin + vMax ) * 0.5f;
			}
			if ( vHitbox.IsZero() )
			{
				return false;
			}
			if ( bIsVisible(vHitbox, pBaseEntity) )
			{
				gOffsets.SetVecVelocity(pBaseEntity, vHitbox);
				flAimDistance[iIndex] = flGetDistance( vHitbox );
				if ( gOffsets.bIsHoldingDeadRinger( GetBaseEntity(iIndex) ) )
				{
					if ( gPlayerVars.bHasMeleeWeapon == false )
					{
						flAimDistance[iIndex] += 8192;
					}
				}
				flAimFOV[ iIndex ] = flGetFOV( vHitbox );
				return true;
			}
			iCounter++;
		}
	}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bIsTargetSpot %i", iLine );
	}

	return false;
}
//===================================================================================
bool CAimbot::bIsValidEntity( int iIndex )
{

	try
	{

		if( iIndex == me )
			return false;

		CBaseEntity *pBaseEntity = GetBaseEntity(iIndex);

		if ( gMiscFuncs.bIsValidPlayer(pBaseEntity) == false )
			return false;

		if( pBaseEntity->IsDormant() )
			return false;

		return gOffsets.bGetIsAlive( GetBaseEntity(iIndex) );

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bIsValidEntity");
		return false;
	}

}
//===================================================================================
bool CAimbot::bIsValidTarget( int iIndex )
{

	int iLine = __LINE__;
	try
	{

		if( bIsValidEntity( iIndex ) == false )
			return false;


		iLine = __LINE__;


		if( bCheckTeam( iIndex ) == false )
			return false;


		iLine = __LINE__;


		CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

		if( bStateAndFriendCheck( pBaseEntity ) == false )
			return false;


		iLine = __LINE__;


		if( bIsTargetSpot( iIndex ) == false )
			return false;

		/*if ( gOffsets.bIsPlayingMvM(gOffsets.dwGameRules) )
		{
			int iClass = gOffsets.iGetPlayerClass(pBaseEntity);
			if ( iClass == TF2_Demoman )
			{
				CBaseEntity* pBaseWeapon = gOffsets.pGetBaseCombatActiveWeapon(pBaseEntity);
				if ( pBaseWeapon != NULL && gOffsets.iGetItemIndex(pBaseWeapon) == WPN_Ullapool && gOffsets.iGetHealth(pBaseEntity) == 1 )
				{
					return false;
				}
			}
			if ( gOffsets.iGetHealth(pBaseEntity) >= 4900 && iClass == TF2_Heavy && gOffsets.iGetNumHealers(pBaseEntity) > 0 )
			{
				return false;
			}
		}*/


		iLine = __LINE__;


		if ( gPlayerVars.bHasMeleeWeapon )
		{
			if( flAimFOV[ iIndex ] > CV( melee_fov ) )
				return false;
		}
		else
		{
			if( flAimFOV[ iIndex ] > CV( aim_fov ) )
				return false;
		}

		return true;
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bIsValidTarget %i", iLine);
		return false;
	}
}
//===================================================================================
void CAimbot::FindTarget( )
{
	try
	{
		CBaseEntity *pBaseEntity = GetBaseEntity(me);

		if ( pBaseEntity == NULL || gOffsets.bGetIsAlive(pBaseEntity) == false || gOffsets.pGetBaseCombatActiveWeapon(pBaseEntity) == NULL )
		{
			DropTarget();
			return;
		}
		
		gPlayerVars.vecEyePos = gOffsets.GetEyePosition(pBaseEntity);

		if( CV(aim_lock) )
		{
			if ( CV(aim_lock) == 1)
			{
				if ( bHasTarget() && bIsValidTarget( m_nTarget ) )
				{
					return;
				}
			}
			if ( CV(aim_lock) == 2)
			{
				static bool bFoundTarget;
				if ( gMiscFuncs.bIsKey(CV(aim_key) ) )
				{
					if ( bHasTarget() && bFoundTarget == false)
					{
						bFoundTarget = true;
					}
					if (bFoundTarget)
					{
						bIsTargetSpot( m_nTarget );
						return;
					}
				}
				else if (bFoundTarget == true)
				{
					bFoundTarget = false;
				}
			}
		}

		DropTarget();

		if ( (CV(aim_rage) == 1) && CV(esp_rage) != 0 )
		{
			FindTargetCall(CV(esp_rage));
			if (m_nTarget != CV(esp_rage))
			{
				for( int iIndex = 1; iIndex <= gInts.Globals->maxclients; iIndex++ )
				{
					if (iIndex != CV(esp_rage))
					{
						FindTargetCall(iIndex);
					}
				}
			}
		}
		else
		{
			for( int iIndex = 1; iIndex <= gInts.Globals->maxclients; iIndex++ )
			{
				FindTargetCall(iIndex);
			}
		}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed FindTarget");
	}

}
//===================================================================================
void	CAimbot::AimAtTarget( )
{

	try
	{

		gPlayerVars.vecAimAngles.Zero();

		if( (!(gMiscFuncs.bIsKey( CV( aim_key ) ) ) || 
			(bHasTarget() == false) ) && 
			gPlayerVars.Sentry == NULL && 
			gPlayerVars.Sticky == NULL )
		{
			return;
		}

		if( GetBaseEntity(me) == NULL )
			return;

		CBaseEntity* pBaseEntity = GetBaseEntity(m_nTarget);

		if ( CV(aim_lock) == 2 )
		{
			int iFOV = CV(aim_fov);
			if (gPlayerVars.bHasMeleeWeapon)
			{
				iFOV = CV(melee_fov);
			}
			if ( bIsValidTarget(m_nTarget) == false || flAimFOV[m_nTarget] > iFOV )
			{
				return;
			}
		}

		Vector vecAimSpot;

		if (bHasTarget())
		{
			vecAimSpot = gOffsets.vecVelocity(pBaseEntity);
		}
		else
		{
			if (gPlayerVars.Sticky)
			{
				gPlayerVars.Sticky->GetWorldSpaceCenter(vecAimSpot);
			}
			if (gPlayerVars.Sentry)
			{
				vecAimSpot = gPlayerVars.vecSentry;
			}
		}
	
		auto param = vecAimSpot - gPlayerVars.vecEyePos;
		VectorAngles(param, qAimAngles );

		if ( CV( aim_smooth ) )
		{
			Vector qCurrentView, qDelta, qAimDirection = qAimAngles;

			gInts.Engine->GetViewAngles( qCurrentView );

			qDelta = qAimDirection - qCurrentView;

			ClampAngle(qDelta);

			if (gPlayerVars.bHasMeleeWeapon && CV(melee_smooth))
			{
				qAimDirection = qCurrentView + qDelta / CV( melee_smooth );
			}
			else if (!gPlayerVars.bHasMeleeWeapon)
			{
				qAimDirection = qCurrentView + qDelta / CV( aim_smooth );
			}

			ClampAngle(qAimDirection);

			gInts.Engine->SetViewAngles( qAimDirection );
		}
		else
		{
			ClampAngle(qAimAngles);

			if ( CV(aim_silentaim) && !gPlayerVars.bHasMeleeWeapon )
			{
				gPlayerVars.vecAimAngles = qAimAngles;
			}
			else
			{
				gInts.Engine->SetViewAngles( qAimAngles );
			}
		}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Aim At Target");
	}

}
//===================================================================================
bool CAimbot::bTraceToSentry( Vector& vecStart )
{

	try
	{

		trace_t pTrace;
		Ray_t pRay;
		TraceFilter filter;

		pRay.Init( gOffsets.GetEyePosition( GetBaseEntity(me) ), vecStart );
		gInts.Trace->TraceRay( pRay, MASK_TF2, &filter, &pTrace );

		if (pTrace.m_pEnt)
		{
			return ( pTrace.m_pEnt->GetClientClass()->iClassID == gEntIDs.iSentryID );
		}

		return false;

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bIsVisible Sentry");
		return false;
	}

}
//===================================================================================
void  CAimbot::AimAtVector( Vector vecAim )
{
	Vector vForward, vRight, vUp;
	Vector qPlayerAngle, qAimAngles;
	gInts.Engine->GetViewAngles( qPlayerAngle );

	AngleVectors( qPlayerAngle, &vForward, &vRight, &vUp );

	vForward.z = -vForward.z;
	
	vTargetOrg += ( vForward + vUp + vRight );

	auto param = vecAim - gPlayerVars.vecEyePos;
	VectorAngles( param, qAimAngles );

	ClampAngle(qAimAngles);

	gInts.Engine->SetViewAngles( qAimAngles );
}
//=================================================================================
void  CAimbot::AimAtSticky( CBaseEntity* pSticky, CUserCmd* pCommand )
{
	Vector vForward, vRight, vUp, vTargetOrg, vStickyOrg;
	Vector qPlayerAngle, qAimAngles;
	gInts.Engine->GetViewAngles( qPlayerAngle );

	AngleVectors( qPlayerAngle, &vForward, &vRight, &vUp );

	vForward.z = -vForward.z;
	
	vTargetOrg += ( vForward + vUp + vRight );

	pSticky->GetWorldSpaceCenter(vStickyOrg);

	auto param = vStickyOrg - gPlayerVars.vecEyePos;
	VectorAngles(param, qAimAngles);

	ClampAngle(qAimAngles);

	pCommand->viewangles = qAimAngles;

	SilentAimFix(pCommand, qPlayerAngle);
}
//=================================================================================
int CAimbot::bTriggerbotTrace( CUserCmd* pCommand )
{
	int iLine = __LINE__;
	try
	{
		float flReloadTime = gMiscFuncs.GetReloadTime();
		if ( flReloadTime != 0.0f )
		{
			switch (gPlayerVars.iWeaponID)
			{
				case WPN_NewShotgun:
				case WPN_Scattergun:
				case WPN_NewScattergun:
				case WPN_SoldierShotgun:
				case WPN_PyroShotgun:
				case WPN_EngineerShotgun:
				case WPN_BabyFaceBlaster:
				{
					if (flReloadTime > .26)
					{
						return TRIGGERBOT_FAIL;
					}
				}
			}
		}

		Vector vDirection;
		trace_t pTrace;
		Ray_t ray;
		TraceFilter filter;

		iLine = __LINE__;
		
		AngleVector( pCommand->viewangles, &vDirection );

		CBaseEntity *pBaseEntity = GetBaseEntity(me);

		if (pBaseEntity != NULL)
		{
			vDirection = vDirection * 8192 + gOffsets.GetEyePosition(pBaseEntity);
		}
		else
		{
			return TRIGGERBOT_FAIL;
		}

		iLine = __LINE__;
		if ( (gMiscFuncs.bIsStreamWeapon(gPlayerVars.iWeaponID) && gMiscFuncs.bIsKey(CV(aim_key)) && (bHasTarget()) && (CV(aim_smooth) < 1)) || gPlayerVars.Sentry != NULL )
		{
			return TRIGGERBOT_STREAM;
		}
		else
		{
			iLine = __LINE__;
			ray.Init( gPlayerVars.vecEyePos, vDirection );
			if (gPlayerVars.bHasMeleeWeapon)
			{
				iLine = __LINE__;
				gInts.Trace->TraceRay( ray, (MASK_TF2), &filter, &pTrace );
			}
			else
			{
				iLine = __LINE__;
				gInts.Trace->TraceRay( ray, (MASK_TF2|CONTENTS_HITBOX), &filter, &pTrace );
			}
		}

		iLine = __LINE__;
		//AUTOKNIFE
		if( ( gPlayerVars.iClass == TF2_Spy ) && gPlayerVars.bHasMeleeWeapon )
		{
			#ifdef _DEBUG
			sprintf_s(chTriggerbotMessage, "Backstab");
			#endif
			return gOffsets.bGetKnifeReadyToBackstab( gPlayerVars.pBaseWeapon );
		}

		iLine = __LINE__;
		if ( pTrace.m_pEnt != NULL )
		{
			iLine = __LINE__;
			if ( bCheckTeam( pTrace.m_pEnt->GetIndex() ) == false )
			{
				iLine = __LINE__;
				#ifdef _DEBUG
				sprintf_s(chTriggerbotMessage, "Failed Team");
				#endif
				return TRIGGERBOT_FAIL;
			}

			iLine = __LINE__;
			int bHeadshot = bIsHeadshotWeapon(gPlayerVars.iWeaponID);
			if ( bHeadshot )
			{
				if (((gInts.Globals->curtime - gOffsets.GetSpawnTime(gPlayerVars.pBaseWeapon)) < 0.4f) && bHeadshot == 1 )
				{
					return TRIGGERBOT_FAIL;
				}
				iLine = __LINE__;
				if ( bStateAndFriendCheck(pTrace.m_pEnt) == false )
				{
					return TRIGGERBOT_FAIL;
				}

				CBaseEntity* pBaseHit = pTrace.m_pEnt;

				Vector vecViewAnglesBackup;
				
				VectorCopy(pCommand->viewangles, vecViewAnglesBackup);
				
				vecViewAnglesBackup.x += 2.5;
				
				AngleVector( vecViewAnglesBackup, &vDirection );
				
				vDirection = vDirection * 8192 + gOffsets.GetEyePosition( GetBaseEntity(me) );
				
				gInts.Trace->TraceRay( ray, (MASK_TF2|CONTENTS_HITBOX), &filter, &pTrace );

				return (pTrace.m_pEnt == pBaseHit && pTrace.hitbox == HITBOX_HEAD && pTrace.hitGroup == 1);
			}
			else
			{
				iLine = __LINE__;
				if ( CV(aim_stickies) )
				{
					if ( pTrace.m_pEnt->GetClientClass()->iClassID == gEntIDs.iGrenadeID && gOffsets.iGetStickyType(pTrace.m_pEnt) == 1 )
					{
						#ifdef _DEBUG
						sprintf_s(chTriggerbotMessage, "Found Sticky");
						#endif
						return ( gOffsets.iGetTeamNum(pTrace.m_pEnt) != gPlayerVars.iTeamNum );
					}
				}
				iLine = __LINE__;
				if ( pTrace.m_pEnt->GetClientClass()->iClassID == gEntIDs.iSentryID && gPlayerVars.bHasFlameThrower == false )
				{
					#ifdef _DEBUG
					sprintf_s(chTriggerbotMessage, "Found Sentry");
					#endif
					return ( gOffsets.iGetTeamNum(pTrace.m_pEnt) != gPlayerVars.iTeamNum );
				}
				iLine = __LINE__;
				if (gMiscFuncs.bIsValidPlayer(pTrace.m_pEnt) == false)
				{
					#ifdef _DEBUG
					sprintf_s(chTriggerbotMessage, "Found: %s", pTrace.m_pEnt->GetClientClass()->chName);
					#endif
					return TRIGGERBOT_FAIL;
				}
				iLine = __LINE__;
				if (bStateAndFriendCheck(pTrace.m_pEnt) == false)
				{
					#ifdef _DEBUG
					sprintf_s(chTriggerbotMessage, "bStateAndFriendCheck");
					#endif
					return TRIGGERBOT_FAIL;
				}
				iLine = __LINE__;
				if (gPlayerVars.bHasFlameThrower)
				{
					if (flGetDistance(pTrace.m_pEnt->GetAbsOrigin()) < 20 )
					{
						#ifdef _DEBUG
						sprintf_s(chTriggerbotMessage, "Flame");
						#endif
						return TRIGGERBOT_SUCCESS;
					}
				}
				else
				{
					iLine = __LINE__;
					if (gPlayerVars.bHasMeleeWeapon == true)
					{
						if (flGetDistance(pTrace.m_pEnt->GetAbsOrigin()) < 10 )
						{
							#ifdef _DEBUG
							sprintf_s(chTriggerbotMessage, "Found Melee");
							#endif
							return TRIGGERBOT_SUCCESS;
						}
					}
					else
					{
						#ifdef _DEBUG
						sprintf_s(chTriggerbotMessage, "Found: %s", pTrace.m_pEnt->GetPlayerName());
						#endif
						return TRIGGERBOT_SUCCESS;
					}
				}
			}
		}
	}
	catch(...)
	{
		#ifdef _DEBUG
		sprintf_s(chTriggerbotMessage, "Crashed");
		#endif
		gBaseAPI.LogToFile("Failed Triggerbot %i", iLine);
		return TRIGGERBOT_FAIL;
	}
	#ifdef _DEBUG
	sprintf_s(chTriggerbotMessage, "No Target");
	#endif
	return TRIGGERBOT_FAIL;
}
//=================================================================================
bool CAimbot::bIsVisiblePlayer( Vector& vecStart, CBaseEntity* pBaseEntity )
{

	try
	{

		trace_t pTrace;
		Ray_t pRay;
		TraceFilter2 filter;

		Vector vecOrigin;
		pBaseEntity->GetWorldSpaceCenter( vecOrigin );

		pRay.Init( vecStart, vecOrigin );
		gInts.Trace->TraceRay(pRay, MASK_TF2, &filter, &pTrace);

		if( pTrace.m_pEnt != NULL )
		{
			if( gOffsets.bGetIsAlive( pTrace.m_pEnt ) )
			{
				return (pBaseEntity == pTrace.m_pEnt);
			}
		}

		return false;

	}
	catch(...)
	{
		return false;
	}

}
//===================================================================================
bool CAimbot::checkStickyVis(CBaseEntity* pSticky, CBaseEntity* pPlayer)
{
	try
	{
		if (pSticky == NULL || pPlayer == NULL)
		{
			return false;
		}

		trace_t pTrace;
		Ray_t pRay;
		TraceFilter2 filter;

		Vector vecPlayerOrigin, vecStickyOrigin;
		pSticky->GetWorldSpaceCenter(vecStickyOrigin);
		pPlayer->GetWorldSpaceCenter(vecPlayerOrigin);

		if (flGetDistance(vecStickyOrigin, vecPlayerOrigin) > 13.1)
		{
			return false;
		}
		
		pRay.Init(vecStickyOrigin, vecPlayerOrigin);
		gInts.Trace->TraceRay(pRay, MASK_EXPLODE, &filter, &pTrace);
		if (pTrace.m_pEnt != NULL)
		{
			if (gOffsets.bGetIsAlive(pTrace.m_pEnt))
			{
				return (pPlayer == pTrace.m_pEnt);
			}
		}
		
		return false;

	}
	catch (...)
	{
		return false;
	}

}
//===================================================================================
bool CAimbot::bStateAndFriendCheck ( CBaseEntity* pBaseEntity )
{

	try
	{

	if ( gMiscFuncs.bIsValidPlayer(pBaseEntity) == false )
	{
		return false;
	}

	if ( CV(aim_rage) == 2 && CV(esp_rage) != 0 )
	{
		//if ( pBaseEntity->GetIndex() == CV(esp_rage) )
		//{
		//	return false;
		//}
	}

	int iPlayerCond = gOffsets.iGetPlayerCond( pBaseEntity );
	int iPlayerCondEx = gOffsets.iGetPlayerCondEx( pBaseEntity );
	int iClassID = gOffsets.iGetPlayerClass(pBaseEntity);

	if( iPlayerCond & TFCond_IgnoreStates || iPlayerCondEx & TFCondEx_IgnoreStates )
	{
		return false;
	}

	if (iClassID == TF2_Spy)
	{
		if ( (CV(aim_cloaked ) == 0) && ( iPlayerCond & TFCond_Cloaked ) && ( !( iPlayerCond & (TFCond_CloakFlicker|TFCond_Disguising|TFCond_Stunned|TFCond_Slowed|TFCond_OnFire|TFCond_Milked|TFCond_Jarated|TFCond_MarkedForDeath|TFCond_Bleeding) ) ) )
		{
			return false;
		}
		if ( CV(aim_deadringer) == 0 )
		{
			if ( (gOffsets.bIsHoldingDeadRinger(pBaseEntity) ) )
			{
				if (gOffsets.iGetRoundState(gOffsets.dwGameRules) != ROUND_WIN)
				{
					return false;
				}
			}
		}
	}

	if ( CV(aim_friends) == 0)
	{
		if (gMiscFuncs.bHasFriend(pBaseEntity))
		{
			return false;
		}
	}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bStateAndFriendCheck");
	}

	return true;
}
//===================================================================================
/*float CAimbot::flGetProjectileSpeed ( )
{
	try
	{
		switch( gPlayerVars.iWeaponID )
		{
			case WPN_Huntsman:
			{
				CBaseEntity* pWeapon = gOffsets.pGetBaseCombatActiveWeapon( GetBaseEntity( me ) );

				if (pWeapon == NULL)
				{
					return 0;
				}

				float flChargeTime = gOffsets.flGetChargeTime( pWeapon );
				if (flChargeTime < 0)
				{
					return 1800.0f;
				}
				else if (flChargeTime <= 1)
				{
					return 1800.0f + flChargeTime * 800.0f;
				}
				return 2600.0f;
			}
			case WPN_Crossbow:
				return 2500;
	//      case WPN_Sandman:
	//			return 1940;
			case WPN_Flaregun:
			case WPN_Detonator:
				return 1450;
			case WPN_GrenadeLauncher:
			case WPN_NewGrenadeLauncher:
				return 1065;
			case WPN_LochNLoad:
				return 1331;
			case WPN_Milk:
			case WPN_Jarate:
				return 850;
			case WPN_RocketLauncher:
			case WPN_NewRocketLauncher:
			case WPN_FestiveRocketLauncher:
			case WPN_BlackBox:
			case WPN_CowMangler:
			case WPN_Original:
				return 1100;
			case WPN_DirectHit:
				return 1980;
			case WPN_LibertyLauncher:
				return 1540;
			case WPN_StickyLauncher:
			case WPN_NewStickyLauncher:
			case WPN_ScottishResistance:
				return 805;
			case WPN_SyringeGun:
			case WPN_NewSyringeGun:
			case WPN_Blutsauger:
			case WPN_Overdose:
				return 990;
			default:
				return 0;
		}
	}
	catch(...)
	{
		return 0;
	}
}*/
//===================================================================================
int CAimbot::iGetHitbox( int iWeaponID, bool bOnGround )
{
	switch (iWeaponID)
	{
		case WPN_Huntsman:
			return HITBOX_HEAD;
		case WPN_RocketLauncher:
		case WPN_NewRocketLauncher:
		case WPN_BlackBox:
		case WPN_CowMangler:
		case WPN_Original:
		case WPN_DirectHit:
		case WPN_LibertyLauncher:
			if (bOnGround)
			{
				return HITBOX_LEFTKNEE;
			}
		default:
			return HITBOX_LOWERCHEST;
	}
}
//===================================================================================
/*float CAimbot::TimeOfImpact( float dist, float speed, CBaseEntity *pBaseEntity, Vector vOrg )
{
	float result = 0.0f, temp = 0.0f, delta = 0.0f, atime = ( dist / speed );
	Vector vTemp, vAdjusted, vFinal;

	int iFlags = gOffsets.iGetFlags(pBaseEntity);
	Vector v;
	gMiscFuncs.EstimateAbsVelocity( pBaseEntity, v );

	CBaseEntity *pLocal = GetBaseEntity(me);

	CBaseEntity *pBaseWeapon = gOffsets.pGetBaseCombatActiveWeapon(pLocal);

	if (pBaseWeapon)
	{
		if( flGetProjectileSpeed( ) )
		{
			result = ( dist / speed );
		}
		else
		{
			for( float time = 0.0; time < 1.5f; time += 0.01f )
			{
				float ent_gravity = 1;

				if( iFlags & FL_ONGROUND )
					v.z = v.z * SQUARE( atime );
				else
					v.z = ( v.z - ( ent_gravity * 800 * SQUARE( atime ) ) ) * 0.5f;

				Vector B(v.x * atime, v.y * atime, v.z );

				vFinal.x = vOrg.x + B.x;
				vFinal.y = vOrg.y + B.y;
				vFinal.z = vOrg.z + B.z;

				vFinal.x = vFinal.x - gPlayerVars.vecEyePos.x;
				vFinal.y = vFinal.y - gPlayerVars.vecEyePos.y;
				vFinal.z = vFinal.z - gPlayerVars.vecEyePos.z;

				temp = vTemp.Length() / speed;
			
				if( time >= temp )
				{
					delta = time - temp;
					atime += delta;

					break;
				}
			}

			result = ( dist / speed ) + delta;
		}
	}

	if( iFlags & FL_ONGROUND )
		result += 0.05f;

	return result;
}*/
//===================================================================================
/*float CAimbot::GetDrop( float dist, float speed, CBaseEntity *pBaseEntity, Vector vOrg )
{
	try
	{
	float g = 0.5f, r = 0.0f, a = 0.0f, time = TimeOfImpact( dist, speed, pBaseEntity, vOrg );

    switch( gPlayerVars.iWeaponID )
    {
		case WPN_SyringeGun:
		case WPN_NewSyringeGun:
		case WPN_Blutsauger:
		case WPN_Overdose:
		case WPN_GrenadeLauncher:
		case WPN_NewGrenadeLauncher:
		case WPN_LochNLoad:
		case WPN_Flaregun:
		case WPN_Detonator:
			g = 0.30000001f;
			break;
		case WPN_Crossbow:
			g = 0.199999f;
			break;
		default:
			g = 0.5f;
			break;
	}

	switch( gPlayerVars.iWeaponID ) // adjusted height for gravity
	{
		case WPN_GrenadeLauncher:
		case WPN_NewGrenadeLauncher:
		{
			if( dist < 1350.0f )
				a = FastSqrt( time * 2 );
			else
				a = FastSqrt( 10 );

			r = ( ( SQUARE( dist ) / 10 ) * a ) / speed;
			break;
		}
		case WPN_LochNLoad:
		{
			if( dist < 1350.0f )
				a = FastSqrt( time * 2 );
			else
				a = FastSqrt( 10 );

			r = ( ( SQUARE( dist ) / 20 ) * a ) / speed;
			break;
		}

		case WPN_SyringeGun:
		case WPN_NewSyringeGun:
		case WPN_Blutsauger:
		case WPN_Overdose:
			r = ( g * 800 * SQUARE( time ) ) * 0.06f;
			break;
		case WPN_Detonator:
		case WPN_Flaregun: // flaregun
			r = ( g * 800 * SQUARE( time ) ) * 0.07f;
			break;
		default:
			r = ( g * 800 * SQUARE( time ) ) * 0.5f;
			break;
	}

	Vector v;
	gMiscFuncs.EstimateAbsVelocity( pBaseEntity , v);
	int iFlags = gOffsets.iGetFlags(pBaseEntity);

	if( iFlags & FL_ONGROUND )
	{
		float flZDiff = vOrg[2] - gPlayerVars.vecEyePos[2];

		if( ( abs( v[2] ) >= 1.0f ) )
		{
			if( abs( flZDiff ) >= 1.0f )
			r -= ( GetWeaponGravity() * flZDiff );
		}
	}

	return r;
	}
	catch(...)
	{
		return 0;
	}
}*/
//===================================================================================
/*Vector CAimbot::BallisticPrediction( CBaseEntity* pEntity )
{
	int iFailed = 0;
	try
	{
    Vector vecPos, vecVelocity;
    Vector qCurrentAngle;

	iFailed = 1;

	gInts.Engine->GetViewAngles(qCurrentAngle);

	int iPlayerCond = gOffsets.iGetPlayerCond(pEntity);

	iFailed = 2;

	CBaseEntity *pBaseEntity = GetBaseEntity( me );

	CBaseEntity *pBaseWeapon = gOffsets.pGetBaseCombatActiveWeapon(pBaseEntity);

	iFailed = 3;

	if ( pBaseWeapon == NULL || pEntity == NULL )
	{
		return gOffsets.vecVelocity(pBaseEntity);
	}

	bool bOnGround = (gOffsets.iGetFlags( pEntity ) & FL_ONGROUND);

	vecPos = vecGetHitbox(pEntity, iGetHitbox(gPlayerVars.iWeaponID, bOnGround ) );
    
    float flGravity = gConVars.sv_gravity->GetFloat();
	
	iFailed = 4;

	Vector vecDistance = vecPos - gPlayerVars.vecEyePos;

    float flImpactTime = TimeOfImpact( vecDistance.Length(), flGetProjectileSpeed( ), pEntity, vecPos );

	iFailed = 5;

	gMiscFuncs.EstimateAbsVelocity(pEntity, vecVelocity);

	iFailed = 6;

    if ( !bOnGround )
	{
        vecVelocity.z = ( vecVelocity.z - ( flGravity * SQUARE( flImpactTime ) ) ) * 0.5f;
	}
	else
	{
		vecVelocity.z = vecVelocity.z * SQUARE ( flImpactTime );
	}

	iFailed = 7;
	
	Vector vAdjusted( vecVelocity.x * flImpactTime, vecVelocity.y * flImpactTime, vecVelocity.z );

	if ( gPlayerVars.iWeaponID == WPN_Huntsman )
    {
		//vAdjusted[2] += SQUARE(vecDistance.Length() * flGetHuntsmanDrop( timeGetTime() - gPlayerVars.dwStartHuntsmanTime ));
    }

	iFailed = 8;

	if ( gPlayerVars.iWeaponID == WPN_Crossbow || gPlayerVars.iWeaponID == WPN_GrenadeLauncher || gPlayerVars.iWeaponID == WPN_NewGrenadeLauncher || gPlayerVars.iWeaponID == WPN_LochNLoad )
    {
		vAdjusted[2] += GetDrop(vecDistance.Length(), flGetProjectileSpeed( ), pEntity, vecPos );
    }

	if ( gPlayerVars.iWeaponID == WPN_Flaregun || gPlayerVars.iWeaponID == WPN_Detonator )
    {
		vAdjusted[2] += GetDrop(vecDistance.Length(), flGetProjectileSpeed( ), pEntity, vecPos ) * 0.5;
    }

	iFailed = 9;

	return vecPos + vAdjusted;
	}
	catch(...)
	{
		Vector vecNull;
		return vecNull;
	}
}*/
//===================================================================================
/*float CAimbot::flGetHuntsmanDrop ( float flChargeTime )
{
	if(flChargeTime < 100 )
	{
		return 0.0000523943;
	}
	if(flChargeTime < 200)
	{
		return 0.0000549142;
	}
	if(flChargeTime < 300)
	{
		return 0.0000430142;
	}
	if( flChargeTime < 400 )
	{
		return 0.0000354783;
	}
	if( flChargeTime < 500 )
	{
		return 0.0000329128;
	}
	if( flChargeTime < 600 )
	{
		return 0.0000186028;
	}
	if( flChargeTime < 700 )
	{
		return 0.0000149521;
	}
	if( flChargeTime < 800 )
	{
		return 0.0000149933;
	}
	if( flChargeTime < 900 )
	{
		return 0.0000076557;
	}
	if( flChargeTime > 900 )
	{
		return 0.0000056557;
	}
	return 0;
}*/
//===================================================================================
/*float CAimbot::GetWeaponGravity( void )
{
    switch( gPlayerVars.iWeaponID )
    {
		case WPN_Flaregun:
		case WPN_Detonator:
		return 0.66f;
		case WPN_Huntsman:
		return 0.09999999403953552f;
		case WPN_Crossbow:
		return 0.199999f;
		default:
		return 0.5f;
	}
	return 0.5f;
}*/
//===================================================================================
void CAimbot::SilentAimFix( CUserCmd* pCommand, Vector& qaViewAngles )
{
	Vector vecSilent( pCommand->forwardmove, pCommand->sidemove, pCommand->upmove );
	float flSpeed = sqrt( vecSilent.x*vecSilent.x + vecSilent.y*vecSilent.y );
	Vector angMove;
	VectorAngles( vecSilent, angMove );
	float flYaw = DEG2RAD( pCommand->viewangles.y - qaViewAngles.y + angMove.y );
	pCommand->forwardmove = cos( flYaw ) * flSpeed;
	pCommand->sidemove = sin( flYaw ) * flSpeed;
	ClampAngle(pCommand->viewangles);
}
//===================================================================================
void CAimbot::ClampAngle( Vector& qaAng )
{
	while (qaAng[0] > 89)
		qaAng[0] -= 180;

	while (qaAng[0] < -89)
		qaAng[0] += 180;

	while (qaAng[1] > 180)
		qaAng[1] -= 360;

	while (qaAng[1] < -180)
		qaAng[1] += 360;

	qaAng.z = 0;
}
//===================================================================================
float CAimbot::GetInterp( )
{

	int iLine = __LINE__;
	try
	{

	float interp = (gConVars.sv_client_min_interp_ratio->GetFloat() / gConVars.sv_maxupdaterate->GetFloat());


	iLine = __LINE__;

	
	float latency = gInts.Engine->GetNetChannelInfo()->GetAvgLatency( FLOW_INCOMING ) + gInts.Engine->GetNetChannelInfo()->GetAvgLatency( FLOW_OUTGOING ) + gMiscFuncs.GetLerpAmount();

	return ( interp + latency );

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed GetInterp %i", iLine );
		return 0;
	}

}
//===================================================================================
void CAimbot::FindTargetCall ( int iIndex )
{
	if( bIsValidTarget( iIndex ) )
	{
		if (gPlayerVars.bHasFlameThrower)
		{
			if ( bHasTarget() && flAimDistance[m_nTarget] < flAimDistance[iIndex] )
				return;

			if ( flAimDistance[iIndex] < 20 )
			{
				SetTarget( iIndex );
				return;
			}
		}
		else
		{
			if( gPlayerVars.bHasMeleeWeapon )
			{
				if ( flAimDistance[iIndex] < iGetMeleeDistance() ) 
				{
					SetTarget( iIndex );
				}
			}
			else
			{
				switch ( (int)CV( aim_mode ) )
				{
					case 1:
					{
						if (flAimDistance[m_nTarget] < flAimDistance[iIndex] && bHasTarget() )
						{
							return;
						}
						break;
					}
					default:
					{
						if (flAimFOV[m_nTarget] < flAimFOV[iIndex] && bHasTarget() )
						{
							return;
						}
					}
				}
				SetTarget(iIndex);
			}
			return;
		}
	}
}