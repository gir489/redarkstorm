#pragma once

#include "SDK.h"

class CBaseAPI
{
public:
	DWORD dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern);
	HMODULE GetModuleHandleSafe( const char* pszModuleName );
	void LogToFile( const char * pszMessage, ... );
	DWORD GetClientSignature(const char* chPattern);
	DWORD GetEngineSignature(const char* chPattern);
	DWORD GetVguiMatSurfaceSignature(const char* chPattern);
	DWORD GetGameOverlayRendererSignature(const char* chPattern);
	void ErrorBox (const char* error );
	//bool KillProcessByName(char *szProcessToKill);
};

extern CBaseAPI gBaseAPI;