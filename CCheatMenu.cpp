#include "SDK.h"
#include "CDrawManager.h"
#include "CCheatMenu.h"
#include "CAimbot.h"
#include "COffsets.h"

CCheatMenu gCheatMenu;

const char* chKeys[] = {
					"NoKey", 
					"MOUSE1",
					"MOUSE2",
					"MOUSE3",
					"MOUSE4",
					"MOUSE5",
					"LSHIFT",
					"RSHIFT",
					"LCTRL",
					"TAB",
					"F",
					"G",
					"F12",
					"LALT",
					"RALT",
					"LCTRL",
					"RCTRL",
					"ON" };

void CCheatMenu::DoDown()
{
	if( iMenuIndex < gCvars2.iTotalCvars - 1 )
	{
		if ( gCvars2.Cvars[iMenuIndex].iType == type_group && GetValue(iMenuIndex) == 0 )
		{
			iMenuIndex = iNextGroup;
		}
		else
		{
			iMenuIndex++;
		}
	}
	else
	{
		iMenuIndex = 0;
	}
}

void CCheatMenu::DoUp()
{
	if( iMenuIndex > 0 ) 
	{
		if ( gCvars2.Cvars[iMenuIndex].iType == type_group && GetValue(iLastGroup) == 0 )
		{
			iMenuIndex = iLastGroup;
		}
		else
		{
			iMenuIndex--;	
		}
	}
	else 
	{
		iMenuIndex = gCvars2.iTotalCvars - 1;
	}
}

void CCheatMenu::DoLeft( )
{
	if ( gCvars2.Cvars[iMenuIndex].iType == type_esp_rage )
	{
		gCvars2.Cvars[iMenuIndex].iValue--;
		while ( gCvars2.Cvars[iMenuIndex].iValue != 0 && 
			   ( gCvars2.Cvars[iMenuIndex].iValue == me || 
				 gMiscFuncs.bIsValidPlayer(gCvars2.Cvars[iMenuIndex].iValue) == false ||
				 gOffsets.iGetTeamNum(GetBaseEntity(gCvars2.Cvars[iMenuIndex].iValue)) < 2 ||
				 gAimbot.bCheckTeam(gCvars2.Cvars[iMenuIndex].iValue) == false
				))
		{
			int val = gCvars2.Cvars[iMenuIndex].iValue - 1;
			if( val < 0 )
			{
				val = gInts.Globals->maxclients;
			}
			gCvars2.Cvars[iMenuIndex].iValue = val;
		}
	}
	else
	{
		int val = gCvars2.Cvars[iMenuIndex].iValue - gCvars2.Cvars[iMenuIndex].iIncrementer;
		if( val < gCvars2.Cvars[iMenuIndex].iMin )
			val = gCvars2.Cvars[iMenuIndex].iMax;
		gCvars2.Cvars[iMenuIndex].iValue = val;
	}
}

void CCheatMenu::DoRight( )
{
	if ( gCvars2.Cvars[iMenuIndex].iType == type_esp_rage )
	{
		gCvars2.Cvars[iMenuIndex].iValue++;
		while ( gCvars2.Cvars[iMenuIndex].iValue != 0 && 
			   ( gCvars2.Cvars[iMenuIndex].iValue == me || 
				 gMiscFuncs.bIsValidPlayer(gCvars2.Cvars[iMenuIndex].iValue) == false ||
				 gOffsets.iGetTeamNum(GetBaseEntity(gCvars2.Cvars[iMenuIndex].iValue)) < 2 ||
				 gAimbot.bCheckTeam(gCvars2.Cvars[iMenuIndex].iValue) == false
				))
		{
			int val = gCvars2.Cvars[iMenuIndex].iValue + 1;
			if( val > gInts.Globals->maxclients )
			{
				val = 0;
			}
			gCvars2.Cvars[iMenuIndex].iValue = val;
		}
	}
	else
	{	
		int val = gCvars2.Cvars[iMenuIndex].iValue + gCvars2.Cvars[iMenuIndex].iIncrementer;
		if( val > gCvars2.Cvars[iMenuIndex].iMax )
			val = gCvars2.Cvars[iMenuIndex].iMin;
		gCvars2.Cvars[iMenuIndex].iValue = val;
	}
}

int CCheatMenu::GetValue(int index)
{
	if (index > gCvars2.iTotalCvars || index < 0 )
		return 0;

	return gCvars2.Cvars[index].iValue;
}

void CCheatMenu::SetValue(int index, int flValue)
{
	if ( index > gCvars2.iTotalCvars || index < 0 )
		return;

	gCvars2.Cvars[index].iValue = flValue;
}

void CCheatMenu::DrawMenu( )
{	
	static DWORD dwNotHighlighted = 0xFFFFFFFF;
	static DWORD dwHighlighted = 0x0080FFFF;

	int x = 720,
		xx = x + 150,
		y = 200,
		w = 210,
		h = 12;

	bool bGroupFound = false;

	int iTotalItems = 0;
	iLastGroup = 0;
	iNextGroup = 0;

	for( int i = 0; i < gCvars2.iTotalCvars; i++ )
	{
		DWORD dwColorUsed = dwNotHighlighted;
		if ( gCvars2.Cvars[i].iType == type_group )
		{
			iTotalItems++;
			if( i == iMenuIndex )
			{
				dwColorUsed = dwHighlighted;
				gDrawManager->DrawRect( x + 1, y + (11*iTotalItems), w - 2, h, 0xFFFFFF50 );
			}
			if ( GetValue(i) == 1 )
			{
				gDrawManager->DrawStringActual( x + 2, y + (11*iTotalItems), dwColorUsed, "+%s", gCvars2.Cvars[i].chMenuTitle );
				bGroupFound = true;
			}
			else
			{
				gDrawManager->DrawStringActual( x + 2, y + (11*iTotalItems), dwColorUsed, "-%s", gCvars2.Cvars[i].chMenuTitle );
				bGroupFound = false;
			}
			if ( i > iMenuIndex )
			{
				if (iNextGroup == 0)
					iNextGroup = i;
			}
			else if ( i < iMenuIndex )
			{
				iLastGroup = i;
			}
			continue;
		}
		else if ( bGroupFound == true )
		{
			iTotalItems++;
			if( i == iMenuIndex  )
			{
				dwColorUsed = dwHighlighted;
				gDrawManager->DrawRect( x + 1, y + (11*iTotalItems) , w - 2, h, 0xFFFFFF50 );
			}
			gDrawManager->DrawStringActual( x + 2, y + (11*iTotalItems), dwColorUsed, " - %s", gCvars2.Cvars[i].chMenuTitle );
			switch ( gCvars2.Cvars[i].iType )
			{
				case type_key:
				{
					gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, chKeys[GetValue(i)] );
					break;
				}
				case type_aim_method:
				{
					if (GetValue(i))
					{
						gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"DIST");
					}
					else
					{
						gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"FOV");
					}
					break;
				}
				case type_onoff:
				{
					if (GetValue(i))
					{
						gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"ON");
					}
					else
					{
						gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"OFF");
					}
					break;
				}
				case type_aim_team:
				{
					switch (GetValue(i))
					{
						case 1:
							gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"ENMY");
							break;
						case 2:
							gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"FRND");
							break;
						default:
							gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"OFF");
					}
					break;
				}
				case type_esp_rage:
				{
					if (GetValue(i) == 0)
					{
						gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, L"OFF");
					}
					else
					{
						if (gMiscFuncs.bIsValidPlayer(GetBaseEntity(GetValue(i))) )
						{
							const char* chName = gMiscFuncs.GetGameResources()->GetPlayerName(GetValue(i));
							wchar_t szString[1024];
							if(	MultiByteToWideChar(CP_UTF8,0,chName,32,szString,1024))
								gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, szString);
							else
								gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, chName);
						}
						else
						{
							SetValue( iMenuIndex, 0 );
						}
					}
					break;
				}
				default:
				{
					gDrawManager->DrawStringActual( xx, y + (11*iTotalItems), dwColorUsed, "%i", GetValue(i) );
				}
			}
			continue;
		}
	}

	gDrawManager->DrawOutlineRect(x, y - (h + 4), w, (h + 4), 0x1E1E1E80);
	gDrawManager->DrawOutlineRect(x, y - (h + 4), w, (iTotalItems + 1) * 11 + 21, dwHighlighted);
}