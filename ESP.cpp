﻿#include "ESP.h"
#include "SDK.h"
#include "COffsets.h"
#include "CAimbot.h"
#include "CCheatMenu.h"
#include "CDrawManager.h"
#include <list>

#ifdef _DEBUG
extern char chTriggerbotMessage[255];
#endif
const char* szClassNames[] = 
{"",
		"Scout",
		"Sniper",
		"Soldier",
		"Demoman",
		"Medic",
		"Heavy",
		"Pyro",
		"Spy",
		"Engineer"
};

void __fastcall DestoryGlow(CBaseEntity* self, int edx)
{
	VMTManager& hook = VMTManager::GetHook(self);
	static DWORD dwReturnAddress = gBaseAPI.GetClientSignature("8B 75 F8 8B CE E8 ? ? ? ? 8B CE");
	if ((DWORD)_ReturnAddress() != dwReturnAddress)
		hook.GetMethod<void(__thiscall*)(PVOID)>(gVFuncOffsets.iDestoryGlowVFuncOffset)(self);	
}
//===================================================================================
DWORD dwGetColorForAmmo(int iCurAmmo, int iMaxAmmo)
{
	float div = ((float)iCurAmmo / (float)iMaxAmmo);

	if (div > 0.8f)
		return 0x00FF00FF;
	if (div >= 0.5f)
		return 0xFFFF00FF;
	return 0xFF0000FF;
}
//===================================================================================
void __fastcall HookedDestructor(CBaseEntity* self, int edx, int flags)
{
	VMTManager& hook = VMTManager::GetHook(self);
	hook.Kill();
	delete &hook;
	self->Destructor(flags);
}
//===================================================================================
const char* szWeaponName(CBaseEntity* pPlayer)
{
	CBaseEntity *pWeapon = gOffsets.pGetBaseCombatActiveWeapon(pPlayer);

	if (pWeapon == NULL)
		return ""; //This is usually caused by some faggot doing the T-pose glitch.

	int iWeaponID = gOffsets.iGetItemIndex(pWeapon); //AttributeContainer::m_ItemId
	switch(iWeaponID)
	{
		//================================================
		//Scout
		//Primary
		case WPN_FAN: case WPN_FestiveFaN: return "F-a-N";
		case WPN_Shortstop			 : return "Shortstop";
		case WPN_SodaPopper			 : return "SodaPopper";
		case WPN_BabyFaceBlaster	 : return "BabyfaceBlaster";
		case WPN_BackScatter		 : return "BackScatter";
		//Secondary
		case WPN_Bonk				 : return "BonkDrink";
		case WPN_CritCola			 : return "Crit-a-Cola";
		case WPN_Milk: case WPN_MutatedMilk: return "MadMilk";
		case WPN_Winger				 : return "Winger";
		case WPN_PocketPistol		 : return "PocketPistol";
		case WPN_FlyingGuillotine1: case WPN_FlyingGuillotine2: return "FlyingGuillotine";
		//Melee
		case WPN_Sandman			 : return "Sandman";
		case WPN_Cane				 : return "CandyCane";
		case WPN_BostonBasher: case WPN_RuneBlade: return "BostonBasher";
		case WPN_SunStick			 : return "SunStick";
		case WPN_FanOWar			 : return "FanO'War";
		case WPN_Atomizer			 : return "Atomizer";
		case WPN_WrapAssassin		 : return "WrapAssassin";
		//================================================
		//Soldier
		//Primary
		case WPN_DirectHit			 : return "DirectHit";
		case WPN_Original			 : return "Original"; //This wepaon has special properties over the rocketlauncher in that it fires from the center of view instead of the dominant hand of the player.
		case WPN_BlackBox: case WPN_FestiveBlackbox: return "BlackBox";
		case WPN_RocketJumper		 : return "RJumper";
		case WPN_LibertyLauncher	 : return "LibertyLauncher";
		case WPN_CowMangler			 : return "CowMangler";
		case WPN_BeggersBazooka		 : return "BeggersBazooka";
		case WPN_Airstrike			 : return "AirStrike";
		//Secondary
		case WPN_BuffBanner: case WPN_FestiveBuffBanner: return "BuffBanner";
		case WPN_BattalionBackup	 : return "BattalionBackup";
		case WPN_Concheror			 : return "Concheror";
		case WPN_ReserveShooter		 : return "ReserveShooter";
		case WPN_RighteousBison		 : return "RighteousBison";
		case WPN_PanicAttack		 : return "PanicAttack";
		//Melee
		case WPN_Equalizer			 : return "Equalizer";
		case WPN_EscapePlan			 : return "EscapePlan";
		case WPN_PainTrain			 : return "PainTrain";
		case WPN_Katana				 : return "Katana";
		case WPN_MarketGardener		 : return "MarketGardner";
		case WPN_DisciplinaryAction	 : return "DisciplinaryAction";
		//================================================
		//Pyro
		//Primary
		case WPN_Backburner: case WPN_FestiveBackburner: return "Backburner";
		case WPN_Degreaser			 : return "Degreaser";
		case WPN_Phlogistinator		 : return "Phlogistinator";
		case WPN_DragonsFury		 : return "DragonsFury";
		//Secondary
		case WPN_Flaregun: case WPN_FestiveFlaregun: return "FlareGun";
		case WPN_Detonator			 : return "Detonator";
		case WPN_ManMelter			 : return "ManMelter";
		case WPN_ScorchShot			 : return "ScorchShot";
		case WPN_ThermalThruster	 : return "ThermalThruster";
		case WPN_GasPasser			 : return "GasPasser";
		//Melee
		case WPN_Axtingusher: case WPN_Mailbox: case WPN_FestiveAxtwingisher: return "Axtinguisher";
		case WPN_HomeWrecker: case WPN_Maul: return "Homewrecker";
		case WPN_PowerJack			 : return "PowerJack";
		case WPN_Backscratcher		 : return "BackScratcher";
		case WPN_VolcanoFragment	 : return "VolcanoFrag";
		case WPN_ThirdDegree		 : return "ThirdDegree";
		case WPN_NeonAnnihilator1 : case WPN_NeonAnnihilator2: return "NeonAnnihilator";
		case WPN_HotHand			 : return "HotHand";
		//================================================
		//Demoman
		//Primary
		case WPN_LochNLoad			 : return "Loch-n-Load";
		case WPM_LoooseCannon		 : return "LooseCannon";
		case WPN_IronBomber			 : return "IronBomber";
		//Secondary
		case WPN_ScottishResistance	 : return "ScottishResistance";
		case WPN_StickyJumper		 : return "SJumper";
		case WPN_QuickieBombLauncher : return "QuickeLauncher";
		//Melee
		case WPN_Sword: case WPN_FestiveEyelander: case WPN_Golfclub: case WPN_Headless: return "Sword";
		case WPN_ScottsSkullctter	 : return "SkullCutter";
		case WPN_Fryingpan           : return "FryingPan";
		case WPN_Ullapool			 : return "UllaPool";
		case WPN_Claidheamhmor		 : return "ClaidheamhMor";
		case WPN_PersainPersuader	 : return "PersainPersuader";
		//================================================
		//Heavy
		//Primary
		case WPN_Natascha			 : return "Natasha";
		case WPN_BrassBeast			 : return "BrassBeast";
		case WPN_Tomislav			 : return "Tomislav";
		case WPN_HuoLongHeatmaker1: case WPN_HuoLongHeatmaker2: return "HuoLongHeater";
		//Secondary
		case WPN_Sandvich: case WPN_RobotSandvich: case WPN_FestiveSandvich: return "Sandvich";
		case WPN_Fishcake: case WPN_CandyBar: return "DalokohsBar";
		case WPN_Steak				 : return "Steak";
		case WPN_FamilyBuisness		 : return "FamilyBuisness";
		case WPN_Banana				 : return "Banana";
		//Melee
		case WPN_KGB				 : return "KGB";
		case WPN_GRU : case WPN_FestiveGRU : case WPN_BreadBite : return "GRU";
		case WPN_WarriorSpirit		 : return "WarriorSpirit";
		case WPN_FistsOfSteel		 : return "FistsOfSteel";
		case WPN_EvictionNotice		 : return "EvictionNotice";
		case WPN_ApocoFists			 : return "ApocoFists";
		case WPN_HolidayPunch		 : return "HolidayPunch";
		//================================================
		//Engineer
		//Primary
		case WPN_FrontierJustice: case WPN_FestiveFrontierJustice: return "FrontierJustice";
		case WPN_Widowmaker			 : return "Widowmaker";
		case WPN_Pomson				 : return "Pomson";
		case WPN_RescueRanger		 : return "RescueRanger";
		//Secondary
		case WPN_Wrangler: case WPN_FestiveWrangler: case WPN_GeigerCounter: return "Wrangler";
		case WPN_ShortCircut		 : return "ShortCircut";
		//Melee
		case WPN_Gunslinger			 : return "RobotHand";
		case WPN_SouthernHospitality : return "SouthernHospitality";
		case WPN_Jag				 : return "Jag";
		case WPN_EurekaEffect		 : return "EurekaEffect";
		//================================================
		//Medic
		//Primary
		case WPN_Blutsauger			 : return "Blutsauger";
		case WPN_Crossbow: case WPN_FestiveCrossbow: return "Crossbow";
		case WPN_Overdose			 : return "Overdose";
		//Secondary
		case WPN_Kritzkrieg		 	 : return "Kritzkrieg";
		case WPN_QuickFix			 : return "QuickFix";
		case WPN_Vaccinator			 : return "Vaccinator";
		//Melee
		case WPN_Ubersaw: case WPN_FestiveUbersaw: return "Ubersaw";
		case WPN_Vitasaw			 : return "Vitasaw";
		case WPN_Amputator			 : return "Amputator";
		case WPN_Solemnvow			 : return "SolemnVow";
		//================================================
		//Sniper
		//Primary
		case WPN_Huntsman: case WPN_FestiveHuntsman: case WPN_CompoundBow: return "Huntsman";
		case WPN_SydneySleeper		 : return "SydneySleeper";
		case WPN_Bazaarbargain		 : return "Bazaarbargain";
		case WPN_Machina : case WPN_ShootingStar: return "Machina";
		case WPN_HitmanHeatmaker	 : return "HitmanHeatmaker";
		case WPN_ClassicSniperRifle	 : return "ClassicSniper";
		//Secondary
		case WPN_Jarate: case WPN_FestiveJarate: case WPN_SelfAwareBeautyMark: return "Jarate";
		case WPN_CleanersCarbine	 : return "CleanersCarbine";
		//Melee
		case WPN_TribalmansShiv		 : return "TribalsmansShiv";
		case WPN_Bushwacka	 		 : return "Bushwacka";
		case WPN_Shahanshah			 : return "Shahanshah";
		//================================================
		//Spy
		//Primary
		case WPN_Ambassador: case WPN_FestiveAmbassador: return "Ambassador";
		case WPN_Letranger			 : return "Letranger";
		case WPN_Enforcer			 : return "Enforcer";
		case WPN_Diamondback		 : return "Diamondback";
		//Melee
		case WPN_WangaPrick: case WPN_EternalReward: return "EternalReward";
		case WPN_Kunai				 : return "Kunai";
		case WPN_BigEarner			 : return "BigEarner";
		case WPN_Spycicle			 : return "SpyCicle";
		//Sapper
		case WPN_RedTape1: case WPN_RedTape2: return "TapeRecorder";
		default: //For Stock/Unknown weapons
		{
			int iClassID = gOffsets.iGetPlayerClass(pPlayer); //TFPlayerClassShared::m_iClass
			int iSlotID = pWeapon->GetWeaponSlot(); //CBaseCombatWeapon::GetSlot
			switch (iClassID)
			{
				case TF2_Scout:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "Scattergun";
						case TF_WEAPONSLOT_SECONDARY:
							return "Pistol";
						case TF_WEAPONSLOT_MELEE:
							return "Bat";
					}
				}
				case TF2_Soldier:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "RocketLauncher";
						case TF_WEAPONSLOT_SECONDARY:
							return "Shotgun";
						case TF_WEAPONSLOT_MELEE:
							return "Shovel";
					}
				}
				case TF2_Pyro:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "FlameThrower";
						case TF_WEAPONSLOT_SECONDARY:
							return "Shotgun";
						case TF_WEAPONSLOT_MELEE:
							return "Axe"; //m8 can I axe you a question?
					}
				}
				case TF2_Demoman:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "PipeLauncher";
						case TF_WEAPONSLOT_SECONDARY:
							return "StickyLauncher";
						case TF_WEAPONSLOT_MELEE:
							return "Bottle";
					}
				}
				case TF2_Heavy:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "Minigun";
						case TF_WEAPONSLOT_SECONDARY:
							return "Shotgun";
						case TF_WEAPONSLOT_MELEE:
							return "Fists";
					}
				}
				case TF2_Engineer:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "Shotgun";
						case TF_WEAPONSLOT_SECONDARY:
							return "Pistol";
						case TF_WEAPONSLOT_MELEE:
							return "Wrench";
						case TF_WEAPONSLOT_PDA1:
						case TF_WEAPONSLOT_PDA2:
							return "PDA";
						case TF_WEAPONSLOT_BUILDING:
							return "Toolbox";
					}
				}
				case TF2_Medic:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "SyringeGun";
						case TF_WEAPONSLOT_SECONDARY:
							return "Medigun";
						case TF_WEAPONSLOT_MELEE:
							return "Bonesaw";
					}
				}
				case TF2_Sniper:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "SniperRifle";
						case TF_WEAPONSLOT_SECONDARY:
							return "SMG";
						case TF_WEAPONSLOT_MELEE:
							return "Kukri";
					}
				}
				case TF2_Spy:
				{
					switch (iSlotID)
					{
						case TF_WEAPONSLOT_PRIMARY:
							return "Revolver";
						case TF_WEAPONSLOT_SECONDARY:
							return "Sapper";
						case TF_WEAPONSLOT_MELEE:
							return "Knife";
						case TF_WEAPONSLOT_PDA1:
							return "DisguiseKit";
					}
				}
			}
		}
		return "";
	}
}
//===================================================================================
bool sortTeamStruct(const teamStruct& first, const teamStruct& second)
{
	if (first.iScore == second.iScore)
		return first.iEntID > second.iEntID;
	return first.iScore > second.iScore;
}
//===================================================================================
int getHeightForScoreboard(int height, int playerId)
{
	if (me == playerId && gDrawManager->GetScreenSizeHeight() == 1080)
		return height + 1;
	return height;
}
//===================================================================================
void DrawESP( )
{
	gPlayerVars.Sentry = NULL;
	gPlayerVars.Sticky = NULL;
	
	int iClass;
	CBaseEntity* pLocalBaseEntity = GetBaseEntity(me);

	if( pLocalBaseEntity == NULL )
		return;

	#ifdef LOG_ENT
		Vector vScreen, vWorldPos;
		for( int iIndex = 1; iIndex <= gInts.EntList->GetHighestEntityIndex( ); iIndex++ )
		{
			CBaseEntity* pBaseEntity = GetBaseEntity( iIndex );
	
			if( pBaseEntity == NULL )
				continue;	
	
			pBaseEntity->GetWorldSpaceCenter( vWorldPos );
			if (!gDrawManager->WorldToScreen( vWorldPos, vScreen ))
				continue;
			gDrawManager->DrawString( vScreen.x, vScreen.y, 0xFFFFFFFF, pBaseEntity->GetClientClass( )->chName );
			static bool bLogEnts;
			if (!bLogEnts)
			{
				gBaseAPI.LogToFile("Begin logging Classes");
				for ( ClientClass* pCC = gInts.Client->GetAllClasses(); pCC; pCC = pCC->pNextClass )
				{
					gBaseAPI.LogToFile( "Class: %s Recv: %s ID: %i", pCC->chName, pCC->Table->GetName(), pCC->iClassID );
				} 
				gBaseAPI.LogToFile("End of logging Classes");
				bLogEnts = true;
			}
		}
	}
	#endif
	
	#ifndef LOG_ENT

	int iLocalTeamNum = gOffsets.iGetTeamNum( pLocalBaseEntity );

	if (!gPlayerVars.bInScore)
	{
		if (gPlayerVars.iClass == TF2_Soldier && !gPlayerVars.bHasMeleeWeapon)
		{
			CBaseEntity* pBaseRocketLauncher = gOffsets.pGetBaseCombatWeapon(gInts.EntList->GetClientEntity(me), 0);
			CBaseEntity* pBaseShotgun = gOffsets.pGetBaseCombatWeapon(gInts.EntList->GetClientEntity(me), 1);
			if (pBaseShotgun && pBaseRocketLauncher)
			{
				int iRockets = gOffsets.iGetCurrentClipAmmo(pBaseRocketLauncher), iShells = gOffsets.iGetCurrentClipAmmo(pBaseShotgun);
				static Data rocketXcoordsArray[] = { {2560,2345},{1920,1760},{1280,1175} };
				static Data rocketYcoordsArray[] = { {1440,1376},{1080,1030},{720,687} };
				static int xCoord = gDrawManager->InterpolateScreenValues(rocketXcoordsArray, sizeof(rocketXcoordsArray) / sizeof(Data), gDrawManager->GetScreenSizeWidth());
				static int yCoord = gDrawManager->InterpolateScreenValues(rocketYcoordsArray, sizeof(rocketYcoordsArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
				static int firstOffset = gDrawManager->GetPixelTextSize(L" R:");
				static int secondOffset = gDrawManager->GetPixelTextSize(L" 0");
				static int thirdOffset = gDrawManager->GetPixelTextSize(L" S:");
				gDrawManager->DrawString(xCoord, yCoord, 0xFFFFFFFF, L"R: ");
				gDrawManager->DrawString(xCoord + firstOffset, yCoord, dwGetColorForAmmo(iRockets, pBaseRocketLauncher->GetMaxClip()), "%i", iRockets);
				gDrawManager->DrawString(xCoord + firstOffset + secondOffset, yCoord, 0xFFFFFFFF, L"S: ");
				gDrawManager->DrawString(xCoord + firstOffset + secondOffset + thirdOffset, yCoord, dwGetColorForAmmo(iShells, pBaseShotgun->GetMaxClip()), "%i", iShells);
			}
		}
	}
	else
	{
		gDrawManager->blueTeam.clear();
		gDrawManager->redTeam.clear();
		IGameResources* gameResources = gMiscFuncs.GetGameResources();
		for (int iIndex = 1; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++)
		{	
			if (!gameResources->IsConnected(iIndex))
				continue;

			if (gameResources->GetTeam(iIndex) == TEAM_BLUE)
				gDrawManager->blueTeam.push_back({ iIndex, gOffsets.GetScore(iIndex) });
			else if (gameResources->GetTeam(iIndex) == TEAM_RED)
				gDrawManager->redTeam.push_back({ iIndex, gOffsets.GetScore(iIndex) });
		}
		gDrawManager->redTeam.sort(sortTeamStruct);
		gDrawManager->blueTeam.sort(sortTeamStruct);

		std::list<teamStruct>::iterator it;
		static Data heightArray[] = { { 720,155 },{ 1080,221 },{ 1440,292 } };
		static int heightOffset = gDrawManager->InterpolateScreenValues(heightArray, sizeof(heightArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data scoreboardOffsetArray[] = { { 720,34 },{ 1080,51 },{ 1440,69 } };
		static int scoreboardOffset = gDrawManager->InterpolateScreenValues(scoreboardOffsetArray, sizeof(scoreboardOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data blueTeamScoreOffsetArray[] = { { 720,495 },{ 1080,743 },{ 1440,990 } };
		static int blueTeamScoreOffset = gDrawManager->InterpolateScreenValues(blueTeamScoreOffsetArray, sizeof(blueTeamScoreOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data blueTeamDeathOffsetArray[] = { { 720,515 },{ 1080,774 },{ 1440,1030 } };
		static int blueTeamDeathOffset = gDrawManager->InterpolateScreenValues(blueTeamDeathOffsetArray, sizeof(blueTeamDeathOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data blueTeamSpawnTimeOffsetArray[] = { { 720,515 },{ 1080,684 },{ 1440,850 } };
		static int blueTeamSpawnTimeOffset = gDrawManager->InterpolateScreenValues(blueTeamSpawnTimeOffsetArray, sizeof(blueTeamSpawnTimeOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		int height = heightOffset;
		for (it = gDrawManager->blueTeam.begin(); it != gDrawManager->blueTeam.end(); ++it)
		{
			teamStruct ts = *it;
			DWORD dwColor = 0x9ACDFFFF;
			if (!gameResources->IsAlive(ts.iEntID))
			{
				dwColor = (ts.iEntID == me) ? 0x7C9ABCFF : 0x506182FF;
				float flSpawnTime = gOffsets.GetNextRespawnTime(ts.iEntID) - gInts.Globals->curtime;
				if (flSpawnTime > 0)
					gDrawManager->DrawStringWithFont(blueTeamSpawnTimeOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%.0f", flSpawnTime);
			}
			gDrawManager->DrawStringWithFont(blueTeamScoreOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%i", gameResources->GetPlayerScore(ts.iEntID));
			gDrawManager->DrawStringWithFont(blueTeamDeathOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%i", gameResources->GetDeaths(ts.iEntID));
			height += scoreboardOffset;
		}
		height = heightOffset;
		static Data redTeamScoreOffsetArray[] = { { 720,975 },{ 1080,1462 },{ 1440,1950 } };
		static int redTeamScoreOffset = gDrawManager->InterpolateScreenValues(redTeamScoreOffsetArray, sizeof(redTeamScoreOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data redTeamDeathOffsetArray[] = { { 720,997 },{ 1080,1496 },{ 1440,1995 } };
		static int redTeamDeathOffset = gDrawManager->InterpolateScreenValues(redTeamDeathOffsetArray, sizeof(redTeamDeathOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		static Data redTeamSpawnTimeOffsetArray[] = { { 720,935 },{ 1080,1402 },{ 1440,1870 } };
		static int redTeamSpawnTimeOffset = gDrawManager->InterpolateScreenValues(redTeamSpawnTimeOffsetArray, sizeof(redTeamSpawnTimeOffsetArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
		for (it = gDrawManager->redTeam.begin(); it != gDrawManager->redTeam.end(); ++it)
		{
			teamStruct ts = *it;
			DWORD dwColor = 0xFF3E3EFF;
			if (!gameResources->IsAlive(ts.iEntID))
			{
				dwColor = (ts.iEntID == me) ? 0xB74A4AFF : 0x885252FF;
				float flSpawnTime = gOffsets.GetNextRespawnTime(ts.iEntID) - gInts.Globals->curtime;
				if (flSpawnTime > 0)
					gDrawManager->DrawStringWithFont(redTeamSpawnTimeOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%.0f", flSpawnTime);
			}
			gDrawManager->DrawStringWithFont(redTeamScoreOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%i", gameResources->GetPlayerScore(ts.iEntID));
			gDrawManager->DrawStringWithFont(redTeamDeathOffset, getHeightForScoreboard(height, ts.iEntID), gDrawManager->m_ScoreboardPlayerFont, dwColor, "%i", gameResources->GetDeaths(ts.iEntID));
			height += scoreboardOffset;
		}
	}

	for( int iIndex = 1; iIndex <= gInts.EntList->GetHighestEntityIndex( ); iIndex++ )
	{
		int iCrash = __LINE__;
		try
		{
			if ( iIndex <= gInts.Globals->maxclients )
			{
				#pragma region "Player"
				if( iIndex == me )
				{
					continue;
				}

				CBaseEntity* pBaseEntity = GetBaseEntity( iIndex );

				iCrash = __LINE__;

				if( (DWORD)pBaseEntity == NULL || pBaseEntity->GetClientClass()->iClassID != gEntIDs.iCTFPlayerID )
				{
					continue;
				}

				int iTeamNum = gOffsets.iGetTeamNum( pBaseEntity );

				iCrash = __LINE__;

				if( pBaseEntity->IsDormant() )
				{
					continue;
				}

				DWORD dwTeamColor = gDrawManager->dwGetTeamColor(iTeamNum);
				Vector vScreen, vWorldPos;

				iCrash = __LINE__;

				if( (gOffsets.bGetIsAlive(pBaseEntity) == true) && (gMiscFuncs.bIsValidPlayer(pBaseEntity) == true) )	
				{
					bool bIsFriend = ( gMiscFuncs.bHasFriend(pBaseEntity) );

					if( CV( esp_radar ) && bIsFriend == false )
					{
						gRadar.DrawRadarBlip( pBaseEntity, dwTeamColor );
					}

					iCrash = __LINE__;

					iClass = gOffsets.iGetPlayerClass( pBaseEntity );
					int iPlayerCond = gOffsets.iGetPlayerCond(pBaseEntity);
					int iPlayerCondEx = gOffsets.iGetPlayerCondEx(pBaseEntity);
					int iPlayerCondEx2 = gOffsets.iGetPlayerCondEx2(pBaseEntity);

					pBaseEntity->GetWorldSpaceCenter(vWorldPos);

					iCrash = __LINE__;

					if ( ( iPlayerCond & TFCond_Cloaked ) && ( iTeamNum != iLocalTeamNum ) )
					{
						dwTeamColor = 0xFFFFFFEE;
					}

					if ( CV(esp_friends) )
					{
						if ( bIsFriend )
						{
							dwTeamColor = ( IS_BLU( iTeamNum ) ) ? 0x00FFFFFF : 0xFF2C2CFF;
						}
					}

					if ( iClass == TF2_Spy && iTeamNum != iLocalTeamNum )
					{
						if (CV(misc_spywarning))
						{
							if (gAimbot.flGetFOV(vWorldPos ) > 55 )
							{
								if (gAimbot.flGetDistance( pBaseEntity->GetAbsOrigin() ) < 25 )
								{
									if (gAimbot.bIsVisible( vWorldPos, pBaseEntity ) )
									{
										if ( ( pBaseEntity->GetAbsOrigin().z > (pLocalBaseEntity->GetAbsOrigin().z - 25 ) ) && ( pBaseEntity->GetAbsOrigin().z < (pLocalBaseEntity->GetAbsOrigin().z + 30 ) ) )
										{
											Vector vLocalOrigin = pLocalBaseEntity->GetAbsOrigin();
											Vector vEnemyOrigin = pBaseEntity->GetAbsOrigin();

											float flDeltaX = vEnemyOrigin.x - vLocalOrigin.x;
											float flDeltaY = vEnemyOrigin.y - vLocalOrigin.y;

											Vector vecViewAngles;
											gInts.Engine->GetViewAngles(vecViewAngles);

											float flYaw = (vecViewAngles.y) * (PI / 180.0);
											float flMainViewAngles_CosYaw = cos(flYaw);
											float flMainViewAngles_SinYaw = sin(flYaw);

											float x = flDeltaY * (-flMainViewAngles_CosYaw) + flDeltaX * flMainViewAngles_SinYaw;
											static int yPos = gDrawManager->GetScreenSizeHeight() / 3;
											static int mainDrawingOffset = (gDrawManager->GetScreenSizeWidth() / 2) - (gDrawManager->GetPixelTextSize(L"SPY INCOMING") / 2);
											static int rightDrawingOffset = (gDrawManager->GetScreenSizeWidth() / 2) + (gDrawManager->GetPixelTextSize(L"SPY INCOMING") / 2);
											static int leftDrawingOffset = (gDrawManager->GetScreenSizeWidth() / 2) - (gDrawManager->GetPixelTextSize(L"SPY INCOMING") / 2) - gDrawManager->GetPixelTextSize(L" >>");
											gDrawManager->DrawString(mainDrawingOffset, yPos, dwTeamColor, L"SPY INCOMING");
											if (x > 0)
												gDrawManager->DrawString(rightDrawingOffset, yPos, dwTeamColor, L" >>");
											else
												gDrawManager->DrawString(leftDrawingOffset, yPos, dwTeamColor, L"<<");
										}
									}
								}
							}
						}
						if ( CV(misc_disguise) )
						{
							if ( gOffsets.iGetPlayerCond(pBaseEntity) & TFCond_Disguised )
							{
								gOffsets.RemovePlayerCondition( pBaseEntity, TFCond_Disguised );
							}
						}
					}

					iCrash = __LINE__;

					if (!gDrawManager->WorldToScreen( vWorldPos, vScreen ))
						continue;

					iCrash = __LINE__;

					if ( (CV(aim_rage) == 1 && CV(esp_rage) == iIndex) || gMiscFuncs.bIsDueleing(pBaseEntity) )
					{
						dwTeamColor = 0x00FF21FF;
					}

					iCrash = __LINE__;

					if( CV(esp_name) )
					{
						iCrash = __LINE__;
						if ( gOffsets.bIsBotPlayer(pBaseEntity) )
						{
							iCrash = __LINE__;
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "**BOT** %s", pBaseEntity->GetPlayerName() );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						else
						{
							wchar_t szString[40];
							iCrash = __LINE__;
							const char* chName = pBaseEntity->GetPlayerName();
							if (chName != NULL)
							{
								iCrash = __LINE__;
								try { MultiByteToWideChar(CP_UTF8, 0, chName, 32, szString, 40); }
								catch (...) { lstrcpyW(szString, L""); }
								iCrash = __LINE__;
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, szString );
								iCrash = __LINE__;
								int score = gMiscFuncs.GetGameResources()->GetPlayerScore(iIndex);
								iCrash = __LINE__;
								int deaths = gMiscFuncs.GetGameResources()->GetDeaths(iIndex);
								iCrash = __LINE__;
								gDrawManager->DrawStringNeedsSizeOfLast( vScreen.x, vScreen.y, 0xFFFFFFFF, " %i/%i", score, deaths);
								iCrash = __LINE__;
								vScreen.y += gDrawManager->GetESPHeight( );
							}
						}
					}

					iCrash = __LINE__;

					if ( CV(esp_steam) )
					{
						if (!gOffsets.bIsBotPlayer(pBaseEntity))
						{
							CSteamID steamId;
							pBaseEntity->GetSteamID(&steamId);
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFFFFFFEE, "STEAM_0:%u:%u", (steamId.m_unAccountID % 2), (steamId.m_unAccountID / 2));
							vScreen.y += gDrawManager->GetESPHeight( );
						}
					}

					iCrash = __LINE__;

					if( CV(esp_class) )
					{
						int iDisguiseClass = gOffsets.iGetPlayerDisguiseClass( pBaseEntity );
						if( iClass == TF2_Spy && iDisguiseClass != TF2_Spy && iDisguiseClass != 0 )
						{
							string Disguise;
							Disguise.append( szClassNames[iClass] );
							if (iDisguiseClass != iClass)
							{
								Disguise.append(" ");
								Disguise.append( szClassNames[iDisguiseClass] );
							}
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, Disguise.c_str() );
						}
						else
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, szClassNames[ iClass ] );
						}
						vScreen.y += gDrawManager->GetESPHeight( );
					}


					iCrash = __LINE__;

					if( CV( esp_weapon ) )
					{
						const char* chWeaponName = szWeaponName(pBaseEntity);
						gDrawManager->DrawString(vScreen.x, vScreen.y, dwTeamColor, chWeaponName);
						if ( iClass == TF2_Medic )
						{
							float flPercent = gOffsets.flGetMedigunCharge( pBaseEntity );
							DWORD dwPercentColor = 0xFF0000FF;
							if (flPercent > 0 && flPercent <= 100 )
							{
								if ( flPercent > 80 )
								{
									dwPercentColor = 0x00FF21FF;
								}
								else if ( flPercent > 25 )
								{
									dwPercentColor = 0xFFD800FF;
								}
							}
							gDrawManager->DrawStringNeedsSizeOfLast( vScreen.x /*+ gDrawManager->GetPixelTextSize(chWeaponName)*/, vScreen.y, dwPercentColor, " %.0f%%", flPercent );
						}
						vScreen.y += gDrawManager->GetESPHeight( );

						iCrash = __LINE__;

						//if (gMiscFuncs.bPlayerHasCrocSet(pBaseEntity))
						//{
						//	gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, /***PISSRIFLESET***/XorStr<0x4F,17,0xED846232>("\x65\x7A\x01\x1B\x00\x07\x07\x1F\x11\x14\x1C\x09\x1E\x08\x77\x74"+0xED846232).s );
						//	vScreen.y += gDrawManager->GetESPHeight( );
						//}
					}
					

					iCrash = __LINE__;

				
					if( CV( esp_health ) ) 
					{
						int iHealth = gOffsets.iGetHealth( pBaseEntity );
						DWORD dwColor = dwTeamColor;
						switch ((int)CV( esp_health))
						{
							case 2:
							{
								if ( iHealth >= 90)
								{
									dwColor = 0x00FF21FF;
								}
								if ( iHealth <= 89 && iHealth >= 50 )
								{
									dwColor = 0xFFD800FF; 
								}
								if ( iHealth <= 49 && iHealth >= 1 )
								{
									dwColor = 0xFF0000FF; 
								}
							}
							case 1:
							{
								gDrawManager->DrawString(vScreen.x, vScreen.y, dwColor, "%i HP", iHealth);
								vScreen.y += gDrawManager->GetESPHeight( );
								break;
							}
						}
					}

					iCrash = __LINE__;

					if (CV(esp_state))
					{
						wstring strState;
						if (iPlayerCondEx & TFCondEx_BulletResistance)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0x0026FFFF, L"B");
							vScreen.x += 8;
						}
						if (iPlayerCondEx & TFCondEx_BulletCharge)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0x0026FFFF, L"C");
							vScreen.x += 8;
						}
						if (iPlayerCondEx & TFCondEx_ExplosiveResistance)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFFD800FF, L"E");
							vScreen.x += 8;
						}
						if (iPlayerCondEx & TFCondEx_ExplosiveCharge)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFFD800FF, L"C");
							vScreen.x += 8;
						}
						if (iPlayerCondEx & TFCondEx_FireResistance)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFF0000FF, L"F");
							vScreen.x += 8;
						}
						if (iPlayerCondEx & TFCondEx_FireCharge)
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFF0000FF, L"C");
							vScreen.x += 8;
						}
						if (iPlayerCond & TFCond_Ubercharged)
						{
							strState.append(L"Ubr");
							if (iPlayerCond & TFCond_UberchargeFading)
								strState.append(L"Fdng");
						}
						if (iPlayerCond & TFCond_Bonked)
							strState.append(L"Bnk");
						if (iPlayerCond & TFCond_Charging)
							strState.append(L"Tar");
						if (iPlayerCond & TFCond_Bleeding)
							strState.append(L"Bld");
						if (iPlayerCond & TFCond_Stunned)
							strState.append(L"Stn");
						if (iPlayerCond & TFCond_OnFire)
							strState.append(L"Fre");
						if (iClass == TF2_Spy)
						{
							if (gOffsets.bIsHoldingDeadRinger(pBaseEntity))
								strState.append(L"Drgr");
							else if (iPlayerCond & TFCond_Cloaked)
							{
								strState.append(L"Clk");
								if (iPlayerCond & TFCond_CloakFlicker)
									strState.append(L"flk");
								else if (gOffsets.iGetFlags(pBaseEntity) & FL_DUCKING)
									strState.append(L"Dck");
							}
						}
						if (gMiscFuncs.bIsCritBoosted(pBaseEntity))
							strState.append(L"Ktz");
						else if (iPlayerCond & TFCond_MiniCrits)
							strState.append(L"MKtz");
						if (iPlayerCondEx & TFCondEx_CritHype || iPlayerCondEx2 & TFCondEx2_Parachute)
							strState.append(L"Jmp");
						if (iPlayerCond & TFCond_Zoomed)
							strState.append(L"Zom");
						if (iPlayerCondEx & TFCondEx_SpeedBuffAlly || iPlayerCond & TFCond_DemoBuff)
							strState.append(L"Spd");
						if (iPlayerCondEx & TFCondEx_UberchargedCanteen)
							strState.append(L"Ubr");
						else if (iPlayerCond & TFCond_Taunting)
							strState.append(L"Tnt");
						if (iPlayerCond & TFCond_MegaHeal)
							strState.append(L"Qkfx");
						if (iPlayerCond & TFCond_Jarated)
							strState.append(L"Jar");
						if (iPlayerCond & TFCond_Milked)
							strState.append(L"Mlk");
						if (iPlayerCond & TFCond_MarkedForDeath || iPlayerCondEx & TFCondEx_MarkedForDeathSilent)
							strState.append(L"Mrkd");
						if (iPlayerCond & TFCond_DefenseBuffed)
							strState.append(L"DBuf");
						if (iPlayerCond & TFCond_RegenBuffed)
							strState.append(L"HBuf");
						if (iPlayerCond & TFCond_Slowed)
							strState.append(L"Slw");
						if (strState.length() > 0)
						{
							wchar_t drawString[50];
							wsprintfW(drawString, L"* %s", strState.c_str());
							gDrawManager->DrawString(vScreen.x, vScreen.y, dwTeamColor, drawString);
							//vScreen.y += gDrawManager->GetESPHeight();
						}
						//gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "PlayerCond: %X PlayerCondEx: %X PlayerCondEx2: %X", iPlayerCond, iPlayerCondEx, iPlayerCondEx2);
						//vScreen.y += gDrawManager->GetESPHeight( );
					}
				}
				#pragma endregion
			}
			else
			{
				if (CV(esp_structure))
				{
					#pragma region "Structures"

					CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

					if( (DWORD)pBaseEntity == NULL )
						continue;

					if ( pBaseEntity->IsDormant() )
						continue;

					int iTeamNum = gOffsets.iGetTeamNum( pBaseEntity );
					DWORD dwTeamColor = gDrawManager->dwGetTeamColor( iTeamNum );

					int iClassID = pBaseEntity->GetClientClass( )->iClassID;

					Vector vScreen, vWorldPos;

					pBaseEntity->GetWorldSpaceCenter( vWorldPos );

					if ( iClassID == gEntIDs.iIntelID )
					{
						if( gOffsets.iGetFlagStatus( pBaseEntity ) == 2 )
						{
							if ( gOffsets.iGetFlagType(pBaseEntity) != 1 )
							{
								float flResetTime =  gOffsets.flGetFlagResetTime( pBaseEntity );
								float left = flResetTime - gInts.Globals->curtime + 1;
								int iDeviation = IS_BLU(iTeamNum) ? -290 : 255;
								gDrawManager->DrawString( (gDrawManager->GetScreenSizeWidth() * .5) + iDeviation, gDrawManager->GetScreenSizeHeight() - 126, dwTeamColor, "[%02.0f]", left );
							}
						}
					}

					if (iClassID == gEntIDs.iCTFAmmoPack)
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if (gAimbot.flGetDistance(pBaseEntity->GetAbsOrigin()) < 25)
							gDrawManager->DrawString(vScreen.x, vScreen.y, 0xFFFFFFEE, L"A");
						continue;
					}

					if( iClassID == gEntIDs.iIntelID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						int iFlagStatus = gOffsets.iGetFlagStatus( pBaseEntity );
						if ( iFlagStatus == 1 )
						{
							if ( gOffsets.GetIntelligenceOwner(pBaseEntity) == pLocalBaseEntity )
							{
								continue;
							}
						}
						if ( iFlagStatus )
						{
							switch ( gOffsets.iGetFlagType(pBaseEntity) )
							{
							case 1:
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**Bomb**" );
								break;
							case 4:
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**Australium**" );
								break;
							default:
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**Intel**" );
							}
						}
						continue;
					}

					if ( iClassID == 1 ) //CBaseAnimating
					{
						PDWORD pModel = pBaseEntity->GetModel();
						if ( pModel != NULL )
						{
							const char* chName = gInts.ModelInfo->GetModelName(pModel);
							if ( *(char*)(chName + 13) == 'p' && *(char*)(chName + 14) == 'l' && *(char*)(chName + 15) == 'a' )
							{
								if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
								gDrawManager->DrawString( vScreen.x - 26, vScreen.y - 6, 0x00FF00FF, L"Sandwich");
							}
						}
						continue;
					}

					if (iClassID == gEntIDs.iCurrencyPackID)
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString(vScreen.x - 5, vScreen.y - 6, 0x267F00FF, "{M}");
					}

					if ( iClassID == gEntIDs.iOrnamentID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 5, vScreen.y - 6, dwTeamColor, L"[&]" );
						continue;
					}

					if( iClassID == gEntIDs.iBaseballID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 9, vScreen.y - 6, dwTeamColor, L"[#]" );
						continue;
					}
										
					if( iClassID == gEntIDs.iGrenadeID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if (gOffsets.bIsGrenadeCrit(pBaseEntity))
						{
							dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
						}
						int iStickyType = gOffsets.iGetStickyType(pBaseEntity);
						if ( iStickyType )
						{
							if (iStickyType == 1)
							{
								if (gOffsets.bIsScottishResistanceBomb(pBaseEntity))
								{
									gDrawManager->DrawString( vScreen.x - 10, vScreen.y - 6, dwTeamColor, L"[+]" );
								}
								else
								{
									gDrawManager->DrawString( vScreen.x - 9, vScreen.y - 6, dwTeamColor, L"[*]" );
								}
							}
							if (iStickyType == 2)
							{
								gDrawManager->DrawString( vScreen.x - 12, vScreen.y - 6, dwTeamColor, L"SJ" );
							}
							if (iStickyType == 3)
							{
								gDrawManager->DrawString( vScreen.x - 3, vScreen.y - 6, dwTeamColor, L"O" );
							}
						}
						else
						{
							gDrawManager->DrawString( vScreen.x - 12, vScreen.y - 6, dwTeamColor, L"[=])");
						}
						continue;
					}

					if( iClassID == gEntIDs.iRocketID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if ( gOffsets.bIsRocketJumperRocket(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x - 10, vScreen.y - 6, dwTeamColor, L"RJ" );
						}
						else
						{
							if (gOffsets.bIsRocketCrit(pBaseEntity))
							{
								dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
							}
							gDrawManager->DrawString( vScreen.x - 20, vScreen.y - 6, dwTeamColor, L"[==]>" );
						}
						continue;
					}

					if( iClassID == gEntIDs.iSentryRocketID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 7, vScreen.y - 6, dwTeamColor, L"[-]" );
						continue;
					}

					if( iClassID == gEntIDs.iJarateID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 8, vScreen.y - 6, dwTeamColor, L"[J]" );
						continue;
					}
					
					if( iClassID == gEntIDs.iMilkID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 10, vScreen.y - 6, dwTeamColor, L"[M]" );
						continue;
					}

					if( iClassID == gEntIDs.iFlareID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if (gOffsets.bIsFlareCrit(pBaseEntity))
						{
							dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
						}
						gDrawManager->DrawString( vScreen.x - 8, vScreen.y - 6, dwTeamColor, L"[F]" );
						continue;
					}

					if( iClassID == gEntIDs.iArrowID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if ( gOffsets.vecVelocity(pBaseEntity) != pBaseEntity->GetAbsOrigin() )
						{
							if (gOffsets.bIsArrowCrit(pBaseEntity) || gOffsets.bIsArrowAlight(pBaseEntity))
							{
								dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
							}
							gOffsets.SetVecVelocity(pBaseEntity, pBaseEntity->GetAbsOrigin() );
							gDrawManager->DrawString( vScreen.x - 12, vScreen.y - 6, dwTeamColor, L"[->]");
						}
						continue;
					}
	
					if( iClassID == gEntIDs.iCleverID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if ( gOffsets.bIsTouched(pBaseEntity) == 0)
						{
							if (gOffsets.bIsGrenadeCrit(pBaseEntity))
							{
								dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
							}
							gOffsets.SetVecVelocity(pBaseEntity, pBaseEntity->GetAbsOrigin() );
							gDrawManager->DrawString( vScreen.x - 12, vScreen.y - 6, dwTeamColor, L"[--=]");
						}
						continue;
					}

					if( iClassID == gEntIDs.iEnergyBall )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if (gOffsets.bIsEnergyCharged(pBaseEntity))
						{
							dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
						}
						gDrawManager->DrawString( vScreen.x - 8, vScreen.y - 6, dwTeamColor, L"[E]");
						continue;
					}
					
					if( iClassID == gEntIDs.iMedicArrowID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if ( gOffsets.vecVelocity(pBaseEntity) != pBaseEntity->GetAbsOrigin() )
						{
							if ( gOffsets.bIsArrowCrit(pBaseEntity) )
							{
								dwTeamColor = ( IS_BLU(iTeamNum) ) ? 0x00AEFFFF : 0xFF0C00FF;
							}
							gOffsets.SetVecVelocity(pBaseEntity, pBaseEntity->GetAbsOrigin() );
							gDrawManager->DrawString( vScreen.x - 15, vScreen.y - 6, dwTeamColor, L"[=>]" );
						}
						continue;
					}

					if( iClassID == gEntIDs.iSniperDotID && (iTeamNum != iLocalTeamNum) )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						gDrawManager->DrawString( vScreen.x - 6, vScreen.y - 6, dwTeamColor, L"●" );
						continue;
					}
					
					if( iClassID == gEntIDs.iSentryID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if (gOffsets.bGetSentryPlayerControlled(pBaseEntity))
						{
							gDrawManager->DrawString(vScreen.x, vScreen.y, dwTeamColor, L"*WRANGLED*");
							vScreen.y += gDrawManager->GetESPHeight();
						}
						else if( gOffsets.bGetBuildingHasSapper(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**SAPPED**" );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						else if ( gOffsets.bIsBuildingDisabled(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**DISABLED**" );
							vScreen.y += gDrawManager->GetESPHeight( );
						}

						int iUpgradeLevel = gOffsets.iGetBuildingUpgradeLevel(pBaseEntity);
						int iBuildingHealth = gOffsets.iGetBuildingHealth( pBaseEntity );

						DWORD dwHealthColor = 0x267F00FF;
						if ( iBuildingHealth <= 99 && iBuildingHealth >= 50 )
						{
							dwHealthColor = 0xA88C00FF;
						}
						else if ( iBuildingHealth <= 49 && iBuildingHealth >= 1 )
						{
							dwHealthColor = 0x7F0000FF;
						}

						if (gOffsets.bIsBuildingMini(pBaseEntity))
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Mini Sentry %i(%i)", gOffsets.iGetSentrygunKills(pBaseEntity), gOffsets.iGetSentryAssists(pBaseEntity) );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						else
						{
							const char* chLevel = "I";
							DWORD dwUpgradeColor = dwTeamColor;
							if ( iUpgradeLevel == 2 )
							{
								chLevel = "II";
								( gOffsets.iGetTeamNum(pBaseEntity) == TEAM_BLUE ) ? dwUpgradeColor = 0xB200FFFF : dwUpgradeColor = 0xA70000FF;
							}
							else if ( iUpgradeLevel == 3 )
							{
								chLevel = "III";
								( gOffsets.iGetTeamNum(pBaseEntity) == TEAM_BLUE ) ? dwUpgradeColor = 0x00FFFFFF : dwUpgradeColor = 0xFF0000FF;
							}
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"Sentry");
							gDrawManager->DrawString( vScreen.x + 41 + 4, vScreen.y, dwUpgradeColor, chLevel );
							gDrawManager->DrawString( vScreen.x + 41 + 4 + (iUpgradeLevel * 5) + 4, vScreen.y, dwTeamColor, "%i(%i)", gOffsets.iGetSentrygunKills(pBaseEntity), gOffsets.iGetSentryAssists(pBaseEntity) );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"Health:" );
						gDrawManager->DrawString( vScreen.x + 48, vScreen.y, dwHealthColor, "%i", iBuildingHealth );
						vScreen.y += gDrawManager->GetESPHeight( );
					
						if( gOffsets.bGetBuildingIsBuilding(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Building %%%2.0f", gOffsets.flGetBuildingPercentageConstructed(pBaseEntity) * 100 );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						else
						{
							const wchar_t* sSGStatus = L"Unknown";
							switch ( gOffsets.iGetSentrygunState( pBaseEntity ) )
							{
								case 0:
									sSGStatus = L"Placing";
									break;
								case 1:
									sSGStatus = L"Idle";
									break;
								case 2:
									sSGStatus = L"Attacking";
									break;
								case 3:
									sSGStatus = L"Upgrading";
							}
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, sSGStatus );
							vScreen.y += gDrawManager->GetESPHeight( );
						}

						/*if( CV( esp_structure ) == 2 )
						{
							if( iUpgradeLevel != 3 )
							{
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Upgrade: %%%i", (gOffsets.iGetBuildingUpgradeMetal(pBaseEntity) * .5 ) );
								vScreen.y += gDrawManager->GetESPHeight( );
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Shells: %i", gOffsets.iGetSentrygunAmmoShells(pBaseEntity) );
								vScreen.y += gDrawManager->GetESPHeight( );
							}
							else
							{
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Ammo: S:%i R:%i", gOffsets.iGetSentrygunAmmoShells(pBaseEntity), gOffsets.iGetSentrygunAmmoRockets(pBaseEntity) );
								vScreen.y += gDrawManager->GetESPHeight( );
							}	
						}*/
						continue;
					}
								
					if( iClassID == gEntIDs.iTeleporterID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if( gOffsets.bGetBuildingHasSapper(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**SAPPED**" );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						int iUpgradeLevel = gOffsets.iGetBuildingUpgradeLevel(pBaseEntity);

						gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Teleporter %s: %i", ( gOffsets.iGetObjectMode(pBaseEntity) ) ? "Exit" : "Entrance", iUpgradeLevel );
						vScreen.y += gDrawManager->GetESPHeight( );

						DWORD dwHealthColor = dwTeamColor;
						gDrawManager->DrawString( vScreen.x, vScreen.y, dwHealthColor, "HP: %i", gOffsets.iGetBuildingHealth( pBaseEntity ) );
						vScreen.y += gDrawManager->GetESPHeight( );

						if (gOffsets.iGetTeleporterState( pBaseEntity ) == 1)
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"Idle" );
							vScreen.y += gDrawManager->GetESPHeight( );
						}

						if( gOffsets.bGetBuildingIsBuilding(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Building %%%2.0f", (gOffsets.flGetBuildingPercentageConstructed(pBaseEntity) * 100) );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						else
						{
							if( CV( esp_structure ) == 2 )
							{
								const wchar_t* sTStat;
								switch ( gOffsets.iGetTeleporterState( pBaseEntity ) )
								{
									case 0:
										sTStat = L"Placing";
										break;
									case 2:
										sTStat = L"Active";
										break;
									case 4:
										sTStat = L"Teleporting";
										break;
									case 6:
										sTStat = L"Charging";
										break;
									default:
										sTStat = L"Unknown";
								}	
								if( iUpgradeLevel < 3 )
								{
									gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Upgrade: %%%i", (gOffsets.iGetBuildingUpgradeMetal(pBaseEntity) * .5) );
									vScreen.y += gDrawManager->GetESPHeight( );
								}
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, sTStat);
								vScreen.y += gDrawManager->GetESPHeight( );
							}
						}
						continue;
					}
					
					if( iClassID == gEntIDs.iDispenserID )
					{
						if (gDrawManager->WorldToScreen(vWorldPos, vScreen) == false) { continue; }
						if( gOffsets.bGetBuildingHasSapper(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, L"**SAPPED**" );
							vScreen.y += gDrawManager->GetESPHeight( );
						}

						int iUpgradeLevel = gOffsets.iGetBuildingUpgradeLevel(pBaseEntity);

						gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Dispenser: %i", iUpgradeLevel );
						vScreen.y += gDrawManager->GetESPHeight( );

						DWORD dwHealthColor = dwTeamColor;
						gDrawManager->DrawString( vScreen.x, vScreen.y, dwHealthColor, "HP: %i", gOffsets.iGetBuildingHealth( pBaseEntity ) );
						vScreen.y += gDrawManager->GetESPHeight( );						

						if( gOffsets.bGetBuildingIsBuilding(pBaseEntity) )
						{
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Building %%%2.0f", gOffsets.flGetBuildingPercentageConstructed(pBaseEntity) * 100 );
							vScreen.y += gDrawManager->GetESPHeight( );
						}

						if( CV( esp_structure ) == 2 )
						{
							if( iUpgradeLevel < 3 )
							{
								gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Upgrade: %%%i", gOffsets.iGetBuildingUpgradeMetal(pBaseEntity) * .5 );
								vScreen.y += gDrawManager->GetESPHeight( );
							}
							gDrawManager->DrawString( vScreen.x, vScreen.y, dwTeamColor, "Metal: %i", gOffsets.iGetDispenserAmmoMetal(pBaseEntity) );
							vScreen.y += gDrawManager->GetESPHeight( );
						}
						continue;
					}
					#pragma endregion
				}
			}

		}
		catch(...)
		{
			gBaseAPI.LogToFile("ESP Failed: iClass: %i iWeaponID: %i Class: %s Line: %i", iClass, gOffsets.iGetItemIndex(gOffsets.pGetBaseCombatActiveWeapon( GetBaseEntity(iIndex) )), GetBaseEntity(iIndex)->GetClientClass()->chName, iCrash );
		}

	}
	#ifdef _DEBUG
	gDrawManager->DrawString(1920 / 2, 1080 / 2, 0xFFFFFF, chTriggerbotMessage);
	#endif
}
#endif