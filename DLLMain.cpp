#include "SDK.h"

#include "client.h"
#include "COffsets.h"
#include "CDrawManager.h"
#include "CAnnouncer.h"
#include "TF2.h"

Interfaces gInts;

void NewNameChangeCallback(IConVar* pConVar)
{
	static bool bBlock = false;
	if (bBlock == false)
	{
		bBlock = true;
		pConVar->SetActualValue("It's not OK to be gay.");
		bBlock = false;
	}
}

DWORD WINAPI dwMainThread( LPVOID lpArguments )
{
	if (gInts.Client == NULL)
	{
		CreateInterfaceFn ClientFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "client.dll" ), "CreateInterface" );
		XASSERT(ClientFactory);
		gInts.Client = ( CHLClient* )ClientFactory( "VClient017", NULL);
		XASSERT(gInts.Client);
		gInts.EntList = ( CEntList* ) ClientFactory( "VClientEntityList003", NULL );
		XASSERT(gInts.EntList);
		CreateInterfaceFn EngineFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "engine.dll" ), "CreateInterface" );
		XASSERT(EngineFactory);
		gInts.VGUI = ( IEngineVGui* )EngineFactory("VEngineVGui001", NULL );
		XASSERT(gInts.VGUI);
		gInts.Engine = ( CEngineClient* ) EngineFactory( "VEngineClient014", NULL );
		XASSERT(gInts.Engine);
		gInts.ModelInfo = ( CModelInfo* ) EngineFactory( "VModelInfoClient006", NULL );
		XASSERT(gInts.ModelInfo);
		gInts.Trace	= (CEngineTrace*) EngineFactory("EngineTraceClient003", 0);
		XASSERT(gInts.Trace);
		CreateInterfaceFn VGUIFactory = (CreateInterfaceFn) GetProcAddress(gBaseAPI.GetModuleHandleSafe("vguimatsurface.dll"), "CreateInterface");
		XASSERT(VGUIFactory);
		gInts.Surface = ( ISurface* ) VGUIFactory( "VGUI_Surface030", NULL );
		XASSERT(gInts.Surface);
		CreateInterfaceFn VGUI2Factory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "vgui2.dll"), "CreateInterface" );
		XASSERT(VGUI2Factory);

		gDrawManager = new CDrawManager();

		if ( !gInts.Friends )
		{
			typedef SteamClient* (__cdecl *SteamClientFn)(void);
			typedef __int32(__cdecl *GetHSteamUserFn)(void);
			typedef __int32(__cdecl *GetHSteamPipeFn)(void);
			SteamClientFn SteamClientGetter = (SteamClientFn)GetProcAddress(gBaseAPI.GetModuleHandleSafe("steam_api.dll"), "SteamClient");
			GetHSteamUserFn GetHSteamUser = (GetHSteamUserFn)GetProcAddress(gBaseAPI.GetModuleHandleSafe("steam_api.dll"), "SteamAPI_GetHSteamUser");
			GetHSteamPipeFn GetHSteamPipe = (GetHSteamPipeFn)GetProcAddress(gBaseAPI.GetModuleHandleSafe("steam_api.dll"), "SteamAPI_GetHSteamPipe");
			XASSERT(SteamClientGetter);
			XASSERT(GetHSteamUser);
			XASSERT(GetHSteamPipe);

			SteamClient* pSteamClient = SteamClientGetter();
			XASSERT(pSteamClient);

			if (pSteamClient)
			{
				gInts.Friends = pSteamClient->GetISteamFriends(GetHSteamUser(), GetHSteamPipe(), "SteamFriends015");
				XASSERT(gInts.Friends);
				#ifdef _DEBUG
				gBaseAPI.LogToFile("SteamClientGetter [0x%.8X]", (DWORD)SteamClientGetter);
				gBaseAPI.LogToFile("GetHSteamUser [0x%.8X]", (DWORD)GetHSteamUser);
				gBaseAPI.LogToFile("GetHSteamPipe [0x%.8X]", (DWORD)GetHSteamPipe);
				gBaseAPI.LogToFile("pSteamClient [0x%.8X]", (DWORD)pSteamClient);
				gBaseAPI.LogToFile("hSteamUser [0x%.8X]", GetHSteamUser());
				gBaseAPI.LogToFile("hSteamPipe [0x%.8X]", GetHSteamPipe());
				gBaseAPI.LogToFile("g_pSteamFriends [0x%.8X]", (DWORD)gInts.Friends);
				#endif
			}
			else
			{
				gBaseAPI.LogToFile("Failed pSteamClient");
			}
		}

		if ( !gInts.Movement )
		{
			gInts.Movement = (CGameMovement*)ClientFactory( "GameMovement001", 0);
			XASSERT(gInts.Movement);
			#ifdef _DEBUG
			gBaseAPI.LogToFile( "g_pGameMovement [0x%.8X]", (DWORD)gInts.Movement );
			#endif
		}

		if( !gInts.Surface )
		{
			#ifdef _DEBUG
			gBaseAPI.LogToFile( "gInts.Surface [0x%.8X]", (DWORD)gInts.Surface );
			#endif
			XASSERT( gInts.Surface );
		}

		if( !gInts.EventManager )
		{
			gInts.EventManager = ( CGameEventManager2* )EngineFactory( "GAMEEVENTSMANAGER002", NULL );
			#ifdef _DEBUG
			gBaseAPI.LogToFile( "gInts.EventManager: [0x%.8X]", (DWORD)gInts.EventManager );
			#endif
			XASSERT( gInts.EventManager );
		}

#ifdef _DEBUG
		DWORD dwClientBase = (DWORD)gBaseAPI.GetModuleHandleSafe("client.dll");
		DWORD dwEngineBase = (DWORD)gBaseAPI.GetModuleHandleSafe("engine.dll");
#endif

		DWORD dwMaxClipAddress = gBaseAPI.GetClientSignature("8B 92 ? ? ? ? FF D2 85 C0 7E 13");
		XASSERT(dwMaxClipAddress);
		if (dwMaxClipAddress)
		{
			gVFuncOffsets.iMaxClipVFuncOffset = (*(DWORD*)(dwMaxClipAddress + 0x2)) / 4;
#ifdef _DEBUG
			gBaseAPI.LogToFile("iMaxClipVFuncOffset [%i]", gVFuncOffsets.iMaxClipVFuncOffset);
#endif
		}

		DWORD dwSlotAddress = gBaseAPI.GetClientSignature("FF 92 ? ? ? ? 3B 45 08");
		XASSERT(dwSlotAddress);
		if (dwSlotAddress)
		{
			gVFuncOffsets.iSlotVFuncOffset = (*(DWORD*)(dwSlotAddress + 0x2)) / 4;
#ifdef _DEBUG
			gBaseAPI.LogToFile("iSlotVFuncOffset [%i]", gVFuncOffsets.iSlotVFuncOffset);
#endif
		}

		gOffsets.dwIsDuelingLocalPlayerFn = gBaseAPI.GetClientSignature("55 8B EC 83 EC 08 8B 4D 08 8D 55 F8 81 65");
		XASSERT(gOffsets.dwIsDuelingLocalPlayerFn);
		if (gOffsets.dwIsDuelingLocalPlayerFn)
		{
			gVFuncOffsets.iSteamIDOffset = *(PDWORD)(gOffsets.dwIsDuelingLocalPlayerFn + 0x2A) / 4;
#ifdef _DEBUG
			gBaseAPI.LogToFile("gVFuncOffsets.iSteamIDOffset [%i]", gVFuncOffsets.iSteamIDOffset);
			gBaseAPI.LogToFile("dwIsDuelingLocalPlayerFn client.dll+0x%X", gOffsets.dwIsDuelingLocalPlayerFn - dwClientBase);
#endif
		}

		gOffsets.dwCritBoostedFn = gBaseAPI.GetClientSignature("55 8B EC 83 EC 08 56 8B F1 6A 0B");
		XASSERT(gOffsets.dwCritBoostedFn);
#ifdef _DEBUG
		if (gOffsets.dwCritBoostedFn)
		{
			gBaseAPI.LogToFile("dwCritBoostedFn client.dll+0x%X", gOffsets.dwCritBoostedFn - dwClientBase);
		}
#endif

		gOffsets.dwMD5PseudoRandom = gMiscFuncs.dwGetCallFunctionLocation(gBaseAPI.GetClientSignature("E8 ? ? ? ? 83 C4 04 25"));
		XASSERT(gOffsets.dwMD5PseudoRandom);
#ifdef _DEBUG
		if (gOffsets.dwMD5PseudoRandom)
		{
			gBaseAPI.LogToFile("dwMD5PseudoRandom client.dll+0x%X", gOffsets.dwMD5PseudoRandom - dwClientBase);
		}
#endif

		gOffsets.dwEstAbsVelocity = gBaseAPI.GetClientSignature("55 8B EC 83 EC 0C 56 8B F1 E8 ? ? ? ? 3B F0");
		XASSERT(gOffsets.dwEstAbsVelocity);
#ifdef _DEBUG
		if (gOffsets.dwEstAbsVelocity)
		{
			gBaseAPI.LogToFile("dwEstAbsVelocity client.dll+0x%X", gOffsets.dwEstAbsVelocity - dwClientBase);
		}
#endif

		gOffsets.dwGetLerpFn = gBaseAPI.GetClientSignature("55 8B EC A1 ? ? ? ? 83 EC 08 56 A8 01 75 22");
		XASSERT(gOffsets.dwGetLerpFn);
#ifdef _DEBUG
		if (gOffsets.dwGetLerpFn)
		{
			gBaseAPI.LogToFile("dwGetLerpFn client.dll+0x%X", gOffsets.dwGetLerpFn - dwClientBase);
		}
#endif

		gOffsets.dwGetSlotItem = gBaseAPI.GetClientSignature("55 8B EC 83 EC 08 53 56 57 8B 7D 08 33 F6");
		XASSERT(gOffsets.dwGetSlotItem);
#ifdef _DEBUG
		if (gOffsets.dwGetSlotItem)
		{
			gBaseAPI.LogToFile("dwGetSlotItem client.dll+0x%X", gOffsets.dwGetSlotItem - dwClientBase);
		}
#endif

		DWORD dwGameRulesAddress = gBaseAPI.GetClientSignature("A1 ? ? ? ? 80 B8 ? ? ? ? ? 74 6D");
		XASSERT(dwGameRulesAddress);
		gOffsets.dwGameRules = *(PDWORD*)(dwGameRulesAddress + 1);
#ifdef _DEBUG
		gBaseAPI.LogToFile("dwGameRules: client.dll+0x%X", (DWORD)gOffsets.dwGameRules - dwClientBase);
#endif

		gOffsets.dwGetGameResources = gBaseAPI.GetClientSignature("A1 ? ? ? ? 85 C0 74 06 05");
		XASSERT(gOffsets.dwGetGameResources);
#ifdef _DEBUG
		gBaseAPI.LogToFile("gOffsets.dwGetGameResources client.dll+0x%X", (DWORD)gOffsets.dwGetGameResources - dwClientBase);
#endif

		DWORD dwPureLocaiton = gBaseAPI.GetEngineSignature("75 55 8B 0D ? ? ? ? 8B 01 8B 40 18");
		XASSERT(dwPureLocaiton);
		gInts.DemoPlayer = **(CDemoPlayer***)(dwPureLocaiton + 4);
		gOffsets.dwPureReturn = dwPureLocaiton + 0xF;
		VMTBaseManager* demoPlayerHook = new VMTBaseManager();
		demoPlayerHook->Init(gInts.DemoPlayer);
		demoPlayerHook->HookMethod(&Hooked_IsPlayingBack, 6);
		demoPlayerHook->Rehook();

		DWORD dwClientModeAddress = gBaseAPI.GetClientSignature("8B 0D ? ? ? ? 8B 02 D9 05");
		XASSERT(dwClientModeAddress);
		gInts.ClientMode = **(ClientModeShared***)(dwClientModeAddress + 2);
#ifdef _DEBUG
		gBaseAPI.LogToFile("g_pClientModeShared_ptr client.dll+0x%X", (DWORD)gInts.ClientMode - dwClientBase);
#endif

		DWORD dwPlayerResourceLocation = gBaseAPI.GetClientSignature("8B 0D ? ? ? ? 6A 01 8D 91");
		XASSERT(dwPlayerResourceLocation);
		gOffsets.dwTFPlayerResource = *(PDWORD*)(dwPlayerResourceLocation + 2);
#ifdef _DEBUG
		gBaseAPI.LogToFile("g_TF_PR client.dll+0x%X", (DWORD)gOffsets.dwTFPlayerResource - dwClientBase);
#endif

		if (!gInts.Prediction)
		{
			DWORD dwCurrentCommandAddress = gBaseAPI.GetClientSignature("8B 01 FF 50 0C 6A 00 C7 87");
			XASSERT(dwCurrentCommandAddress);
			gOffsets.dwCurrentCommandOffset = *(PDWORD)(dwCurrentCommandAddress + 9);

			DWORD dwRunCommand = gBaseAPI.GetClientSignature("8B 0D ? ? ? ? 8D 86 ? ? ? ? FF 35");
			XASSERT(dwRunCommand);
			gInts.Prediction = **(CPrediction***)(dwRunCommand + 2);
			gOffsets.dwMoveHelper = **(PDWORD**)(dwRunCommand + 14);

#ifdef _DEBUG
			gBaseAPI.LogToFile("g_pPrediction client.dll+0x%X", (DWORD)gInts.Prediction - dwClientBase);
			gBaseAPI.LogToFile("dwMoveHelper client.dll+0x%X", (DWORD)gOffsets.dwMoveHelper - dwClientBase);
			gBaseAPI.LogToFile("dwCurrentCommandOffset [0x%.4X]", (DWORD)gOffsets.dwCurrentCommandOffset);
#endif
		}

		DWORD dwflSpawnTimeAddress = gBaseAPI.GetClientSignature("D9 9F ? ? ? ? 8B 35");
		XASSERT(dwflSpawnTimeAddress);
		gOffsets.dwflSpawnTimeOffset = *(PDWORD)(dwflSpawnTimeAddress + 2);
#ifdef _DEBUG
		gBaseAPI.LogToFile("CBaseEntity::m_flSpawnTime [0x%.X]", gOffsets.dwflSpawnTimeOffset);
#endif

		gOffsets.dwWellGoodInitM8 = gMiscFuncs.dwGetCallFunctionLocation(gBaseAPI.GetClientSignature("E8 ? ? ? ? 83 C4 14 85 C0 74 10 68"));
		XASSERT(gOffsets.dwWellGoodInitM8);
#ifdef _DEBUG
		gBaseAPI.LogToFile("dwWellGoodInitM8 client.dll+0x%X", (DWORD)gOffsets.dwWellGoodInitM8 - dwClientBase);
#endif

		DWORD dwSetKeyValuesLocation = gBaseAPI.GetClientSignature("FF 15 ? ? ? ? 83 C4 08 89 06 8B C6");
		XASSERT(dwSetKeyValuesLocation);
		gOffsets.dwSetKeyValuesName = **(PDWORD*)(dwSetKeyValuesLocation + 0x2);
#ifdef _DEBUG
		gBaseAPI.LogToFile("dwSetKeyValuesName client.dll+0x%X", (DWORD)gOffsets.dwSetKeyValuesName - dwClientBase);
#endif

		DWORD dwAlphaOffsetAddress = gBaseAPI.GetVguiMatSurfaceSignature("F3 0F 2C 81");
		XASSERT(dwAlphaOffsetAddress);
		gVFuncOffsets.dwAlphaOffset = *(DWORD*)(dwAlphaOffsetAddress + 0x4);
#ifdef _DEBUG
		gBaseAPI.LogToFile("dwAlphaOffset 0x%X", gVFuncOffsets.dwAlphaOffset);
#endif

		gVFuncOffsets.dwGetUserIdFn = gBaseAPI.GetClientSignature("55 8B EC 81 EC ? ? ? ? A1 ? ? ? ? 83 C1 08");
		XASSERT(gVFuncOffsets.dwGetUserIdFn);
#ifdef _DEBUG
		gBaseAPI.LogToFile("dwGetUserIdFn client.dll+0x%X", (DWORD)gVFuncOffsets.dwGetUserIdFn - dwClientBase);
#endif

#ifdef _DEBUG
		try
		{
#endif
			CreateInterfaceFn AppSysFactory = NULL;
			DWORD dwAppSystem = gBaseAPI.GetEngineSignature("A1 ? ? ? ? 8B 11 68");
			XASSERT(dwAppSystem);
			if (dwAppSystem)
			{
				gInts.Globals = *(CGlobals**)(dwAppSystem + 8);
				DWORD dwAppSystemAddress = **(PDWORD*)((dwAppSystem)+1);
				AppSysFactory = (CreateInterfaceFn)dwAppSystemAddress;
#ifdef _DEBUG
				gBaseAPI.LogToFile("g_pGlobals engine.dll+[0x%X]", (DWORD)gInts.Globals - (DWORD)gBaseAPI.GetModuleHandleSafe("engine.dll"));
				gBaseAPI.LogToFile("g_AppSysFactory: engine.dll+[0x%X]", (DWORD)AppSysFactory - (DWORD)gBaseAPI.GetModuleHandleSafe("engine.dll"));
#endif
			}

			gOffsets.Initialize();
			gCvars2.ReadCvars();
			gAnnouncer.AddListeners();

#ifdef _DEBUG
			gBaseAPI.LogToFile("Internals successfully initialized.");
#endif

			gInts.Cvar = (ICvar*)AppSysFactory("VEngineCvar004", NULL);
			XASSERT(gInts.Cvar);
#ifdef _DEBUG
			gBaseAPI.LogToFile("gInts.Cvar [0x%.8X]", (DWORD)gInts.Cvar);
#endif

			//Null out the CVars from a previous session.
			gCvars2.Cvars[esp_rage].iValue = 0;
			gCvars2.Cvars[misc_noisemaker].iValue = 0;

			if (gInts.Client)
			{
				VMTBaseManager* clientHook = new VMTBaseManager();
				clientHook->Init(gInts.Client);
				clientHook->HookMethod(&Hooked_IN_KeyEvent, gVFuncOffsets.inKeyFn);
				clientHook->HookMethod(&Hooked_HLClientCreateMove, gVFuncOffsets.createMove);
				clientHook->HookMethod(&Hooked_HudUpdate, gVFuncOffsets.hudUpdate);
				clientHook->Rehook();

				VMTBaseManager* clientModeHook = new VMTBaseManager(); //Setup our VMTBaseManager for ClientMode.
				clientModeHook->Init(gInts.ClientMode);
				clientModeHook->HookMethod(&Hooked_ClientModeCreateMove, gVFuncOffsets.createMove); //ClientMode create move is called inside of CHLClient::CreateMove, and thus no need for hooking WriteUserCmdDelta.
				clientModeHook->Rehook();

#ifdef _DEBUG
				gBaseAPI.LogToFile("Hooked CHLClient.");
				gBaseAPI.LogToFile("CreateMove: client.dll+%X", (DWORD)clientHook->GetMethod<DWORD>(gVFuncOffsets.createMove) - dwClientBase);
#endif
				DWORD dwInputPointer = gBaseAPI.dwFindPattern((DWORD)clientHook->GetMethod<DWORD>(gVFuncOffsets.createMove), ((DWORD)clientHook->GetMethod<DWORD>(gVFuncOffsets.createMove)) + 0x100, "8B 0D");
				XASSERT(dwInputPointer);
				gInts.Input = **(CInput***)(dwInputPointer + 2);

				VMTBaseManager* inputHook = new VMTBaseManager();
				inputHook->Init(gInts.Input);

				DWORD dwmpCommandsOffsetAddress = gBaseAPI.dwFindPattern((DWORD)inputHook->GetMethod<DWORD>(gVFuncOffsets.getUserCmd), ((DWORD)inputHook->GetMethod<DWORD>(gVFuncOffsets.getUserCmd)) + 0x40, "03 86");
				XASSERT(dwmpCommandsOffsetAddress);
				gOffsets.dwmpCommandsOffset = *(PDWORD)(dwmpCommandsOffsetAddress + 2);
#ifdef _DEBUG
				gBaseAPI.LogToFile("CInput: client.dll+%X", gInts.Input - dwClientBase);
				gBaseAPI.LogToFile("CUserCmd SizeOf: %i", sizeof(CUserCmd));
				gBaseAPI.LogToFile("m_pCommands offset: 0x%X", gOffsets.dwmpCommandsOffset);
#endif
				inputHook->HookMethod(&Hooked_GetUserCmd, gVFuncOffsets.getUserCmd);
				inputHook->Rehook();
		}

			DWORD* PlusShowScores = gInts.Cvar->FindCommand("+showscores");
			gOffsets.dwShowScoresOn = *(DWORD*)((DWORD)PlusShowScores + 0x18);
			*(DWORD*)((DWORD)PlusShowScores + 0x18) = (DWORD)NewShowScoresOnCallback;
#ifdef _DEBUG
			gBaseAPI.LogToFile("+showscores: %X", NewShowScoresOnCallback);
#endif

			DWORD* MinusShowScores = gInts.Cvar->FindCommand("-showscores");
			gOffsets.dwShowScoresOff = *(DWORD*)((DWORD)MinusShowScores + 0x18);
			*(DWORD*)((DWORD)MinusShowScores + 0x18) = (DWORD)NewShowScoresOffCallback;
#ifdef _DEBUG
			gBaseAPI.LogToFile("-showscores: %X", MinusShowScores);
#endif

			DWORD* Quit = gInts.Cvar->FindCommand("quit");
			*(DWORD*)((DWORD)Quit + 0x18) = (DWORD)NewQuitCallback;
#ifdef _DEBUG
			gBaseAPI.LogToFile("Quit: %X", Quit);
#endif

			DWORD* Exit = gInts.Cvar->FindCommand("exit");
			gOffsets.dwExit = *(DWORD*)((DWORD)Exit + 0x18);
			*(DWORD*)((DWORD)Exit + 0x18) = (DWORD)NewExitCallback;
#ifdef _DEBUG
			gBaseAPI.LogToFile("exit: %X", Exit);
#endif

			gConVars.sv_maxupdaterate = gInts.Cvar->FindVar("sv_maxupdaterate");
			gConVars.sv_client_min_interp_ratio = gInts.Cvar->FindVar("sv_client_min_interp_ratio");

			gConVars.name = gInts.Cvar->FindVar("name");
			*(DWORD*)((DWORD)gConVars.name + 0x58) = (DWORD)NewNameChangeCallback;
			gConVars.name->SetValue("It's not OK to be gay.");
#ifdef _DEBUG
			gBaseAPI.LogToFile("name: %X", (DWORD)gConVars.name);
#endif

			IConVar* tf_avoidteammates_pushaway = gInts.Cvar->FindVar("tf_avoidteammates_pushaway");
			if (tf_avoidteammates_pushaway)
			{
				*(DWORD*)((DWORD)tf_avoidteammates_pushaway + 0x14) = 0;
				tf_avoidteammates_pushaway->SetValue("0");
			}

#ifdef _DEBUG
		}
		catch (...)
		{
			gBaseAPI.LogToFile("Failed Intro");
			gBaseAPI.ErrorBox("Failed Intro");
		}
#endif

		int iLine = __LINE__;
		try
		{
			iLine = __LINE__;
			DWORD pOverlayPointer = gBaseAPI.GetGameOverlayRendererSignature("FF 15 ? ? ? ? 8B F0 85 FF");
			XASSERT(pOverlayPointer);

			iLine = __LINE__;
			DWORD_PTR* Present_V = (DWORD_PTR*)(*(PDWORD*)(pOverlayPointer + 2));

			iLine = __LINE__;
			extern Present_T OriginalPresent;
			extern HRESULT _stdcall HookedPresent(LPDIRECT3DDEVICE9 pDevice, RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride, RGNDATA* pDirtyRegion);
			iLine = __LINE__;
			OriginalPresent = (Present_T)(*Present_V);
			iLine = __LINE__;
			*Present_V = (DWORD_PTR)(&HookedPresent);
		}
		catch (...)
		{
			gBaseAPI.LogToFile("Failed to hook game overlay %i", iLine);
		}

		HMODULE vstdlib = gBaseAPI.GetModuleHandleSafe("vstdlib.dll");
		gOffsets.dwRandomFloat = (DWORD)GetProcAddress(vstdlib, "RandomFloat");
		gOffsets.dwRandomInt = (DWORD)GetProcAddress(vstdlib, "RandomInt");
		gOffsets.dwRandomSeed = (DWORD)GetProcAddress(vstdlib, "RandomSeed");
		XASSERT(gOffsets.dwRandomFloat);
		XASSERT(gOffsets.dwRandomSeed);
		extern DWORD WINAPI dwCritThread(LPVOID lpArguments);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)dwCritThread, NULL, 0, NULL);
	}
	return 0;
}

int WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)dwMainThread, NULL, 0, NULL );
	}
	return true;
}