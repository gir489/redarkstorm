// Use pragma directive if supported.
#pragma once

// Use preprocessor header guards if pragma once is unavailable
// Required to ensure portability
#ifndef __CYPHER__WINCUSTOM_H
#define __CYPHER__WINCUSTOM_H

// Windows API
#include <Windows.h>
#include <WinNT.h>
#include <winternl.h>

// All the structs below are manually defined because they are from
// ReactOS/Wine. They are used because they are  either better than
// the ones provided by Microsoft or are dependencies of the structs
// that are actually needed
// Note: Names slightly modified to avoid collisions

class PebLdrData
{
public:
	ULONG Length;
	BOOLEAN Initialized;
	PVOID SsHandle;
	LIST_ENTRY InLoadOrderModuleList;
	LIST_ENTRY InMemoryOrderModuleList;
	LIST_ENTRY InInitializationOrderModuleList;
	PVOID EntryInProgress;
};

class PebLdrDataEntry
{
public:
	LIST_ENTRY InLoadOrderLinks;
	LIST_ENTRY InMemoryOrderModuleList;
	LIST_ENTRY InInitializationOrderModuleList;
	PVOID DllBase;
	PVOID EntryPoint;
	ULONG SizeOfImage;
	UNICODE_STRING FullDllName;
	UNICODE_STRING BaseDllName;
	ULONG Flags;
	USHORT LoadCount;
	USHORT TlsIndex;
	union
	{
		LIST_ENTRY HashLinks;
		PVOID SectionPointer;
	};
	ULONG CheckSum;
	union
	{
		ULONG TimeDateStamp;
		PVOID LoadedImports;
	};
	PVOID EntryPointActivationContext;
	PVOID PatchInformation;
};

typedef struct _PEB_FREE_BLOCK {
	struct _PEB_FREE_BLOCK *Next;
	ULONG Size;
} PEB_FREE_BLOCK, *PPEB_FREE_BLOCK;

#define GDI_HANDLE_BUFFER_SIZE      34

#define TLS_MINIMUM_AVAILABLE 64    // winnt

#define STATUS_UNSUCCESSFUL ((NTSTATUS)0xC0000001L)
#define STATUS_SUCCESS ((NTSTATUS)0x00000000L)

// Required for NtQueryDirectoryFile return value
#define STATUS_NO_SUCH_FILE ((NTSTATUS)0xC000000FL)

class ProcEnvBlock
{
public:
	BOOLEAN InheritedAddressSpace;      // These four fields cannot change unless the
	BOOLEAN ReadImageFileExecOptions;   //
	BOOLEAN BeingDebugged;              //
	BOOLEAN SpareBool;                  //
	HANDLE Mutant;                      // INITIAL_PEB structure is also updated.

	PVOID ImageBaseAddress;
	PebLdrData* Ldr;
	struct _RTL_USER_PROCESS_PARAMETERS *ProcessParameters;
	PVOID SubSystemData;
	PVOID ProcessHeap;
	PVOID FastPebLock;
	PVOID FastPebLockRoutine;
	PVOID FastPebUnlockRoutine;
	ULONG EnvironmentUpdateCount;
	PVOID KernelCallbackTable;
	HANDLE EventLogSection;
	PVOID EventLog;
	PPEB_FREE_BLOCK FreeList;
	ULONG TlsExpansionCounter;
	PVOID TlsBitmap;
	ULONG TlsBitmapBits[2];         // relates to TLS_MINIMUM_AVAILABLE
	PVOID ReadOnlySharedMemoryBase;
	PVOID ReadOnlySharedMemoryHeap;
	PVOID *ReadOnlyStaticServerData;
	PVOID AnsiCodePageData;
	PVOID OemCodePageData;
	PVOID UnicodeCaseTableData;

	// Useful information for LdrpInitialize
	ULONG NumberOfProcessors;
	ULONG NtGlobalFlag;

	// Passed up from MmCreatePeb from Session Manager registry key

	LARGE_INTEGER CriticalSectionTimeout;
	ULONG HeapSegmentReserve;
	ULONG HeapSegmentCommit;
	ULONG HeapDeCommitTotalFreeThreshold;
	ULONG HeapDeCommitFreeBlockThreshold;

	// Where heap manager keeps track of all heaps created for a process
	// Fields initialized by MmCreatePeb.  ProcessHeaps is initialized
	// to point to the first free byte after the PEB and MaximumNumberOfHeaps
	// is computed from the page size used to hold the PEB, less the fixed
	// size of this data structure.

	ULONG NumberOfHeaps;
	ULONG MaximumNumberOfHeaps;
	PVOID *ProcessHeaps;

	//
	//
	PVOID GdiSharedHandleTable;
	PVOID ProcessStarterHelper;
	PVOID GdiDCAttributeList;
	PVOID LoaderLock;

	// Following fields filled in by MmCreatePeb from system values and/or
	// image header.

	ULONG OSMajorVersion;
	ULONG OSMinorVersion;
	ULONG OSBuildNumber;
	ULONG OSPlatformId;
	ULONG ImageSubsystem;
	ULONG ImageSubsystemMajorVersion;
	ULONG ImageSubsystemMinorVersion;
	ULONG ImageProcessAffinityMask;
	ULONG GdiHandleBuffer[GDI_HANDLE_BUFFER_SIZE];
};

// For NtQueryVirtualMemory
typedef enum _MEMORY_INFORMATION_CLASS
{
	MemoryBasicInformation,
	MemoryWorkingSetList,
	MemorySectionName,
	MemoryBasicVlmInformation
} MEMORY_INFORMATION_CLASS;

typedef LONG KPRIORITY_C;

#define VK_0  0x30 // 0 key 
#define VK_1  0x31 // 1 key 
#define VK_2  0x32 // 2 key 
#define VK_3  0x33 // 3 key 
#define VK_4  0x34 // 4 key 
#define VK_5  0x35 // 5 key 
#define VK_6  0x36 // 6 key 
#define VK_7  0x37 // 7 key 
#define VK_8  0x38 // 8 key 
#define VK_9  0x39 // 9 key 
#define VK_A  0x41 // a key 
#define VK_B  0x42 // b key 
#define VK_C  0x43 // c key 
#define VK_D  0x44 // d key 
#define VK_E  0x45 // e key 
#define VK_F  0x46 // f key 
#define VK_G  0x47 // g key 
#define VK_H  0x48 // h key 
#define VK_I  0x49 // i key 
#define VK_J  0x4A // j key 
#define VK_K  0x4B // k key 
#define VK_L  0x4C // l key 
#define VK_M  0x4D // m key 
#define VK_N  0x4E // n key 
#define VK_O  0x4F // o key 
#define VK_P  0x50 // p key 
#define VK_Q  0x51 // q key 
#define VK_R  0x52 // r key 
#define VK_S  0x53 // s key 
#define VK_T  0x54 // t key 
#define VK_U  0x55 // u key 
#define VK_V  0x56 // v key 
#define VK_W  0x57 // w key 
#define VK_X  0x58 // x key 
#define VK_Y  0x59 // y key 
#define VK_Z  0x5A // z key 

// Used for getting PEB
typedef struct ___PROCESS_BASIC_INFORMATION
{
	NTSTATUS ExitStatus;
	ProcEnvBlock* PebBaseAddress;
	ULONG_PTR AffinityMask;
	KPRIORITY_C BasePriority;
	ULONG_PTR UniqueProcessId;
	ULONG_PTR InheritedFromUniqueProcessId;
} __PROCESS_BASIC_INFORMATION,*__PPROCESS_BASIC_INFORMATION;

#endif // __CYPHER__WINCUSTOM_H