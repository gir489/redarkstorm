#pragma once

#include <Windows.h>
#include <intrin.h>
#include <Tlhelp32.h>
#include <math.h>
#include <string>
#include <sstream>
#include <vector>
#include "Vector.h"
#include "getvfunc.h"
#include "dt_recv2.h"
#include "CBaseAPI.h"
#include "WeaponList.h"
#include "ControlVariables.h"
#include "VMTHooks.h"

typedef void* ( __cdecl *CreateInterfaceFn)(const char *pName, int *pReturnCode);

#define VMTManager toolkit::VMTManager
#define VMTBaseManager toolkit::VMTBaseManager

#undef PlaySound

using namespace std;

class CVfuncOffsets
{
public:
	int iMaxClipVFuncOffset, iSlotVFuncOffset, iUpdateGlowVFuncOffset, iDestoryGlowVFuncOffset, iSteamIDOffset;
	static const int createMove = 21, getUserCmd = 8, writeUserCmd = 23, inKeyFn = 20, hudUpdate = 11;
	DWORD dwAlphaOffset, dwGetUserIdFn;
};

extern CVfuncOffsets gVFuncOffsets;

#define WIN32_LEAN_AND_MEAN
#pragma optimize("gsy",on)
#pragma warning(disable: 4244) // possible loss of data
#pragma warning(disable: 4305) // possible loss of data

typedef float matrix3x4[3][4];

#define me gInts.Engine->GetLocalPlayer()
#define GetBaseEntity gInts.EntList->GetClientEntity
#define MASK_TF2 0x200400B
#define MASK_EXPLODE 0x6004003
#define	FL_ONGROUND (1<<0)
#define FL_DUCKING (1<<1)
#define CONTENTS_HITBOX 0x40000000
#define FLOW_OUTGOING 0
#define FLOW_INCOMING 1
#define PI 3.14159265358979323846f
#define M_PI 3.14159265358979323846
#define DEG2RAD( x ) ( ( float )( x ) * ( float )( ( float )( PI ) / 180.0f ) )
#define RAD2DEG( x ) ( ( float )( x ) * ( float )( 180.0f / ( float )( PI ) ) )
#define RADPI 57.295779513082f
#define SQUARE( a ) a*a
#define IS_RED(iTeamNumber) (TEAM_RED==(iTeamNumber))
#define IS_BLU(iTeamNumber) (TEAM_BLUE==(iTeamNumber))
#define IS_SPECTATOR(iTeamNumber) (TEAM_SPECTATOR==(iTeamNumber))

enum VGuiPanel_t
{
	PANEL_ROOT = 0,
	PANEL_GAMEUIDLL,
	PANEL_CLIENTDLL,
	PANEL_TOOLS,
	PANEL_INGAMESCREENS,
	PANEL_GAMEDLL,
	PANEL_CLIENTDLL_TOOLS
};

class CMoveData
{
public:
	bool			m_bFirstRunOfFunctions : 1;
	bool			m_bGameCodeMovedPlayer : 1;

	unsigned long	m_nPlayerHandle;	// edict index on server, client entity handle on client

	int				m_nImpulseCommand;	// Impulse command issued.
	Vector			m_vecViewAngles;	// Command view angles (local space)
	Vector			m_vecAbsViewAngles;	// Command view angles (world space)
	int				m_nButtons;			// Attack buttons.
	int				m_nOldButtons;		// From host_client->oldbuttons;
	float			m_flForwardMove;
	float			m_flSideMove;
	float			m_flUpMove;
	
	float			m_flMaxSpeed;
	float			m_flClientMaxSpeed;
	float			m_flSpacer01;

	// Variables from the player edict (sv_player) or entvars on the client.
	// These are copied in here before calling and copied out after calling.
	Vector			m_vecVelocity;		// edict::velocity		// Current movement direction.
	Vector			m_vecAngles;		// edict::angles
	Vector			m_vecOldAngles;
	
// Output only
	float			m_outStepHeight;	// how much you climbed this move
	Vector			m_outWishVel;		// This is where you tried 
	Vector			m_outJumpVel;		// This is your jump velocity

	// Movement constraints	(radius 0 means no constraint)
	Vector			m_vecConstraintCenter;
	float			m_flConstraintRadius;
	float			m_flConstraintWidth;
	float			m_flConstraintSpeedFactor;

	void			SetAbsOrigin( const Vector &vec );
	const Vector	&GetAbsOrigin() const;

private:
	Vector			m_vecAbsOrigin;		// edict::origin
};

class CViewSetup
{
public:
};

enum virutalkeys_t
{
	VK_0 = 0x30,
	VK_1,
	VK_2,
	VK_3,
	VK_4,
	VK_5,
	VK_6,
	VK_7,
	VK_8,
	VK_9,
	VK_A = 0x41,
	VK_B,
	VK_C,
	VK_D,
	VK_E,
	VK_F,
	VK_G,
	VK_H,
	VK_I,
	VK_J,
	VK_K,
	VK_L,
	VK_M,
	VK_N,
	VK_O,
	VK_P,
	VK_Q,
	VK_R,
	VK_S,
	VK_T,
	VK_U,
	VK_V,
	VK_W,
	VK_X,
	VK_Y,
	VK_Z,
};

class CSteamID
{
public:
	unsigned int m_unAccountID : 32;
    unsigned int m_unAccountInstance : 20;
    unsigned int m_EAccountType : 4;
	unsigned int m_EUniverse : 8;
};

class IGameEvent
{
public:
	virtual ~IGameEvent() {};
	virtual const char *GetName() const = 0;	// get event name
	virtual bool  IsReliable() const = 0; // if event handled reliable
	virtual bool  IsLocal() const = 0; // if event is never networked
	virtual bool  IsEmpty(const char *keyName ) = 0;
	virtual bool  GetBool( const char *keyName, bool defaultValue ) = 0;
	virtual int   GetInt( const char *keyName, int defaultValue ) = 0;
	virtual float GetFloat( const char *keyName, float defaultValue) = 0;
	virtual const char *GetString( const char *keyName, const char *defaultValue = "" ) = 0;
};

class ClientClass
{
private:
	BYTE _chPadding[8];
public:
	char* chName;
	RecvTable* Table;
	ClientClass* pNextClass;
	int iClassID;
};

class CHLClient
{
public:
	ClientClass* GetAllClasses( void )
	{
		typedef ClientClass* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 8 )( this );
	}
};

//class CTimer {
//public: 
//	CTimer( ) { dwStop = 0; } 
//	~CTimer( ) { } 
//	void Start( float fSec ) { dwStop = (DWORD)(fSec*1000) + timeGetTime(); }
//	void Stop( ) { dwStop = 0; } 
//	float TimeLeft( ) { if( Running() ) return ( ((float)(dwStop - timeGetTime())/1000) ); return 0; } 
//	bool Running( ) { return ( dwStop > timeGetTime() ); } 
//	DWORD GetTime( ) { return timeGetTime(); }
//protected: 
//	DWORD dwStop; 
//};

class CGlobals
{
public:
	float realtime;
	int framecount;
	float absoluteframetime;
	float curtime;
	float frametime;
	int maxclients;
	int tickcount;
	float interval_per_tick;
	float interpolation_amount;
};

class CUserCmd
{
public:
	virtual ~CUserCmd() {}; //Destructor 0
	int command_number; //4
	int tick_count; //8
	Vector viewangles; //C
	float forwardmove; //18
	float sidemove; //1C
	float upmove; //20
	int	buttons; //24
	BYTE impulse; //28
	int weaponselect; //2C
	int weaponsubtype; //30
	int random_seed; //34
	short mousedx; //38
	short mousedy; //3A
	bool hasbeenpredicted; //3C;
};

class IConVar
{
public:
	void SetActualValue( const char* pszValue )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 2 )( this, pszValue );
	}
	void SetValue( const char* pszValue )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char* );
		PVOID pBase = (PVOID)( this + 0x18 );
		return getvfunc<OriginalFn>( pBase, 2 )( pBase, pszValue );
	}
	float GetFloat ( void )
	{
		return *(float*)( *((DWORD*)( this + 0x1C)) + 0x2C );
	}
};

class CModelInfo
{
public:
	const char *GetModelName( DWORD* model )
	{
		typedef const char* ( __thiscall* OriginalFn )( PVOID, DWORD* );
		return getvfunc<OriginalFn>( this, 3 )( this, model );
	}
	DWORD* GetStudiomodel( DWORD *model )
	{
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID, DWORD* );
		return getvfunc<OriginalFn>( this, 28 )( this, model );
	}
};

class ICvar
{
public:
	IConVar* FindVar( const char* chName )
	{
		typedef IConVar* ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 13 )( this, chName );
	}
	DWORD* FindCommand( const char* chName )
	{
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 15 )( this, chName );
	}
};

class IGameResources
{
	public:
	 virtual ~IGameResources() {};

	 virtual const char* GetTeamName( int iTeamIndex ) = 0;
	 virtual int GetTeamScore( int iTeamIndex ) = 0;
	 virtual const DWORD& GetTeamColor( int iTeamIndex ) = 0;
	 virtual bool IsConnected( int iEntityIndex ) = 0;
	 virtual bool IsAlive( int iEntityIndex ) = 0;
	 virtual bool IsFakePlayer( int iEntityIndex ) = 0;
	 virtual bool IsLocalPlayer( int iEntityIndex ) = 0;
	 virtual const char* GetPlayerName( int iEntityIndex ) = 0;
	 virtual int GetPlayerScore( int iEntityIndex ) = 0;
	 virtual int GetPing( int iEntityIndex ) = 0;
	 virtual int GetDeaths( int iEntityIndex ) = 0;
	 virtual int GetFrags( int iEntityIndex ) = 0;
	 virtual int GetTeam( int iEntityIndex ) = 0;
	 virtual int GetHealth( int iEntityIndex ) = 0;
};

class CBaseEntity
{
public:
	void Destructor(int flags)
	{
		typedef void (__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 0)(this, flags);
	}
	Vector& GetAbsOrigin()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 9)(this);
	}
	void GetWorldSpaceCenter( Vector& vWorldSpaceCenter)
	{
		Vector vMin, vMax;
		this->GetRenderBounds( vMin, vMax );
		vWorldSpaceCenter = this->GetAbsOrigin();
		vWorldSpaceCenter.z += (vMin.z + vMax.z) / 2;
	}
	void GetSteamID( CSteamID* pSID )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, CSteamID* );
		return getvfunc<OriginalFn>(this, gVFuncOffsets.iSteamIDOffset)(this, pSID);
	}
	DWORD* GetModel( )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pRenderable, 9 )( pRenderable );
	}
	bool SetupBones( matrix3x4 *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef bool ( __thiscall* OriginalFn )( PVOID, matrix3x4*, int, int, float );
		return getvfunc<OriginalFn>( pRenderable, 16 )( pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime );
	}
	ClientClass* GetClientClass( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef ClientClass* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pNetworkable, 2 )( pNetworkable );
	}
	/*Vector* GetPunchAngles()
	{
		return (Vector*)((DWORD)this + gVFuncOffsets.dwPunchAngles);
	}*/
	void UpdateGlow( )
	{
		typedef void (__thiscall* ThisFn )( PVOID );
		getvfunc<ThisFn>( this, gVFuncOffsets.iUpdateGlowVFuncOffset )( this );
	}
	/*float flGetWeaponSpread() //FF D0 51 8B 4D ?? D9 1C 24 8B 01 8B 80 ??
	{
		typedef float(__thiscall* ThisFn)(PVOID);
		return getvfunc<ThisFn>(this, 455)(this);
	}*/
	int GetWeaponSlot( )
	{
		typedef int (__thiscall* ThisFn )( PVOID );
		return getvfunc<ThisFn>( this, gVFuncOffsets.iSlotVFuncOffset )( this );
	}
	int GetMaxClip( )
	{
		typedef int (__thiscall* ThisFn )( PVOID );
		return getvfunc<ThisFn>( this, gVFuncOffsets.iMaxClipVFuncOffset )( this );
	}
	bool IsDormant( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pNetworkable, 8 )( pNetworkable );
	}
	int GetUserId()
	{
		typedef int(__thiscall *GetUserIdFn)(PVOID);
		static GetUserIdFn GetUserId = (GetUserIdFn)gVFuncOffsets.dwGetUserIdFn;
		return GetUserId(this);
	}
	int GetIndex( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pNetworkable, 9 )( pNetworkable );
	}
	const char* GetPlayerName( );
private:
	void GetRenderBounds( Vector& mins, Vector& maxs )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& , Vector& );
		getvfunc<OriginalFn>( pRenderable, 20 )( pRenderable, mins, maxs );
	}
};

struct trace_t
{
	Vector start;
	Vector end;
	BYTE plane[20];
	float flFraction;
	int contents;
	WORD dispFlags;
	bool allSolid;
	bool startSolid;
	float fractionLeftSolid;
	BYTE surface[8];
	int hitGroup;
	short physicsBone;
	CBaseEntity* m_pEnt;
	int hitbox;
};

class __declspec(align(16)) VectorAligned : public Vector
{
public:
	inline VectorAligned(void) {};
	inline VectorAligned(float X, float Y, float Z) 
	{
		Init(X,Y,Z);
	}
public:
	explicit VectorAligned(const Vector &vOther) 
	{
		Init(vOther.x, vOther.y, vOther.z);
	}
	
	VectorAligned& operator=(const Vector &vOther)	
	{
		Init(vOther.x, vOther.y, vOther.z);
		return *this;
	}
	float w;
};

class ClientModeShared
{
public:
	bool IsChatPanelOutOfFocus(void)
	{
		typedef PVOID(__thiscall* OriginalFn)(PVOID);
		PVOID CHudChat = getvfunc<OriginalFn>(this, 19)(this);
		if (CHudChat)
		{
			return *(PFLOAT)((DWORD)CHudChat + gVFuncOffsets.dwAlphaOffset) == 0;
		}
		return false;
	}
};

class IGameEventListener2
{
public:
	virtual	~IGameEventListener2( void ) {};
	virtual void FireGameEvent( IGameEvent *event ) = 0;
};

class CGameEventManager2
{
public:
	virtual	~CGameEventManager2( void ) {};
	virtual int LoadEventsFromFile( const char *filename ) = 0;
	virtual void Reset() = 0;
	virtual bool AddListener( IGameEventListener2 *listener, const char *name, bool bServerSide ) = 0;
	virtual bool FindListener( IGameEventListener2 *listener, const char *name ) = 0;
	virtual void RemoveListener( IGameEventListener2 *listener) = 0;
	virtual IGameEvent *CreateEvent( const char *name, bool bForce = false ) = 0;
	virtual bool FireEvent( IGameEvent *event, bool bDontBroadcast = false ) = 0;
	virtual bool FireEventClientSide( IGameEvent *event ) = 0;
};

struct Ray_t
{
	VectorAligned  m_Start;
	VectorAligned  m_Delta;
	VectorAligned  m_StartOffset;
	VectorAligned  m_Extents;
	//DWORD dwSpacer00; //L4D
	bool	m_IsRay;
	bool	m_IsSwept;

	void Init( Vector const& start, Vector const& end )
	{
		Assert( &end );

		m_Delta.x = end.x - start.x;
		m_Delta.y = end.y - start.y;
		m_Delta.z = end.z - start.z;

		m_IsSwept = (m_Delta.LengthSqr() != 0);

		VectorClear( m_Extents );
		m_IsRay = true;

		VectorClear( m_StartOffset );
		VectorCopy( start, m_Start );
	}
};

class CPrediction
{
public:
	void SetupMove( CBaseEntity* pPlayer, CUserCmd *pCommand, PDWORD pMoveHelper, CMoveData* MoveData )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, CBaseEntity*, CUserCmd*, PDWORD, CMoveData* );
		return getvfunc<OriginalFn>( this, 18 )( this, pPlayer, pCommand, pMoveHelper, MoveData );
	}
	void FinishMove( CBaseEntity* pPlayer, CUserCmd *pCommand, CMoveData* MoveData )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, CBaseEntity*, CUserCmd*, CMoveData* );
		return getvfunc<OriginalFn>( this, 19 )( this, pPlayer, pCommand, MoveData );
	}
};

class SteamFriends
{
public:
	const char *GetPersonaName( void )
	{
		typedef const char* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(this, 0)(this);
	}
	bool HasFriend( CSteamID steamIDFriend )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID, CSteamID, int );
		return getvfunc<OriginalFn>(this, 17)(this, steamIDFriend, 4);
	}
};

class SteamClient
{
public:
	SteamFriends *GetISteamFriends(int hSteamUser, int hSteamPipe, const char *pchVersion)
	{
		typedef SteamFriends* (__thiscall* OriginalFn)(PVOID, __int32, __int32, const char*);
		return getvfunc<OriginalFn>(this, 8)(this, hSteamUser, hSteamPipe, pchVersion );
	}
};

class CGameMovement
{
public:
	void ProcessMovement( CBaseEntity* pPlayer, CMoveData* MoveData )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, CBaseEntity*, CMoveData* );
		return getvfunc<OriginalFn>( this, 1 )( this, pPlayer, MoveData );
	}
};

class INetChannel
{
public:
	const char* GetAddress( void )
	{
		typedef const char* ( __thiscall* GetAvgLatencyFn )( PVOID );
		return getvfunc<GetAvgLatencyFn>(this, 1)( this );
	}
	float GetAvgLatency ( int iFlow )
	{
		typedef float ( __thiscall* GetAvgLatencyFn )( PVOID, int );
		return getvfunc<GetAvgLatencyFn>(this, 10)( this, iFlow );
	}
	void AddToSequenceNr(int value)
	{
		if (value != 0)
			*(PDWORD)((DWORD)this + 0x8) += value;
	}
	bool SendNetMsg(PVOID msg, bool bForceReliable = false, bool bVoice = false)
	{
		typedef bool(__thiscall* GetAvgLatencyFn)(PVOID, PVOID, bool, bool);
		return getvfunc<GetAvgLatencyFn>(this, 40)(this, msg, bForceReliable, bVoice);
	}
};

class CEngineClient
{
public:
	void GetScreenSize( int& width, int& height )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int& , int& );
		return getvfunc<OriginalFn>( this, 5 )( this, width, height );
	}
	//bool Con_IsVisible( void )
	//{
	//	typedef bool ( __thiscall* OriginalFn )( PVOID );
	//	return getvfunc<OriginalFn>( this, 11 )( this );
	//}
	void ServerCmd(const char *szCmdString, bool bReliable = true)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const char*, bool);
		return getvfunc<OriginalFn>(this, 6)(this, szCmdString, bReliable);
	}
	int GetLocalPlayer( void )
	{
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 12 )( this );
	}
	float Time( void )
	{
		typedef float ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 14 )( this );
	}
	void GetViewAngles( Vector& va )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& va );
		return getvfunc<OriginalFn>( this, 19 )( this, va );
	}
	void SetViewAngles( Vector& va )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& va );
		return getvfunc<OriginalFn>( this, 20 )( this, va );
	}
	bool IsInGame( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 26 )( this );
	}
	bool IsConnected( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 27 )( this );
	}
	const matrix3x4& WorldToScreenMatrix( void )
	{
		typedef const matrix3x4& ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(this, 36)(this);
	}
	INetChannel* GetNetChannelInfo(void)
	{
		typedef INetChannel* (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 72)(this);
	}
	bool IsTakingScreenshot( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 85 )( this );
	}
	void ClientCmd_Unrestricted( const char* chCommandString )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char * );
		return getvfunc<OriginalFn>( this, 106 )( this, chCommandString );
	}
	void ServerCmdKeyValues(PVOID kv)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, PVOID);
		getvfunc<OriginalFn>(this, 127)(this, kv);
	}
};

class IEngineVGui
{
public:
	virtual ~IEngineVGui(void) = 0;
	virtual unsigned int GetPanel( int type ) = 0;
	virtual bool IsGameUIVisible() = 0;
	virtual void ActivateGameUI() = 0;
};

class ISurface
{
public:
	void DrawSetColor(int r, int g, int b, int a)
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int, int, int, int );
		getvfunc<OriginalFn>( this, 11 )( this, r, g, b, a );
	}
	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int, int, int, int );
		getvfunc<OriginalFn>( this, 12 )( this, x0, y0, x1, y1 );
	}
	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int, int, int, int );
		getvfunc<OriginalFn>( this, 14 )( this, x0, y0, x1, y1 );
	}
	void DrawSetTextFont(unsigned long font)
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, unsigned long );
		getvfunc<OriginalFn>( this, 17 )( this, font );
	}
	void DrawSetTextColor(int r, int g, int b, int a )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int, int, int, int );
		getvfunc<OriginalFn>( this, 19 )( this, r, g, b, a );
	}
	void DrawSetTextPos(int x, int y )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int, int );
		getvfunc<OriginalFn>( this, 20 )( this, x, y );
	}
	void DrawPrintText(const wchar_t *text, int textLen )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const wchar_t *, int, int );
		return getvfunc<OriginalFn>( this, 22 )( this, text, textLen, 0 );
	}
	unsigned long CreateFont( )
	{
		typedef unsigned int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 66 )( this );
	}
	void SetFontGlyphSet(unsigned long &font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, unsigned long, const char*, int, int, int, int, int, int, int );
		getvfunc<OriginalFn>( this, 67 )( this, font, windowsFontName, tall, weight, blur, scanlines, flags, 0, 0 );
	}
	void GetTextSize(unsigned long font, const wchar_t *text, int &wide, int &tall)
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, unsigned long, const wchar_t *, int&, int& );
		getvfunc<OriginalFn>( this, 75 )( this, font, text, wide, tall );
	}
	void PlaySound(const char* fileName)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const char*);
		getvfunc<OriginalFn>(this, 78)(this, fileName);
	}
};

class CEntList
{
public:
	CBaseEntity* GetClientEntity( int entnum )
	{
		typedef CBaseEntity* ( __thiscall* OriginalFn )( PVOID, int );
		return getvfunc<OriginalFn>( this, 3 )( this, entnum );
	}
	CBaseEntity* GetClientEntityFromHandle( int hEnt )
	{
		typedef CBaseEntity* ( __thiscall* OriginalFn )( PVOID, int );
		return getvfunc<OriginalFn>( this, 4 )( this, hEnt );
	}
	int GetHighestEntityIndex(void)
	{
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 6 )( this );
	}
};

class CDemoPlayer
{
	bool GetHighestEntityIndex(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 6)(this);
	}
};

enum playercontrols
{
	IN_ATTACK = (1 << 0),
	IN_JUMP	= (1 << 1),
	IN_DUCK = (1 << 2),
	IN_FORWARD = (1 << 3),
	IN_BACK = (1 << 4),
	IN_USE = (1 << 5),
	IN_CANCEL = (1 << 6),
	IN_LEFT = (1 << 7),
	IN_RIGHT = (1 << 8),
	IN_MOVELEFT = (1 << 9),
	IN_MOVERIGHT = (1 << 10),
	IN_ATTACK2 = (1 << 11),
	IN_RUN = (1 << 12),
	IN_RELOAD = (1 << 13),
	IN_ALT1 = (1 << 14), 
	IN_ALT2 = (1 << 15),
	IN_SCORE = (1 << 16),	// Used by client.dll for when scoreboard is held down
	IN_SPEED = (1 << 17),	// Player is holding the speed key
	IN_WALK = (1 << 18),
	IN_ATTACK3 = (1 << 25 ), //Added 12/20/2012
};

enum source_lifestates
{
	LIFE_ALIVE,
	LIFE_DYING,
	LIFE_DEAD,
	LIFE_RESPAWNABLE,
	LIFE_DISCARDBODY,
};

class CInput
{
public:
	CUserCmd* GetUserCmd( int seq )
	{
		typedef CUserCmd* ( __thiscall* OriginalFn )( PVOID, int );
		return getvfunc<OriginalFn>( this, 8 )( this, seq );
	}
};

#define XASSERT( x ) if( !x ) { MessageBoxW( NULL, L#x, NULL, MB_ICONERROR | MB_OK ); gBaseAPI.LogToFile("Failed %s", #x ); }

struct mstudiobbox_t
{
	int	bone;
	int	group;
	Vector bbmin;
	Vector bbmax;
	int	szhitboxnameindex;
	int	unused[8];
};

struct mstudiohitboxset_t
{
	int					sznameindex;
	inline char * const	pszName( void ) const { return ((char *)this) + sznameindex; }
	int					numhitboxes;
	int					hitboxindex;
	mstudiobbox_t *pHitbox( int i ) const { return (mstudiobbox_t *)(((PBYTE)this) + hitboxindex) + i; };
};

class IHandleEntity;

class ITraceFilter
{
public:
	virtual bool ShouldHitEntity( IHandleEntity *pEntityHandle, int contentsMask )
	{
		return true;
	}
	virtual int	GetTraceType( ) const
	{
		return 0;
	}
};


class CEngineTrace
{
public:
	void TraceRay( const Ray_t &ray, unsigned int fMask, ITraceFilter *pTraceFilter, trace_t *pTrace )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const Ray_t &ray, unsigned int fMask, ITraceFilter *pTraceFilter, trace_t *pTrace );
		return getvfunc<OriginalFn>(this, 4)(this, ray, fMask, pTraceFilter, pTrace );
	}
};

#include "CGlobalVars.h"
#include "TF2.h"

class Interfaces
{
public:
	CInput* Input;
	CEntList* EntList;
	CEngineClient* Engine;
	CEngineTrace* Trace;
	CModelInfo* ModelInfo;
	IEngineVGui* VGUI;
	ISurface* Surface;
	CHLClient* Client;
	CPrediction* Prediction;
	CGameMovement* Movement;
	ICvar* Cvar;
	CGlobals* Globals;
	SteamFriends* Friends;
	CGameEventManager2* EventManager;
	ClientModeShared* ClientMode;
	CDemoPlayer* DemoPlayer;
};

extern Interfaces gInts;
extern CMiscFuncs gMiscFuncs;

//class CResponse : public ssq::IRulesResponse
//{
//public:
//	virtual void RulesResponded( const char* rule, const char* value )
//	{
//		//gBaseAPI.LogToFile( " rule %s = %s\n", rule, value );
//		if ( !strcmp(rule, "smac_version") )
//		{
//			gPlayerVars.bIsSMACServer = true;
//		}
//	}
//	virtual void RulesFinished( bool success )
//	{
//		return;
//	}
//};