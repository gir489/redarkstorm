﻿#include "SDK.h"
#include "CCheatMenu.h"
#include "COffsets.h"
#include "CDrawManager.h"
#include "CAimbot.h"
#include "ESP.h"
#include "CAnnouncer.h"

CPlayerVariables gPlayerVars;
CEntityIDS gEntIDs;
CGlobalCvars gConVars;
CUserCmd* __fastcall Hooked_GetUserCmd( PVOID pInput, int edx, int sequence_number )
{
	CUserCmd *pCommands = *(CUserCmd**)((DWORD)pInput + gOffsets.dwmpCommandsOffset);
	return &pCommands[sequence_number % 90];
}
//============================================================================================
bool __fastcall Hooked_IsPlayingBack(PVOID DemoPlayer, int edx)
{
	try
	{
		if ((DWORD)_ReturnAddress() != gOffsets.dwPureReturn)
			return VMTManager::GetHook(DemoPlayer).GetMethod<bool(__thiscall*)(PVOID)>(6)(DemoPlayer);
		return true;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed IsPlayingBack");
		return false;
	}
}
//============================================================================================
void RetrieveClassIDFromString( const char* szName, int& iClassID, ClientClass* pCC )
{
	if ( (iClassID == 0) && (0==strcmp(szName, pCC->chName)) )
	{
		iClassID = pCC->iClassID;
		XASSERT(iClassID);
		#ifdef _DEBUG
		gBaseAPI.LogToFile("%s ID: %i", pCC->chName, pCC->iClassID);
		#endif
	}
}
//============================================================================================
void GetClassIDs()
{
	for ( ClientClass* pCC = gInts.Client->GetAllClasses(); pCC; pCC = pCC->pNextClass )
	{
		RetrieveClassIDFromString("CObjectSentrygun", gEntIDs.iSentryID, pCC);
		RetrieveClassIDFromString("CObjectDispenser", gEntIDs.iDispenserID, pCC);
		RetrieveClassIDFromString("CObjectTeleporter", gEntIDs.iTeleporterID, pCC);
		RetrieveClassIDFromString("CSniperDot", gEntIDs.iSniperDotID, pCC);
		RetrieveClassIDFromString("CTFGrenadePipebombProjectile", gEntIDs.iGrenadeID, pCC);
		RetrieveClassIDFromString("CTFProjectile_Rocket", gEntIDs.iRocketID, pCC);
		RetrieveClassIDFromString("CTFProjectile_SentryRocket", gEntIDs.iSentryRocketID, pCC);
		RetrieveClassIDFromString("CTFStunBall", gEntIDs.iBaseballID, pCC);
		RetrieveClassIDFromString("CTFBall_Ornament", gEntIDs.iOrnamentID, pCC);
		RetrieveClassIDFromString("CTFProjectile_JarMilk", gEntIDs.iMilkID, pCC);
		RetrieveClassIDFromString("CTFProjectile_Flare", gEntIDs.iFlareID, pCC);
		RetrieveClassIDFromString("CTFProjectile_Jar", gEntIDs.iJarateID, pCC);
		RetrieveClassIDFromString("CTFProjectile_Arrow", gEntIDs.iArrowID, pCC);
		RetrieveClassIDFromString("CTFProjectile_HealingBolt", gEntIDs.iMedicArrowID, pCC);
		RetrieveClassIDFromString("CFuncRespawnRoomVisualizer", gEntIDs.iSpawnRoomID, pCC);
		RetrieveClassIDFromString("CTFProjectile_EnergyBall", gEntIDs.iEnergyBall, pCC);
		RetrieveClassIDFromString("CCaptureFlag", gEntIDs.iIntelID, pCC);
		RetrieveClassIDFromString("CTFPlayer", gEntIDs.iCTFPlayerID, pCC);
		RetrieveClassIDFromString("CTFProjectile_Cleaver", gEntIDs.iCleverID, pCC);
		RetrieveClassIDFromString("CTFAmmoPack", gEntIDs.iCTFAmmoPack, pCC);
		RetrieveClassIDFromString("CCurrencyPack", gEntIDs.iCurrencyPackID, pCC);
	}
}
//============================================================================================
void __fastcall Hooked_HudUpdate(PVOID pClient, int edx, bool active)
{
	VMTManager& hook = VMTManager::GetHook(pClient); //Get a pointer to the instance of your VMTManager with the function GetHook.
	hook.GetMethod<void(__thiscall*)(PVOID, bool)>(gVFuncOffsets.hudUpdate)(pClient, active); //Call the original.
	int iLine = __LINE__;
	try
	{
		if (gDrawManager->readyToCopy == false)
		{
			iLine = __LINE__;
			gDrawManager->Clear();
			iLine = __LINE__;
			DrawESP();
			iLine = __LINE__;
			gDrawManager->readyToCopy = true;
		}
		static bool bReset = false;
		if (gInts.Engine->IsInGame() == false)
		{
			gPlayerVars.iTeamNum = 0;
			gPlayerVars.bIsRageMode = false;
			if (gCvars2.Cvars[esp_rage].iValue != 0)
			{
				gCvars2.Cvars[esp_rage].iValue = 0;
			}
			if (gCvars2.Cvars[misc_noisemaker].iValue != 0)
			{
				gCvars2.Cvars[misc_noisemaker].iValue = 0;
			}
			gAnnouncer.Reset();
			bReset = true;
			return;
		}
		else if (gEntIDs.iArrowID == 0) {
			GetClassIDs();
		}
		else if (bReset == true && gInts.Engine->GetLocalPlayer() != NULL) {
			gInts.Engine->ClientCmd_Unrestricted("hud_reloadscheme; snd_restart; record fix; stop");
			bReset = false;
		}

		iLine = __LINE__;
		gAnnouncer.Think();
		iLine = __LINE__;
		gAimbot.FindTarget();
		iLine = __LINE__;
		gAimbot.AimAtTarget();
		iLine = __LINE__;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed Hooked_HudUpdate %i", iLine);
	}
	return;
}
//============================================================================================
void __fastcall Hooked_HLClientCreateMove(PVOID pClient, int edx, int sequence_number, float input_sample_frametime, bool active)
{
	try
	{
		if (pClient == NULL)
			return;

		CBaseEntity* pBaseEntity = GetBaseEntity(me);

		//GRABWEAPON
		gPlayerVars.pBaseWeapon = gOffsets.pGetBaseCombatActiveWeapon(pBaseEntity);

		//GRABPLAYERINFO
		if (gPlayerVars.pBaseWeapon != NULL && pBaseEntity != NULL)
		{
			gPlayerVars.iFlags = gOffsets.iGetFlags(pBaseEntity);
			gPlayerVars.iPlayerCond = gOffsets.iGetPlayerCond(pBaseEntity);
			gPlayerVars.iPlayerCondEx = gOffsets.iGetPlayerCondEx(pBaseEntity);
			gPlayerVars.iClass = gOffsets.iGetPlayerClass(pBaseEntity);
			gPlayerVars.iWeaponID = gOffsets.iGetItemIndex(gPlayerVars.pBaseWeapon);
			gPlayerVars.iWeaponSlot = gPlayerVars.pBaseWeapon->GetWeaponSlot();
			gPlayerVars.iClipMax = gPlayerVars.pBaseWeapon->GetMaxClip();
			gPlayerVars.iClip = gOffsets.iGetCurrentClipAmmo(gPlayerVars.pBaseWeapon);
			gPlayerVars.bHasMeleeWeapon = (gPlayerVars.iWeaponSlot == TF_WEAPONSLOT_MELEE);
			gPlayerVars.bHasFlameThrower = (gPlayerVars.iClass == TF2_Pyro && gPlayerVars.iWeaponSlot == 0);
			gPlayerVars.bHasMinigun = (gPlayerVars.iClass == TF2_Heavy && gPlayerVars.iWeaponSlot == 0);
			gPlayerVars.iTeamNum = gOffsets.iGetTeamNum(pBaseEntity);
			gPlayerVars.iUserId = pBaseEntity->GetUserId();
			gPlayerVars.iHealth = gOffsets.iGetHealth(pBaseEntity);
			gPlayerVars.iMaxHealth = gOffsets.GetMaxHealth(pBaseEntity);

			//Set sniper rifle scope-in time for triggerbot.
			if (gPlayerVars.iPlayerCond & TFCond_Zoomed)
			{
				if (gOffsets.GetSpawnTime(gPlayerVars.pBaseWeapon) == 0)
				{
					gOffsets.SetSpawnTime(gPlayerVars.pBaseWeapon, gInts.Globals->curtime);
				}
			}
			else if (gOffsets.GetSpawnTime(gPlayerVars.pBaseWeapon) != 0)
			{
				gOffsets.SetSpawnTime(gPlayerVars.pBaseWeapon, 0);
			}
		}

		//TAUNTSLIDE
		if (CV(misc_disguise))
		{
			if (gOffsets.bGetIsAlive(pBaseEntity))
			{
				if (gPlayerVars.iPlayerCond & TFCond_Taunting)
				{
					gOffsets.RemovePlayerCondition(pBaseEntity, TFCond_Taunting);
				}
			}
		}

		//CALLORIGINAL
		VMTManager& hook = VMTManager::GetHook(pClient); //Get a pointer to the instance of your VMTManager with the function GetHook.
		hook.GetMethod<void(__thiscall*)(PVOID, int, float, bool)>(gVFuncOffsets.createMove)(pClient, sequence_number, input_sample_frametime, active); //Call the original.

		//CUserCmd* pCommand = Hooked_GetUserCmd(gInts.Input, 0, sequence_number);
		//pCommand->random_seed = gMiscFuncs.MD5_PseudoRandom(pCommand->command_number) & 0x7FFFFFFF;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed Hooked_HLClientCreateMove");
		return;
	}
}
//============================================================================================
bool __fastcall Hooked_ClientModeCreateMove(PVOID ClientMode, int edx, float input_sample_frametime, CUserCmd* pCommand)
{
	try
	{
		VMTManager& hook = VMTManager::GetHook(ClientMode); //Get a pointer to the instance of your VMTManager with the function GetHook.
		bool bReturn = hook.GetMethod<bool(__thiscall*)(PVOID, float, CUserCmd*)>(gVFuncOffsets.createMove)(ClientMode, input_sample_frametime, pCommand); //Call the original.

		if (pCommand->command_number == 0)
			return bReturn;

		CBaseEntity* pBaseEntity = GetBaseEntity(me);

		if (pBaseEntity == NULL)
			return bReturn;

		if (gPlayerVars.pBaseWeapon == NULL)
			return bReturn;

		///RUNCOMMAND
		if (CV(aim_prediction))
		{
			int flags = gOffsets.iGetFlags(pBaseEntity);
			gInts.Globals->curtime = gOffsets.iGetTickBase(pBaseEntity) * gInts.Globals->interval_per_tick;
			gInts.Globals->frametime = gInts.Globals->interval_per_tick;

			*(CUserCmd**)((DWORD)pBaseEntity + gOffsets.dwCurrentCommandOffset) = pCommand;

			gInts.Prediction->SetupMove(pBaseEntity, pCommand, gOffsets.dwMoveHelper, &gOffsets.MoveData);
			gInts.Movement->ProcessMovement(pBaseEntity, &gOffsets.MoveData);
			gInts.Prediction->FinishMove(pBaseEntity, pCommand, &gOffsets.MoveData);

			*(PDWORD)((DWORD)pBaseEntity + gOffsets.dwCurrentCommandOffset) = NULL;
			gOffsets.SetFlags(pBaseEntity, flags);
		}

		gAimbot.FindTarget();
		gAimbot.AimAtTarget();

		gInts.Engine->SetViewAngles(pCommand->viewangles); //We're going to return false from CreateMove, so the game isn't going to set the view angles, so we need to do it manually.
		bReturn = false;

		//SILENTAIM
		if (CV(aim_silentaim))
		{
			if (gPlayerVars.vecAimAngles.x != 0 || gPlayerVars.vecAimAngles.y != 0)
			{
				pCommand->viewangles.x = gPlayerVars.vecAimAngles.x;
				pCommand->viewangles.y = gPlayerVars.vecAimAngles.y;
				Vector qCurrentView;
				gInts.Engine->GetViewAngles(qCurrentView);
				gAimbot.SilentAimFix(pCommand, qCurrentView);
			}
		}

		//TRIGGERBOT
		static int iTriggerCounts = 0;
		if (gPlayerVars.iClass == TF2_Demoman)
		{
			gMiscFuncs.RunAutoStickyFrame(pCommand);
		}
		if (gMiscFuncs.bIsKey(CV(triggerbot)))
		{
			switch (gPlayerVars.iClass)
			{
				case TF2_Demoman:
				{
					if (gPlayerVars.bHasMeleeWeapon)
					{
						if (gAimbot.bTriggerbotTrace(pCommand) == CAimbot::TRIGGERBOT_SUCCESS)
						{
							iTriggerCounts = 4;
						}
					}
					break;
				}
				case TF2_Pyro:
				{
					if (gPlayerVars.bHasFlameThrower && gPlayerVars.iWeaponID != WPN_Phlogistinator)
					{
						if (gMiscFuncs.bAirblastFrame())
						{
							iTriggerCounts = 4;
						}
					}
					else
					{
						if (gAimbot.bTriggerbotTrace(pCommand) == CAimbot::TRIGGERBOT_SUCCESS)
						{
							iTriggerCounts = 4;
						}
					}
					break;
				}
				default:
				{
					switch (gAimbot.bTriggerbotTrace(pCommand))
					{
					case CAimbot::TRIGGERBOT_SUCCESS:
						iTriggerCounts = 4;
						break;
					case CAimbot::TRIGGERBOT_STREAM:
						iTriggerCounts = 1;
					}
				}
			}
		}
		if (iTriggerCounts > 0)
		{
			if (gPlayerVars.bHasFlameThrower)
			{
				pCommand->buttons &= ~IN_ATTACK;
				pCommand->buttons |= IN_ATTACK2;
			}
			else
			{
				pCommand->buttons |= IN_ATTACK;
			}
			iTriggerCounts--;
		}

		//BUNNYHOP
		if (CV(misc_bhop))
		{
			gMiscFuncs.DoBunnyHop(pCommand, pBaseEntity);
		}

		if (gOffsets.iResistToSelect != NULL)
		{
			if (gPlayerVars.iWeaponID != WPN_Vaccinator)
			{
				gOffsets.iResistToSelect = NULL;
			}
			else
			{
				static int iTriggerCounts = 0;
				static int iPreviousCondEx = 0;
				if (gPlayerVars.iPlayerCondEx & gOffsets.iResistToSelect)
				{
					gOffsets.iResistToSelect = NULL;
					iTriggerCounts = 0;
					iPreviousCondEx = 0;
				}
				else
				{
					if (iTriggerCounts > 0)
					{
						iTriggerCounts--;
						pCommand->buttons |= IN_RELOAD;
					}
					else if (iPreviousCondEx != (gPlayerVars.iPlayerCondEx >> 29))
					{
						iTriggerCounts = 4;
						iPreviousCondEx = (gPlayerVars.iPlayerCondEx >> 29);
					}
				}
			}
		}

		//CRITSTUFF
		if (gMiscFuncs.bIsKey(CV(misc_crits)) && !gMiscFuncs.bIsNoCrits(gPlayerVars.iWeaponID))
		{
			if (pCommand->buttons & IN_ATTACK || (gPlayerVars.iClass == TF2_Demoman && gPlayerVars.iWeaponSlot == 1 && (gOffsets.flGetChargeBeginTime(gPlayerVars.pBaseWeapon) != 0)))
			{
				pCommand->command_number = gPlayerVars.command_number;
				*(PDWORD)((DWORD)_AddressOfReturnAddress() + 0x90 - 4) = gPlayerVars.command_number; //Replace the sequence_number from the CInput::CreateMove call with our crit command_number so the original create move will set random_spread for us.
			}
		}
		gPlayerVars.bHasGeneratedSeed = false;

		if (gPlayerVars.iClipMax > 0)
		{
			if ((pCommand->buttons & (IN_ATTACK | IN_ATTACK2)) == false)
			{
				if (gOffsets.iGetAmmo(pBaseEntity, gPlayerVars.pBaseWeapon) > 0)
				{
					if (gPlayerVars.iClip < gPlayerVars.iClipMax)
					{
						pCommand->buttons |= IN_RELOAD;
					}
					else if (gPlayerVars.iWeaponID == WPN_CowMangler)
					{
						pCommand->buttons |= IN_RELOAD;
					}
				}
			}
		}

		if (CV(misc_noisemaker))
		{
			gMiscFuncs.DoNoiseMaker();
		}

		if (gPlayerVars.bIsRageMode && gPlayerVars.bHasMinigun)
		{
			pCommand->buttons |= IN_ATTACK2;
		}

		bool bIsFKeyPressed = gMiscFuncs.bIsKeyPressed(VK_F);
		if ((!(pCommand->buttons & IN_ATTACK)) &&
			(bIsFKeyPressed || gMiscFuncs.bIsKeyPressed(VK_K)) &&
			!(gMiscFuncs.bIsKeyPressed(VK_LSHIFT)) &&
			(gPlayerVars.iPlayerCond & TFCond_Taunting) &&
			(gInts.ClientMode->IsChatPanelOutOfFocus() == true)
			)  //Conga in place.
		{
			if (bIsFKeyPressed && CV(misc_disguise))
			{
				gMiscFuncs.Spinbot(pCommand, false);
			}
			static bool bInverse = false;
			if (bInverse)
			{
				bInverse = false;
				pCommand->sidemove = 450;
			}
			else
			{
				bInverse = true;
				pCommand->sidemove = -450;
			}
		}

		return bReturn;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed Hooked_ClientModeCreateMove");
		return false;
	}
}
//============================================================================================
int __fastcall Hooked_IN_KeyEvent ( PVOID pClient, int edx, int eventcode, int keynum, const char *pszCurrentBinding )
{
	//gBaseAPI.LogToFile("IN_Key Called: eventcode: %i keynum: %i currentbinding: %s", eventcode, keynum, pszCurrentBinding);
	try
	{
		if(eventcode == 1 )
		{
			static bool bVoiceMenuPressed = false;
			if (gPlayerVars.iWeaponID == WPN_Vaccinator && (gMiscFuncs.bIsKeyPressed(VK_LBUTTON)) && !bVoiceMenuPressed) //MOUSE1
			{
				switch (keynum)
				{
					case 2:
						gOffsets.iResistToSelect = TFCondEx_BulletResistance;
						return 0;
					case 3:
						gOffsets.iResistToSelect = TFCondEx_ExplosiveResistance;
						return 0;
					case 4:
						gOffsets.iResistToSelect = TFCondEx_FireResistance;
						return 0;
				}
			}
			switch (keynum)
			{
				case 13:
				case 34:
				case 36:
					bVoiceMenuPressed = true;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
					bVoiceMenuPressed = false;
			}
			if ( keynum == 95 ) //F4
			{
				gPlayerVars.bIsRageMode = !gPlayerVars.bIsRageMode;
				return 0;
			}
			if ( keynum == 99 ) //F8
			{
				if ( gPlayerVars.iClass == TF2_Sniper )
				{
					gCvars2.Cvars[aim_fov].iValue = 1;
					gCvars2.Cvars[aim_smooth].iValue = 6;
					gCvars2.Cvars[misc_norecoil].iValue = 1;
				}
				else
				{
					gCvars2.Cvars[misc_norecoil].iValue = 0;
					gCvars2.Cvars[aim_fov].iValue = 6;
					gCvars2.Cvars[aim_smooth].iValue = 4;
				}
				gCvars2.Cvars[aim_cloaked].iValue = 0;
				gCvars2.Cvars[aim_mode].iValue = 0;
				gCvars2.Cvars[aim_silentaim].iValue = 0;
				gCvars2.Cvars[aim_sentry].iValue = 0;
				gCvars2.Cvars[aim_stickies].iValue = 0;
				gCvars2.Cvars[aim_lock].iValue = 2;
				gCvars2.Cvars[aim_deadringer].iValue = 0;
				gCvars2.Cvars[aim_foot].iValue = 0;
				gCvars2.Cvars[melee_fov].iValue = 60;
				gCvars2.Cvars[melee_smooth].iValue = 10;
				if ( gPlayerVars.iClass == TF2_Heavy )
				{
					gCvars2.Cvars[misc_nospread].iValue = 1;
				}
				else
				{
					gCvars2.Cvars[misc_nospread].iValue = 0;
				}
				return 0;
			}
			if ( keynum == 100 ) //F9
			{
				gCvars2.Cvars[aim_fov].iValue = 360;
				gCvars2.Cvars[aim_smooth].iValue = 0;
				gCvars2.Cvars[aim_cloaked].iValue = 0;
				gCvars2.Cvars[aim_mode].iValue = 1;
				gCvars2.Cvars[aim_silentaim].iValue = 1;
				gCvars2.Cvars[aim_sentry].iValue = 1;
				if (gPlayerVars.iClass != TF2_Sniper )
				{
					gCvars2.Cvars[aim_stickies].iValue = 1;
				}
				gCvars2.Cvars[aim_lock].iValue = 0;
				gCvars2.Cvars[melee_fov].iValue = 360;
				gCvars2.Cvars[melee_smooth].iValue = 0;
				gCvars2.Cvars[misc_norecoil].iValue = 1;
				gCvars2.Cvars[misc_nospread].iValue = 1;
				return 0;
			}

			if( keynum == 72 ) //insert
			{
				gCheatMenu.bMenuActive = !gCheatMenu.bMenuActive;
				/*if( !gCheatMenu.bMenuActive )
				{
					gCvars2.WriteCvars( );
				}*/
				return 0;
			}
			if(gCheatMenu.bMenuActive)
			{
				if(keynum == 88 || keynum == 112) // Up
				{
					gCheatMenu.DoUp();
					return 0;
				}
				if(keynum == 90 || keynum == 113 ) // Down
				{
					gCheatMenu.DoDown();
					return 0;
				}
				if(keynum == 89 || keynum == 107 ) // Left
				{
					gCheatMenu.DoLeft();
					return 0;
				}
				else if(keynum == 91 || keynum == 108 ) // Right
				{
					gCheatMenu.DoRight();
					return 0;
				}
			}
		}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Hooked_IN_KeyEvent");
	}

	VMTManager& hook = VMTManager::GetHook(pClient);
	return hook.GetMethod<int(__thiscall*)(PVOID, int, int, const char*)>(gVFuncOffsets.inKeyFn)(pClient, eventcode, keynum, pszCurrentBinding);
}
//============================================================================================
DWORD WINAPI dwCritThread( LPVOID lpArguments )
{
	int iLine = __LINE__;

	while (1)
	{
		try
		{
			iLine = __LINE__;
			if (gPlayerVars.bIsQuitting == true )
			{
				return 0;
			}

			//if (bNeedsToDraw)
			//{
				//
				//bNeedsToDraw = false;
			//}

			//if (*(PDWORD)gOffsets.dwPure != NULL)
				//*(PDWORD)gOffsets.dwPure = NULL;

			iLine = __LINE__;
			if (gInts.Engine->IsInGame() == false)
			{
				continue;
			}

			//DWORD processIdFocused;
			//GetWindowThreadProcessId(GetForegroundWindow(), &processIdFocused);
			//if (processIdFocused == GetCurrentProcessId() && !IsWindowVisible(gDrawManager->m_Hwnd))
				//SetWindowPos(gDrawManager->m_Hwnd, HWND_TOP, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

			iLine = __LINE__;

			CBaseEntity* pBaseEntity = GetBaseEntity(me);

			//GLOW
			//if (CV(esp_glow))
			//{
			//	iLine = __LINE__;
			//	try
			//	{
			//		if (pBaseEntity != NULL)
			//		{
			//			for (int iCounter = 0; iCounter <= gInts.Globals->maxclients; iCounter++)
			//			{
			//				iLine = __LINE__;
			//				if (iCounter == me)
			//				{
			//					continue;
			//				}
			//				CBaseEntity* pEntity = GetBaseEntity(iCounter);
			//
			//				if (pEntity == NULL)
			//				{
			//					continue;
			//				}
			//				iLine = __LINE__;
			//
			//				if ((gOffsets.bIsGlowEnabled(pEntity) == true) &&
			//					(
			//					((gInts.Engine->IsTakingScreenshot() == true) ||
			//					(gOffsets.iGetTeamNum(pEntity) == gOffsets.iGetTeamNum(pBaseEntity))) &&
			//					(gOffsets.bHasObject(pEntity) == false)
			//					))
			//				{
			//					iLine = __LINE__;
			//					gOffsets.SetGlow(pEntity, false);
			//					iLine = __LINE__;
			//					pEntity->UpdateGlow();
			//				}
			//			}
			//		}
			//
			//	}
			//	catch (...)
			//	{
			//		gBaseAPI.LogToFile("Failed Crit Thread Glow %i %i", iLine, gInts.Globals->maxclients);
			//	}
			//
			//}

			if (pBaseEntity == NULL)
			{
				continue;
			}

			iLine = __LINE__;

			CBaseEntity* pBaseWeapon = gOffsets.pGetBaseCombatActiveWeapon(pBaseEntity);

			if ( (gPlayerVars.bHasGeneratedSeed == true) || (pBaseWeapon == NULL) || (gOffsets.bGetIsAlive(pBaseEntity) == false) )
			{
				continue;
			}

			iLine = __LINE__;

			static unsigned int iCommandNumber = 0x80000000;
			if (iCommandNumber > 0xFFFFFFF0)
			{
				iCommandNumber = 0x80000000;
			}
			iCommandNumber++;

			const unsigned int iSeed = gMiscFuncs.MD5_PseudoRandom(iCommandNumber) & 0x7FFFFFFF;

			iLine = __LINE__;

			//if (gPlayerVars.bHasMinigun && (CV(aim_fov) != 360))
			//{
			//	if (CV(misc_nospread) && bWasLastFrameNospread == false)
			//	{
			//		if ((iSeed & 255) != 39)
			//		{
			//			continue;
			//		}
			//	}
			//}
			//else
			//{
			//	int iSeedCompare = gPlayerVars.bHasMinigun ? 39 : 33;
			//	if (CV(misc_nospread) && gPlayerVars.bHasMeleeWeapon == false)
			//	{
			//		if ((iSeed & 255) != iSeedCompare)
			//		{
			//			continue;
			//		}
			//	}
			//}

			iLine = __LINE__;

			unsigned int uiSeedXor = (pBaseWeapon->GetIndex() << 8) | (me);

			bool bIsCrit;

			if (gPlayerVars.bHasMeleeWeapon)
			{
				uiSeedXor <<= 8;
			}

			gMiscFuncs.RandomSeed(iSeed ^ uiSeedXor);

			iLine = __LINE__;

			if (gMiscFuncs.bIsNoCrits(gPlayerVars.iWeaponID))
			{
				continue;
			}
			else
			{
				bIsCrit = (80 > gMiscFuncs.RandomInt(0, 9999));
			}

			iLine = __LINE__;

			if (bIsCrit == true)
			{
				gPlayerVars.command_number = iCommandNumber;
				gPlayerVars.random_seed = iSeed;
				gPlayerVars.bHasGeneratedSeed = true;
				//gBaseAPI.LogToFile("Signed %i UnSigned: %u", iCommandNumber, iCommandNumber);
				iLine = __LINE__;
				continue;
			}
		}
		catch (...)
		{
			gBaseAPI.LogToFile("Failed CritThread %i", iLine);
		}
	}
	return 0;
}