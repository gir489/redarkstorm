#include "SDK.h"

class CPlayerVariables
{
public:
	bool bHasMeleeWeapon;
	bool bHasFlameThrower;
	bool bHasMinigun;
	bool bIsOKToFireSniper;
	bool bIsRageMode;
	bool bHasGeneratedSeed;
	bool bIsQuitting;
	bool bInScore;
	int iHealth;
	int iMaxHealth;
	int iClass;
	int iClip;
	int iClipMax;
	int iWeaponSlot;
	int iWeaponID;
	int iPlayerCond;
	int iPlayerCondEx;
	int iFlags;
	int iUserId;
	int iTeamNum;
	int command_number;
	int random_seed;
	char chName[34];
	Vector vecEyePos;
	Vector vecAimAngles;
	CBaseEntity* pBaseWeapon;
	CBaseEntity* Sentry;
	CBaseEntity* Sticky;
	Vector vecSentry;
	DWORD dwStartHuntsmanTime;
};

class CEntityIDS
{
public:
	int iGrenadeID, iRocketID, iArrowID, iMedicArrowID,
		iBaseballID, iOrnamentID, iJarateID, iSniperDotID,
		iMilkID, iSentryRocketID, iFlareID, iEnergyBall,
		iSpawnRoomID, iSentryID, iDispenserID, iTeleporterID,
		iIntelID, iCTFPlayerID, iCleverID, iCTFAmmoPack,
		iCurrencyPackID;
};

class CGlobalCvars
{
public:
	IConVar* name;
	IConVar* sv_client_min_interp_ratio;
	IConVar* sv_gravity;
	IConVar* sv_maxupdaterate;
};

extern CPlayerVariables gPlayerVars;
extern CEntityIDS gEntIDs;
extern CGlobalCvars gConVars;