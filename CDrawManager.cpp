#include "CDrawManager.h"
#include "CCheatMenu.h"
#include "ESP.h"
#include "COffsets.h"
#include "Client.h"
#include "CAimbot.h"
#include "CAnnouncer.h"

CVfuncOffsets gVFuncOffsets;
COffsets gOffsets;
//===================================================================================
typedef HRESULT(_stdcall *Present_T)(void*, const RECT*, RECT*, HWND, RGNDATA*);
Present_T OriginalPresent;
//===================================================================================
HRESULT _stdcall HookedPresent(LPDIRECT3DDEVICE9 pDevice, RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride, RGNDATA* pDirtyRegion)
{
	int iLine = __LINE__;
	static int iTracker = 0;
	static int iIterator = 0;
	try
	{
		if (gDrawManager->Init(pDevice) && gInts.Engine->IsInGame())
		{
			iLine = __LINE__;
			static CDrawElement drawElementCopy[MAX_DRAW_ELEMENTS+1];
			if (gDrawManager->readyToCopy == true)
			{
				iTracker = gDrawManager->GetTracker();
				memcpy(drawElementCopy, gDrawManager->m_drawElement, sizeof(CDrawElement) * (iTracker));
				/*static bool bPressed = false;
				if (gMiscFuncs.bIsKeyPressed(VK_F10))
				{
					if (bPressed == false)
						gDrawManager->GetTextures();
					bPressed = true;
				}
				else if (bPressed == true)
				{
					bPressed = false;
				}*/
				gDrawManager->readyToCopy = false;
			}
			iLine = __LINE__;
			gDrawManager->m_Direct3DXSprite->Begin(D3DXSPRITE_SORT_TEXTURE|D3DXSPRITE_ALPHABLEND);
			iLine = __LINE__;
			gDrawManager->DrawScoreboardIcons();
			iLine = __LINE__;
			while (iIterator < iTracker)
			{
				iLine = __LINE__;
				CDrawElement& element = drawElementCopy[iIterator];
				iLine = __LINE__;
				if (element.font != NULL)
				{
					iLine = __LINE__;
					gDrawManager->DrawStringRawFont(element.x, element.y, element.dwColor, element.chString, element.font);
				}
				else
				{
					if (element.bNeedsSizeOfLastIndex)
					{
						iLine = __LINE__;
						CDrawElement& lastElement = drawElementCopy[iIterator - 1];
						iLine = __LINE__;
						gDrawManager->DrawStringActual(element.x + gDrawManager->GetPixelTextSize(lastElement.chString), element.y, element.dwColor, element.chString);
					}
					else
					{
						iLine = __LINE__;
						gDrawManager->DrawStringActual(element.x, element.y, element.dwColor, element.chString);
					}
				}
				iIterator++;
			}
			iIterator = 0;
			iLine = __LINE__;
			gAnnouncer.Render();
			iLine = __LINE__;
			if (gCheatMenu.bMenuActive)
			{
				gCheatMenu.DrawMenu();
			}
			if (CV(esp_radar))
			{
				iLine = __LINE__;
				gRadar.DrawRadarBack();
			}
			gDrawManager->m_Direct3DXSprite->End();
		}
		return OriginalPresent(pDevice, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed HookedPresent Line: %i iTracker: %i iIterator %i", iLine, iTracker, iIterator);
		return NULL;
	}
}
//===================================================================================
CDrawManager* gDrawManager;
#define ESP_HEIGHT 14
//===================================================================================
LRESULT CALLBACK WindowProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	return DefWindowProc( hWnd, message, wParam, lParam );
}
//===================================================================================
void CDrawManager::DrawStringActual( int x, int y, DWORD dwColor, const wchar_t *pszText )
{
	if (pszText == NULL)
		return;

	D3DXCOLOR color = D3DCOLOR_RGBA(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	RECT font_rect = { x-1, y-1, x + 50, 14 };
	m_Direct3DXFont->DrawTextW(m_Direct3DXSprite, pszText, wcslen(pszText), &font_rect, DT_NOCLIP, D3DCOLOR_RGBA(0, 0, 0, ALPHA(dwColor)));
	font_rect = { x+1, y+1, x + 50, 14 };
	m_Direct3DXFont->DrawTextW(m_Direct3DXSprite, pszText, wcslen(pszText), &font_rect, DT_NOCLIP, D3DCOLOR_RGBA(0, 0, 0, ALPHA(dwColor)));
	font_rect = { x, y, x + 50, 14 };
	m_Direct3DXFont->DrawTextW(m_Direct3DXSprite, pszText, wcslen(pszText), &font_rect, DT_NOCLIP, color);
}
//===================================================================================
void CDrawManager::DrawStringRawFont(int x, int y, DWORD dwColor, const wchar_t *pszText, ID3DXFont* font)
{
	D3DXCOLOR color = D3DCOLOR_RGBA(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	RECT font_rect = { x, y, x + 50, 14 };
	font->DrawTextW(m_Direct3DXSprite, pszText, wcslen(pszText), &font_rect, DT_NOCLIP, color);
}
//===================================================================================
void CDrawManager::DrawScoreboardIcons()
{
	if (gPlayerVars.bInScore)
	{
		int iLine = __LINE__;
		try
		{
			iLine = __LINE__;

			static Data iconHeightArray[] = { { 720,34 },{ 1080,51 },{ 1440,69 } };
			static float iconHeight = gDrawManager->InterpolateScreenValues(iconHeightArray, sizeof(iconHeightArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
			static Data yPosArray[] = { { 720,145 },{ 1080,216 },{ 1440,272 } };
			static float positionY = gDrawManager->InterpolateScreenValues(yPosArray, sizeof(yPosArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
			static Data blueXArray[] = { { 720,1039.5f },{ 1080,1560 },{ 1440,2079 } };
			static float blueX = gDrawManager->InterpolateScreenValues(blueXArray, sizeof(blueXArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
			static Data redXArray[] = { { 720,559.5f },{ 1080,840 },{ 1440,1119 } };
			static float redX = gDrawManager->InterpolateScreenValues(redXArray, sizeof(redXArray) / sizeof(Data), gDrawManager->GetScreenSizeHeight());
			static D3DXVECTOR3 Position;
			Position.y = positionY;
			if (gPlayerVars.iTeamNum == TEAM_BLUE)
			{
				iLine = __LINE__;
				Position.x = blueX;
				std::list<teamStruct>::iterator it;
				IGameResources* gameResources = gMiscFuncs.GetGameResources();
				for (it = redTeam.begin(); it != redTeam.end(); ++it)
				{
					teamStruct ts = *it;
					iLine = __LINE__;
					m_Direct3DXSprite->Draw(m_ClassLogos[gameResources->IsAlive(ts.iEntID)][gOffsets.GetClassFromResource(ts.iEntID)], NULL, NULL, &Position, 0xFFFFFFFF);
					Position.y += iconHeight;
				}
			}
			else if (gPlayerVars.iTeamNum == TEAM_RED)
			{
				iLine = __LINE__;
				Position.x = redX;
				std::list<teamStruct>::iterator it;
				IGameResources* gameResources = gMiscFuncs.GetGameResources();
				for (it = blueTeam.begin(); it != blueTeam.end(); ++it)
				{
					teamStruct ts = *it;
					iLine = __LINE__;
					m_Direct3DXSprite->Draw(m_ClassLogos[gameResources->IsAlive(ts.iEntID)][gOffsets.GetClassFromResource(ts.iEntID)], NULL, NULL, &Position, 0xFFFFFFFF);
					Position.y += iconHeight;
				}
			}
		}
		catch (...)
		{
			gBaseAPI.LogToFile("Failed DrawScoreboardIcons %i", iLine);
		}
	}
}
//===================================================================================
void CDrawManager::DrawStringWithFont(int x, int y, ID3DXFont* font, DWORD dwColor, const wchar_t *pszText, bool bNeedsSizeOfLastIndex)
{
	if (m_iTracker >= (MAX_DRAW_ELEMENTS - 1) || pszText == NULL)
		return;
	CDrawElement& element = m_drawElement[m_iTracker];
	element.x = x;
	element.y = y;
	element.dwColor = dwColor;
	element.font = font;
	wcscpy_s(element.chString, pszText);
	element.bNeedsSizeOfLastIndex = bNeedsSizeOfLastIndex;
	m_iTracker++;
}
//===================================================================================
void CDrawManager::DrawString(int x, int y, DWORD dwColor, const wchar_t *pszText, bool bNeedsSizeOfLastIndex)
{
	if (m_iTracker >= (MAX_DRAW_ELEMENTS - 1) || pszText == NULL)
		return;
	CDrawElement& element = m_drawElement[m_iTracker];
	element.x = x;
	element.y = y;
	element.dwColor = dwColor;
	element.font = NULL;
	wcscpy_s(element.chString, pszText);
	element.bNeedsSizeOfLastIndex = bNeedsSizeOfLastIndex;
	m_iTracker++;
}
//===================================================================================
void CDrawManager::DrawString( int x, int y, DWORD dwColor, const char *pszText, ... )
{
	if( pszText == NULL )
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start( va_alist, pszText );
	vsprintf_s( szBuffer, pszText, va_alist );
	va_end( va_alist );

	wsprintfW( szString, L"%S", szBuffer );
	
	DrawString(x, y, dwColor, szString);
}
//===================================================================================
void CDrawManager::DrawStringWithFont(int x, int y, ID3DXFont* font, DWORD dwColor, const char *pszText, ...)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	DrawStringWithFont(x, y, font, dwColor, szString);
}
//===================================================================================
void CDrawManager::DrawStringNeedsSizeOfLast(int x, int y, DWORD dwColor, const char *pszText, ...)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	DrawString(x, y, dwColor, szString, true);
}
//===================================================================================
void CDrawManager::DrawStringActual(int x, int y, DWORD dwColor, const char *pszText, ...)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	DrawStringActual(x, y, dwColor, szString);
}
//===================================================================================
BYTE CDrawManager::GetESPHeight( )
{
	return m_iFontHeight;
}
//===================================================================================
int CDrawManager::GetPixelTextSize ( const char *pszText )
{
	if( pszText == NULL )
		return NULL;

	wchar_t szString[1024] = { '\0' };

	wsprintfW( szString, L"%S", pszText );

	return GetPixelTextSize(szString);
}
//===================================================================================
int CDrawManager::GetPixelTextSize ( const wchar_t *pszText )
{
	if (pszText == NULL)
		return 0;
	static RECT rcRect = { 0, 0, 0, 0 };
	m_Direct3DXFont->DrawTextW(m_Direct3DXSprite, pszText, wcslen(pszText), &rcRect, DT_CALCRECT, 0);
	return rcRect.right - rcRect.left;
}
//===================================================================================
void CDrawManager::DrawOutlineRect( int x, int y, int w, int h, DWORD dwColor )
{
	DrawRect(x - 1, y - 1, w + 2, 1, dwColor);
	DrawRect(x - 1, y, 1, h - 1, dwColor);
	DrawRect(x + w, y, 1, h - 1, dwColor);
	DrawRect(x - 1, y + h - 1, w + 2, 1, dwColor);
}
//===================================================================================
void CDrawManager::DrawRect( int x, int y, int w, int h, DWORD dwColor )
{
	D3DXCOLOR color = D3DCOLOR_RGBA(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	D3DXVECTOR2 vLine[2];

	m_Direct3DXLine->SetWidth(w);
	m_Direct3DXLine->SetAntialias(false);
	m_Direct3DXLine->SetGLLines(true);

	vLine[0].x = x + w / 2;
	vLine[0].y = y;
	vLine[1].x = x + w / 2;
	vLine[1].y = y + h;

	m_Direct3DXLine->Begin();
	m_Direct3DXLine->Draw(vLine, 2, color);
	m_Direct3DXLine->End();
}
//===================================================================================
DWORD CDrawManager::dwGetTeamColor( int iIndex )
{
	static DWORD dwColors[] = { 0xFFFFFFFF, //Dummy
					 0x0080FFFF, // 1 Teamone (BLUE)
					 0xFF8000FF, // 2 Teamtwo (RED)
					 0x0080FFFF, // 3 teamthree (BLUE)
					 0xFF8000FF // 4 teamfour (RED)
					};
	return dwColors[ iIndex ];
}
//===================================================================================
bool CDrawManager::WorldToScreen( Vector &vOrigin, Vector &vScreen )
{
	int iLine = __LINE__;
	try
	{
		if ( &vOrigin == NULL )
		{
			gBaseAPI.LogToFile("vOrigin was null in WorldToScreen");
			return false;
		}
		if ( &vScreen == NULL)
		{
			gBaseAPI.LogToFile("vScreen was null in WorldToScreen");
			return false;
		}
		iLine = __LINE__;

		const matrix3x4& worldToScreen = gInts.Engine->WorldToScreenMatrix();
		iLine = __LINE__;

		float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3];
		vScreen.z = 0;

		iLine = __LINE__;

		if (w > 0.001)
		{
			float fl1DBw = 1 / w;
			vScreen.x = (m_iWidth / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * m_iWidth + 0.5);
			iLine = __LINE__;
			vScreen.y = (m_iHeight / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * m_iHeight + 0.5);
			return true;
		}
		return false;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed WorldToScreen %i", iLine);
		return false;
	}
}
//===================================================================================
int CDrawManager::GetScreenSizeHeight()
{
	return m_iHeight;
}
//===================================================================================
int CDrawManager::GetScreenSizeWidth()
{
	return m_iWidth;
}
//===================================================================================
CDrawManager::CDrawManager()
{
	m_Direct3D9Device = nullptr;
	m_Direct3DXFont = nullptr;
	m_Direct3DXLine = nullptr;
	m_iHeight = 0;
	m_iWidth = 0;
	ZeroMemory(m_drawElement, sizeof(m_drawElement));
	readyToCopy = false;
}

void CDrawManager::Clear()
{
	m_iTracker = 0;
}

int CDrawManager::GetTracker()
{
	return m_iTracker;
}

std::string CDrawManager::GetClassLogo(const char* chName)
{
	std::ostringstream stringStream;
	int height = this->GetScreenSizeHeight();
	if (height <= 720)
		height = 720;
	else if (height >= 1440)
		height = 1440;
	else
		height = 1080;
	stringStream << "classlogos\\" << height << "\\" << chName;
	return stringStream.str();
}

void CDrawManager::GetTextures()
{
	int classLogosReturn = 0;
	//Dead
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("unknown.png").c_str(), &m_ClassLogos[0][0]); //This scenario is used when the player hasn't picked a class yet.
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("scout_dead.png").c_str(), &m_ClassLogos[0][1]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("sniper_dead.png").c_str(), &m_ClassLogos[0][2]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("soldier_dead.png").c_str(), &m_ClassLogos[0][3]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("demoman_dead.png").c_str(), &m_ClassLogos[0][4]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("medic_dead.png").c_str(), &m_ClassLogos[0][5]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("heavy_dead.png").c_str(), &m_ClassLogos[0][6]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("pyro_dead.png").c_str(), &m_ClassLogos[0][7]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("spy_dead.png").c_str(), &m_ClassLogos[0][8]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("engineer_dead.png").c_str(), &m_ClassLogos[0][9]);
	//Alive
	m_ClassLogos[1][0] = m_ClassLogos[0][0]; //This scenario should never happen; duplicate unknown.png scenario Justin Case.
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("scout.png").c_str(), &m_ClassLogos[1][1]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("sniper.png").c_str(), &m_ClassLogos[1][2]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("soldier.png").c_str(), &m_ClassLogos[1][3]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("demoman.png").c_str(), &m_ClassLogos[1][4]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("medic.png").c_str(), &m_ClassLogos[1][5]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("heavy.png").c_str(), &m_ClassLogos[1][6]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("pyro.png").c_str(), &m_ClassLogos[1][7]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("spy.png").c_str(), &m_ClassLogos[1][8]);
	classLogosReturn += D3DXCreateTextureFromFile(m_Direct3D9Device, GetClassLogo("engineer.png").c_str(), &m_ClassLogos[1][9]);
	if (D3D_OK != classLogosReturn)
		gBaseAPI.LogToFile("Failed to find classlogos");
}

bool CDrawManager::Init(IDirect3DDevice9* pDevice)
{
	int iLine = __LINE__;
	try {
		if (m_Direct3D9Device == nullptr)
		{
			m_Direct3D9Device = pDevice;
			iLine = __LINE__;
			RECT rect;
			iLine = __LINE__;
			D3DDEVICE_CREATION_PARAMETERS parameters;
			iLine = __LINE__;
			m_Direct3D9Device->GetCreationParameters(&parameters);
			iLine = __LINE__;
			GetClientRect(parameters.hFocusWindow, &rect);
			iLine = __LINE__;
			m_iHeight = rect.bottom;
			iLine = __LINE__;
			m_iWidth = rect.right;
			iLine = __LINE__;
			D3DXCreateLine(m_Direct3D9Device, &m_Direct3DXLine);
			iLine = __LINE__;
			Data normalFontArray[] = { { 720,12 },{ 1080,13 },{ 1440,14 } };
			iLine = __LINE__;
			m_iFontHeight = (BYTE)InterpolateScreenValues(normalFontArray, sizeof(normalFontArray) / sizeof(Data), GetScreenSizeHeight());
			iLine = __LINE__;
			D3DXCreateFont(m_Direct3D9Device, m_iFontHeight, 0, FW_BOLD, 1, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Tahoma", &m_Direct3DXFont);
			iLine = __LINE__;
			Data scoreBoardFontArray[] = { { 720,13 },{ 1080,21 },{ 1440,24 } };
			iLine = __LINE__;
			int scoreBoardFontHeight = InterpolateScreenValues(scoreBoardFontArray, sizeof(scoreBoardFontArray) / sizeof(Data), GetScreenSizeHeight());
			iLine = __LINE__;
			D3DXCreateFont(m_Direct3D9Device, scoreBoardFontHeight, 0, FW_BOLD, 1, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Verdana", &m_ScoreboardPlayerFont);
			iLine = __LINE__;
			D3DXCreateSprite(m_Direct3D9Device, &m_Direct3DXSprite);
			iLine = __LINE__;
			GetTextures();
			iLine = __LINE__;
			return true;
		}
		else
		{
			return true;
		}
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed CDrawManager::Init %i %i", iLine, GetLastError());
		return false;
	}
}

//Using Lagrange's Interpolation, find equivalent unknown values.
double CDrawManager::InterpolateScreenValues(Data dataSet[], int arraySize, int valueToFind)
{
	double result = 0; // Initialize result
	for (int i = 0; i < arraySize; i++)
	{
		// Compute individual terms of above formula
		double term = dataSet[i].y;
		for (int j = 0; j < arraySize; j++)
		{
			if (j != i)
				term = term * (valueToFind - dataSet[j].x) / double(dataSet[i].x - dataSet[j].x);
		}

		// Add current term to result
		result += term;
	}

	return result;
}