#include "SDK.h"
#include <time.h>
#include <fstream>

#define INRANGE(x,a,b)    (x >= a && x <= b) 
#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))

DWORD CBaseAPI::dwFindPattern(DWORD dwAddress, DWORD dwLength, const char* szPattern)
{
	const char* pat = szPattern;
	DWORD firstMatch = NULL;
	for (DWORD pCur = dwAddress; pCur < dwLength; pCur++)
	{
		if( !*pat ) return firstMatch;
		if( *(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte( pat ) ) {
			if( !firstMatch ) firstMatch = pCur;
			if( !pat[2] ) return firstMatch;
			if( *(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?' ) pat += 3;
			else pat += 2;
		} else {
			pat = szPattern;
			firstMatch = 0;
		}
	}
	return NULL;
}
//===================================================================================
HMODULE CBaseAPI::GetModuleHandleSafe( const char* pszModuleName )
{
	HMODULE hmModuleHandle = NULL;

	do
	{
		hmModuleHandle = GetModuleHandle( pszModuleName );
		Sleep( 1 );
	}
	while(hmModuleHandle == NULL);

	return hmModuleHandle;
}
//===================================================================================
//bool CBaseAPI::KillProcessByName(char *szProcessToKill)
//{
//	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
//    PROCESSENTRY32 pEntry;
//    pEntry.dwSize = sizeof (pEntry);
//    BOOL hRes = Process32First(hSnapShot, &pEntry);
//    while (hRes)
//    {
//        if (strcmp(pEntry.szExeFile, szProcessToKill) == 0)
//        {
//            HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
//                                          (DWORD) pEntry.th32ProcessID);
//            if (hProcess != NULL)
//            {
//                TerminateProcess(hProcess, 9);
//                CloseHandle(hProcess);
//            }
//        }
//        hRes = Process32Next(hSnapShot, &pEntry);
//    }
//    CloseHandle(hSnapShot);
//	return true;
//}
//===================================================================================
void CBaseAPI::LogToFile( const char * pszMessage, ... )
{
	va_list va_alist;
	char szLogbuf[4096] = "";
	va_start ( va_alist, pszMessage );
	vsnprintf ( szLogbuf + strlen( szLogbuf ), sizeof( szLogbuf ) - strlen( szLogbuf ), pszMessage, va_alist);
	va_end ( va_alist );
#ifdef _DEBUG
	sprintf_s ( szLogbuf,  "%s\n", szLogbuf );
#endif
	ofstream myfile;
	myfile.open("C:\\Fraps\\error.txt", ofstream::app);
	if (myfile.is_open())
	{
		myfile << szLogbuf;
		myfile.close();
#ifndef _DEBUG
		exit(EXIT_SUCCESS);
#endif
	}
}
//===================================================================================
DWORD CBaseAPI::GetClientSignature(const char* chPattern )
{
	static HMODULE hmModule = GetModuleHandleSafe("client.dll");
	static PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	static PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}
//===================================================================================
DWORD CBaseAPI::GetEngineSignature ( const char* chPattern )
{
	static HMODULE hmModule = GetModuleHandleSafe("engine.dll");
	static PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	static PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}
//===================================================================================
DWORD CBaseAPI::GetVguiMatSurfaceSignature(const char* chPattern)
{
	static HMODULE hmModule = GetModuleHandleSafe("vguimatsurface.dll");
	static PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	static PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}
//===================================================================================
DWORD CBaseAPI::GetGameOverlayRendererSignature(const char* chPattern)
{
	static HMODULE hmModule = GetModuleHandleSafe("gameoverlayrenderer.dll");
	static PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hmModule;
	static PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((DWORD)hmModule) + pDOSHeader->e_lfanew);
	return dwFindPattern(((DWORD)hmModule) + pNTHeaders->OptionalHeader.BaseOfCode, ((DWORD)hmModule) + pNTHeaders->OptionalHeader.SizeOfCode, chPattern);
}
//===================================================================================
void CBaseAPI::ErrorBox (const char* error )
{
#ifdef _DEBUG
	string s = error;
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    wstring r(buf);
    delete[] buf;
	MessageBoxW(NULL, r.c_str(), 0, 0);
#endif
}

CBaseAPI gBaseAPI;