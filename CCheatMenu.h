#pragma once
//===================================================================================
#include <windows.h>
//===================================================================================
 struct CMenuItems_t
{
	string szTitle;
	int iType;
	int flMin;
	int flMax;
	int flStep;
};
//===================================================================================
class CCheatMenu
{
public:
	void DrawMenu( );
	void DoUp();
	void DoDown();
	void DoLeft();
	void DoRight();
	int GetValue(int index);
	void SetValue(int index, int flValue);
	bool bMenuActive;
private:
	int iMenuIndex;
	int iLastIndex;
	int iLastGroup;
	int iNextGroup;
	int iNextIndex;
};
//===================================================================================
extern CCheatMenu gCheatMenu;
//===================================================================================