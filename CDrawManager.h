#pragma once
//===================================================================================
#include "SDK.h"
#include "d3dx9.h"
#include <list>
#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )

#define MAX_DRAW_ELEMENTS 1500
#define RED(COLORCODE)	((int) ( COLORCODE >> 24) )
#define BLUE(COLORCODE)	((int) ( COLORCODE >> 8 ) & 0xFF )
#define GREEN(COLORCODE)	((int) ( COLORCODE >> 16 ) & 0xFF )
#define ALPHA(COLORCODE)	((int) COLORCODE & 0xFF )
#define COLORCODE(r,g,b,a)((DWORD)((((r)&0xff)<<24)|(((g)&0xff)<<16)|(((b)&0xff)<<8)|((a)&0xff)))
//===================================================================================
typedef HRESULT(_stdcall *Present_T)(void*, const RECT*, RECT*, HWND, RGNDATA*);
//===================================================================================
class CDrawElement
{
public:
	int x;
	int y;
	wchar_t chString[100];
	DWORD dwColor;
	bool bNeedsSizeOfLastIndex;
	ID3DXFont* font;
};
//===================================================================================
struct teamStruct
{
	int iEntID;
	int iScore;
};
//===================================================================================
struct Data
{
	float x, y;
};
//===================================================================================
class CDrawManager
{
	IDirect3DDevice9*	m_Direct3D9Device;
	ID3DXFont*          m_Direct3DXFont;
	ID3DXLine*			m_Direct3DXLine;
	int					m_iHeight;
	BYTE				m_iFontHeight;
	int					m_iWidth;
	int					m_iTracker;
	std::string			GetClassLogo(const char* chName);
public:
	LPDIRECT3DTEXTURE9	m_ClassLogos[2][10];
	ID3DXFont*			m_ScoreboardPlayerFont;
	ID3DXSprite*		m_Direct3DXSprite;
	CDrawManager();

	void GetTextures();
	void DrawScoreboardIcons();
	void DrawStringWithFont(int x, int y, ID3DXFont* font, DWORD dwColor, const wchar_t *pszText, bool bNeedsSizeOfLastIndex = false);
	void DrawStringRawFont(int x, int y, DWORD dwColor, const wchar_t *pszText, ID3DXFont* font);
	void DrawString(int x, int y, DWORD dwColor, const wchar_t *pszText,  bool bNeedsSizeOfLastIndex = false);
	void DrawString(int x, int y, DWORD dwColor, const char *pszText, ... );
	void DrawStringWithFont(int x, int y, ID3DXFont* font, DWORD dwColor, const char *pszText, ...);
	void DrawStringNeedsSizeOfLast(int x, int y, DWORD dwColor, const char *pszText, ...);
	void DrawStringActual(int x, int y, DWORD dwColor, const wchar_t *pszText);
	void DrawStringActual(int x, int y, DWORD dwColor, const char *pszText, ...);
	BYTE GetESPHeight( );
	int GetPixelTextSize ( const char *pszText );
	int GetPixelTextSize ( const wchar_t *pszText );
	void DrawOutlineRect(int x, int y, int w, int h, DWORD dwColor);
	void DrawRect( int x, int y, int w, int h, DWORD dwColor );
	void Clear();

	bool Init(IDirect3DDevice9* pDevice);

	int GetScreenSizeHeight();
	int GetScreenSizeWidth();
	
	int					GetTracker();
	CDrawElement		m_drawElement[MAX_DRAW_ELEMENTS];
	std::list<teamStruct> redTeam;
	std::list<teamStruct> blueTeam;

	bool WorldToScreen( Vector &vOrigin, Vector &vScreen );
	DWORD dwGetTeamColor( int iIndex );
	bool readyToCopy;

	double InterpolateScreenValues(Data dataSet[], int arraySize, int valueToFind);
};
//===================================================================================
extern CDrawManager* gDrawManager;
//===================================================================================