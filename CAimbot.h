#pragma once

#include "SDK.h"

class IHandleEntity
{
public:
	virtual ~IHandleEntity() {}
	virtual void SetRefEHandle ( DWORD* dwRefEHandle ) = 0;
	virtual DWORD GetRefEHandle( void ) = 0;
	CBaseEntity* GetEntity( void )
	{
		return gInts.EntList->GetClientEntity(((*(PINT)(GetRefEHandle())) & ((1 << (11 + 1)) - 1)));
	}
};

class TraceFilter : public ITraceFilter
{
public:
	virtual bool ShouldHitEntity( IHandleEntity *pEntityHandle, int contentsMask )
	{
		CBaseEntity *pBaseEntity = pEntityHandle->GetEntity();
		if ( pBaseEntity->GetClientClass()->iClassID == gEntIDs.iSpawnRoomID )
		{
			return false; 
		}
		if (pBaseEntity->GetClientClass()->iClassID == gEntIDs.iIntelID)
		{
			return false;
		}
		if ( pBaseEntity->GetIndex() == gInts.Engine->GetLocalPlayer() )
		{
			return false;
		}
		return true; 
	}
	virtual int	GetTraceType( ) const
	{
		return 0;
	}
};

class TraceFilter2 : public ITraceFilter
{
public:
	virtual bool ShouldHitEntity( IHandleEntity *pEntityHandle, int contentsMask )
	{
		CBaseEntity *pBaseEntity = pEntityHandle->GetEntity();
		if ( pBaseEntity->GetClientClass()->iClassID == gEntIDs.iSpawnRoomID )
		{
			return false; 
		}
		if (pBaseEntity->GetClientClass()->iClassID == gEntIDs.iIntelID)
		{
			return false;
		}
		if ( pBaseEntity->GetClientClass()->iClassID == gEntIDs.iGrenadeID )
		{
			return false; 
		}
		if (pBaseEntity->GetIndex() == gInts.Engine->GetLocalPlayer())
		{
			return false;
		}
		return true;
	}
	virtual int	GetTraceType() const
	{
		return 0;
	}
};

class CAimbot
{
public:
	CAimbot( );

	void	DropTarget		( void );
	void	FindTarget		( void );
	void	SetTarget		( int iIndex );
	void	SilentAimFix	( CUserCmd* pCommand, Vector& qaViewAngles );
	void	AimAtTarget		( );
	void	AimAtVector		( Vector vecAim );
	void	AimAtSticky		( CBaseEntity* pSticky, CUserCmd* pCommand );
	void	FindTargetCall	( int iIndex );
	void	ClampAngle		( Vector& qaAng );
	void	AngleVectors( Vector& angles, Vector *forward, Vector *right, Vector *up );
	void	VectorAngles( Vector &forward, Vector &angles );
	void    VectorAngles( const Vector &forward, const Vector &pseudoup, Vector &angles );

	float	flGetFOV		( Vector vOrigin );
	float	flGetDistance	( Vector vOrigin );
	float	flGetDistance	( Vector vStart, Vector vEnd );
	float	GetDrop			( float dist, float speed, CBaseEntity *pBaseEntity, Vector vOrg );
	float	flGetHuntsmanDrop ( float flChargeTime );
	float	TimeOfImpact	( float dist, float speed, CBaseEntity *pBaseEntity, Vector vOrg );
	float	flGetProjectileSpeed ( );
	float	GetWeaponGravity( void );
	float	GetInterp		( void );

	Vector	BallisticPrediction( CBaseEntity* pEntity );
	Vector	vecGetHitbox(CBaseEntity* CBaseEntity, int iHitbox );

	bool	bStateAndFriendCheck (CBaseEntity* pBaseEntity);
	int 	bTriggerbotTrace( CUserCmd* pCommand );
	bool	bCheckTeam		( int iIndex );
	bool	bIsTargetSpot	( int iIndex );
	bool	bHasTarget		( void );
	bool	bIsValidTarget	( int iIndex );
	bool	bIsValidEntity	( int iIndex );
	bool	bIsVisible		( Vector& vecEnemy, CBaseEntity* pBaseEntity );
	bool    bIsVisiblePlayer( Vector& vecStart, CBaseEntity* pBaseEntity );
	bool    checkStickyVis  ( CBaseEntity* pSticky, CBaseEntity* pPlayer );
	bool	bTraceToSentry	( Vector& vecStart );

	int		iGetTarget		( )	{	return m_nTarget;	}
	int		iGetHitbox		( int iWeaponID, bool bOnGround );

	enum triggerbotStatus
	{
		TRIGGERBOT_FAIL,
		TRIGGERBOT_SUCCESS,
		TRIGGERBOT_STREAM
	};


private:
	void SinCos( float radians, float *sine, float *cosine );
	void AngleVector( Vector& Angles, Vector *in );
	void VectorTransform( Vector& in1, const matrix3x4 &in2, Vector &out );

	int	iGetMeleeDistance( );

	int		m_nTarget;

	float flAimDistance[32];
	float flAimFOV[32];

	Vector	vTargetOrg;
	Vector	vMin,vMax;
	Vector	qAimAngles;
	Vector  qCurrentView;

	matrix3x4 pBoneToWorld[128];

	mstudiobbox_t* pbox;
};

extern CAimbot gAimbot;