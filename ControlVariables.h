#pragma once

// Uncomment for logging entities or classes.
//#define LOG_ENT

// Uncomment for Halloween event
//#define HALLOWEEN

// Dump all Netvars to file.
//#define NETVAR

enum { MAX_CHEAT_KEY = 16 }; // See gMiscFuncs.bIsKey implementation

/*============
||n = name
||t = title
||i = initializer
||a = min
||b = max
||s = incrementing variable
||f = Type (See MenuTypes_t)
=============*/

enum MenuTypes_t
{
	type_number,
	type_key,
	type_aim_method,
	type_onoff,
	type_aim_team,
	type_esp_rage,
	type_group,
};

class CControlVarType
{
public:
	const char* chMenuTitle;
	int iInitlizer, iMin, iMax, iIncrementer, iType, iValue;
	void Initialize( const char* menu, int init, int min, int max, int inc, int type )
	{
		chMenuTitle = menu;
		iInitlizer = init;
		iMin = min;
		iMax = max;
		iIncrementer = inc;
		iType = type;
	};
};

enum cvars_t
{
	aimbot,
	aim_key,
	aim_team,
	aim_mode,
	aim_friends,
	aim_cloaked,
	aim_fov,
	melee_fov,
	aim_smooth,
	melee_smooth,
	triggerbot,
	aim_silentaim,
	aim_lock,
	aim_foot,
	aim_prediction,
	aim_projectile,
	aim_airblast,
	aim_sentry,
	aim_stickies,
	esp,
	esp_friends,
	esp_health,
	esp_weapon,
	esp_name,
	esp_class,
	esp_state,
	esp_structure,
	esp_steam,
	esp_glow,
	esp_radar,
	misc,
	misc_disguise,
	misc_crits,
	misc_spywarning,
	misc_norecoil,
	misc_nospread,
	misc_spamintel,
	misc_bhop,
	main,
	aim_rage,
	esp_rage,
	aim_deadringer,
	misc_noisemaker
};

class CControlVars
{
public:
	CControlVarType Cvars[60];
	int iTotalCvars;
	CControlVars(void)
	{
		AddCvar("Aimbot",0,0,1,1,type_group);                                       
		AddCvar("Key", 6, 0, MAX_CHEAT_KEY, 1, type_key);
		AddCvar("Team", 1, 0, 2, 1, type_aim_team);
		AddCvar("Mode", 0, 0, 1, 1, type_aim_method);
		AddCvar("Friends", 0, 0, 1, 1, type_onoff);
		AddCvar("Cloaked", 0, 0, 1, 1, type_onoff);
		AddCvar("FoV", 6, 1, 360, 1, type_number);
		AddCvar("MeleeFoV", 60, 1, 360, 1, type_number);
		AddCvar("Smooth", 4, 0, 100, 1, type_number);
		AddCvar("MeleeSmooth", 10, 0, 100, 1, type_number);
		AddCvar("Triggerbot", 4, 0, MAX_CHEAT_KEY, 1, type_key);
		AddCvar("Silentaim", 0, 0, 2, 1, type_number);
		AddCvar("Lock", 2, 0, 2, 1, type_number);
		AddCvar("Foot", 0, 0, 1, 1, type_onoff);
		AddCvar("Prediction", 1, 0, 1, 1, type_onoff);
		AddCvar("Projectile", 0, 0, 1, 1, type_onoff);
		AddCvar("Airblast", 0, 0, 1, 1, type_onoff);
		AddCvar("Sentries", 0, 0, 1, 1, type_onoff);
		AddCvar("Stickies", 0, 0, 1, 1, type_onoff);
		AddCvar("ESP", 0, 0, 1, 1, type_group);
		AddCvar("FriendsESP", 1, 0, 2, 1, type_number);
		AddCvar("HealthESP", 2, 0, 2, 1, type_number);
		AddCvar("WeaponESP", 1, 0, 1, 1, type_onoff);
		AddCvar("NameESP", 1, 0, 1, 1, type_onoff);
		AddCvar("ClassESP", 1, 0, 1, 1, type_onoff);
		AddCvar("StateESP", 1, 0, 1, 1, type_onoff);
		AddCvar("StructureESP", 1, 0, 2, 1, type_number);
		AddCvar("SteamIDESP", 0, 0, 1, 1, type_onoff);
		AddCvar("Glow", 1, 0, 1, 1, type_onoff);
		AddCvar("RADAR", 1, 0, 1, 1, type_onoff);
		AddCvar("Misc", 0, 0, 1, 1, type_group);
		AddCvar("RemoveDisguises", 1, 0, 1, 1, type_onoff);
		AddCvar("CritKey", 10, 0, MAX_CHEAT_KEY, 1, type_key);
		AddCvar("SpyWarning", 1, 0, 1, 1, type_onoff);
		AddCvar("NoRecoil", 0, 0, 1, 1, type_onoff);
		AddCvar("NoSpread", 0, 0, 1, 1, type_onoff);
		AddCvar("DropIntel", 0, 0, 1, 1, type_onoff);
		AddCvar("BHop", 1, 0, 1, 1, type_onoff);
		AddCvar("Main", 1, 1, 1, 1, type_group);
		AddCvar("AimRage", 1, 0, 2, 1, type_aim_team);
		AddCvar("RageESP", 0, 0, 32, 1, type_esp_rage);
		AddCvar("AimDeadringer", 0, 0, 1, 1, type_onoff);
        AddCvar("Noisemaker", 0, 0, 1, 1, type_onoff);
	};
	void ReadCvars( void );
private:
	char m_szFileName[255];
	void AddCvar( const char* menu, int init, int min, int max, int inc, int type )
	{
		Cvars[iTotalCvars].Initialize(menu,init,min,max,inc,type);
		iTotalCvars++;
	};
};

#define CV(n) gCvars2.Cvars[##n].iValue
//===================================================================================
extern CControlVars gCvars2;
//===================================================================================