#include "CAnnouncer.h"
CAnnouncer gAnnouncer;
//=================================================================================
CAnnouncer::CAnnouncer(void)
{
	killStreakTimeout = 4.0f; //UT99 specs.
}
//=================================================================================
void CAnnouncer::AddListeners(void)
{
	gInts.EventManager->AddListener(this, "player_death", false);
	gInts.EventManager->AddListener(this, "player_spawn", false);
}
//=================================================================================
bool CAnnouncer::IsEventProperForKillCounter(IGameEvent *event)
{
	try
	{
#ifdef USE_TF2_KILLSTREAKS
		const char* weaponName = event->GetString("weapon");
		if (strstr(weaponName, "deflect_"))
		{
			return (!strcmp(weaponName, "deflect_promode")); //Recongizes reflected grenades but not rockets #valvelogic
		}
		if (!strcmp(weaponName, "world") || !strcmp(weaponName, "player")) //Suicides
		{
			return false;
		}
#else
		if (event->GetInt("death_flags", 0) & TF_DEATH_FEIGN_DEATH)
			return false;
#endif
		return true;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed IsEventProperForKillCounter");
		return false;
	}
}
//=================================================================================
void CAnnouncer::FireGameEvent(IGameEvent *event)
{
	try
	{
#ifdef _DEBUG
		if (!strcmp(event->GetName(), "player_death"))
		{
			gBaseAPI.LogToFile("player_death");
			gBaseAPI.LogToFile("\tAttacker: %i (%s)", event->GetInt("attacker", 0), (event->GetInt("attacker", 0) == gPlayerVars.iUserId) ? "Me" : "Not me");
			gBaseAPI.LogToFile("\tVictim: %i (%s)", event->GetInt("userid", 0), (event->GetInt("userid", 0) == gPlayerVars.iUserId) ? "Me" : "Not me");
			gBaseAPI.LogToFile("\tCustom Event: %i", event->GetInt("customkill", 0));
			gBaseAPI.LogToFile("\tDamage Flags: 0x%08X", event->GetInt("damagebits", 0));
			gBaseAPI.LogToFile("\tDeath Flags: 0x%04X", (short)event->GetInt("death_flags", 0));
			gBaseAPI.LogToFile("\tWeapon: %s (%i)[%s]", event->GetString("weapon"), event->GetInt("weaponid", 0), event->GetString("weapon_logclassname"));
		}
#endif

		if (gPlayerVars.iUserId == 0) //CBasePlayer::GetUserId() You can get this from the player_info_t struct as well.
			return;

		//Check if the user has switched servers since the last call.
		if (lastUserId != gPlayerVars.iUserId)
		{
			Reset();
			lastUserId = gPlayerVars.iUserId;
		}

		if (!strcmp(event->GetName(), "player_death"))
		{
			int attacker = event->GetInt("attacker", 0);
			int userId = event->GetInt("userid", 0);
			if (attacker == gPlayerVars.iUserId)
			{
				if (attacker == userId) //Suicide
					return;

				bool isEventProper = IsEventProperForKillCounter(event);
				if (isEventProper)
					killCounter++;
				killStreakCounter++;
				if ((gInts.Globals->curtime - lastKillTime) < killStreakTimeout)
				{
					//This is done to prevent getting spammed with killstreak sounds when killing like 4 people at once with a crit sticky, so it will play the most recent sound when Think is called.
					switch (killStreakCounter)
					{
					case 2:
						strcpy_s(chKillstreakSound, "UT2k4/Double_Kill.wav");
						break;
					case 3:
						strcpy_s(chKillstreakSound, "Quake3/quake_tripplekill.wav");
						break;
					case 4:
						strcpy_s(chKillstreakSound, "UT99/multikill.wav");
						break;
					case 5:
						strcpy_s(chKillstreakSound, "UT2k4/MegaKill.wav");
						break;
					case 6:
						strcpy_s(chKillstreakSound, "UT99/ultrakill.wav");
						break;
					case 7:
						strcpy_s(chKillstreakSound, "UT99/monsterkill.wav");
						break;
					case 8:
						strcpy_s(chKillstreakSound, "UT2k4/LudicrousKill.wav");
						break;
					default: //9 or more kills in a row, play 'Holy Shit!' every time.
						strcpy_s(chKillstreakSound, "UT2k4/HolyShit.wav");
					}
				}
				else
				{
					//This scenario is when you kill someone outside the killStreakTimeout variable.
					killStreakCounter = 1;
				}
				switch (event->GetInt("customkill", 0))
				{
				case TF_CUSTOM_HEADSHOT:
				case TF_CUSTOM_HEADSHOT_DECAPITATION:
				case TF_CUSTOM_PENETRATE_HEADSHOT:
				{
					PlaySound("UT99/headshot.wav");
					break;
				}
				}
				int damagebits = event->GetInt("damagebits", 0);
				if (damagebits & DMG_SLASH || damagebits & DMG_CLUB)
				{
					PlaySound("Quake3/quake_humiliation.wav"); //A little quake never hurt nobody.
				}

				if (killCounter > 0)
				{
					switch (killCounter)
					{
#ifdef USE_TF2_KILLSTREAKS
					case 5:
						PlaySound("UT99/killingspree.wav");
						break;
					case 10:
						PlaySound("UT99/unstoppable.wav");
						break;
					case 15:
						PlaySound("UT99/rampage.wav");
						break;
					default: //If the player gets a 20 or more killstreak, just play Godlike every 5 kills.
						if (killCounter % 5 == 0)
							PlaySound("UT99/godlike.wav");
#else
					case 5:
						PlaySound("UT99/killingspree.wav");
						break;
					case 10:
						PlaySound("UT99/rampage.wav");
						break;
					case 15:
						PlaySound("UT2k4/Dominating.wav");
						break;
					case 20:
						PlaySound("UT99/unstoppable.wav");
						break;
					default: //If the player gets a 25 or more killstreak, just play Godlike every 5 kills.
						if (killCounter % 5 == 0)
							PlaySound("UT99/godlike.wav");
#endif
					}
				}
				if (isEventProper)
					lastKillTime = gInts.Globals->curtime;
			}
			return;
		}
		if (!strcmp(event->GetName(), "player_spawn")) //This will also get called when the player changes class.
		{
			if (event->GetInt("userid", 0) == gPlayerVars.iUserId)
			{
				Reset();
			}
			return;
		}
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed FireGameEvent");
	}
}
//=================================================================================
void CAnnouncer::Think(void)
{
	try
	{
		if (strcmp(chKillstreakSound, "")) //Check if the killstreak has something queued.
		{
			PlaySound(chKillstreakSound);
			strcpy_s(chKillstreakSound, ""); //Once done, zero out the chKillstreakSound member.
		}
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed CAnnouncer::think");
	}
}
//=================================================================================
void CAnnouncer::Render(void)
{
	try
	{
#ifdef _DEBUG
		gDrawManager->DrawString(500, 200, 0xFFFFFFEE, "Kills: %i Killstreak: %i Timeout: %f", killCounter, killStreakCounter, gInts.Globals->curtime - killStreakTimeout);
#endif

		CBaseEntity* pLocalPlayer = gInts.EntList->GetClientEntity(me);

		if (killCounter < 5 || pLocalPlayer == NULL)
			return;

		wchar_t chName[40];
		const char* chGetPlayerName = pLocalPlayer->GetPlayerName();
		if (chGetPlayerName != NULL)
		{
			MultiByteToWideChar(CP_UTF8, 0, chGetPlayerName, 32, chName, 40);
		}
		wchar_t chKillStreakMessage[15] = L"";
		wchar_t chKillStreak[30] = L"";

		static int lastFive = 0;
		static float last5KillTime = 0.0f;

		if (killCounter % 5 == 0)
		{
			lastFive = killCounter;
			last5KillTime = lastKillTime;
		}

		//We want to check this twice in Render to prevent a leave/join false display.
		if (lastUserId != gPlayerVars.iUserId)
		{
			Reset();
			lastUserId = gPlayerVars.iUserId;
			return;
		}

#ifdef USE_TF2_KILLSTREAK_HUD_DISPLAY_METHOD
		int killCounterTemp = lastFive;
#else
		int killCounterTemp = killCounter;
#endif

		float killDelta = gInts.Globals->curtime - last5KillTime;

		DWORD killStreakColor;
		float killFadeOffset = 4.0;

#ifdef USE_TF2_KILLSTREAKS
		if (killCounter < 10)
		{
			wsprintfW(chKillStreak, L" Killing Spree %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is on a ");
			killStreakColor = 0x5A833CFF;
		}
		else if (killCounter < 15)
		{
			wsprintfW(chKillStreak, L" Unstoppable %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is ");
			killStreakColor = 0x9A542FFF;
			killFadeOffset = 4.4f;
		}
		else if (killCounter < 20)
		{
			wsprintfW(chKillStreak, L" Rampage %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is on a ");
			killStreakColor = 0x633D7DFF;
			killFadeOffset = 4.7f;
		}
		else if (killCounter < 25)
		{
			wsprintfW(chKillStreak, L" Godlike %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is ");
			killStreakColor = 0xB89C16FF;
			killFadeOffset = 5.5f;
		}
		else
		{
			wsprintfW(chKillStreak, L" Godlike %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is still ");
			killStreakColor = 0xB89C16FF;
			killFadeOffset = 5.9f;
		}
#else
		if (killCounter < 10)
		{
			wsprintfW(chKillStreak, L" Killing Spree %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is on a ");
			killStreakColor = 0x5A833CFF;
		}
		else if (killCounter < 15)
		{
			wsprintfW(chKillStreak, L" Rampage %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is on a ");
			killStreakColor = 0x7F6A00FF;
			killFadeOffset = 4.5f;
		}
		else if (killCounter < 20)
		{
			wsprintfW(chKillStreak, L" Dominating %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is ");
			killStreakColor = 0xB89C16FF;
			killFadeOffset = 5.0f;
		}
		else if (killCounter < 25)
		{
			wsprintfW(chKillStreak, L" Unstoppable %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is ");
			killStreakColor = 0x9A542FFF;
			killFadeOffset = 5.9f;
		}
		else if (killCounter < 30)
		{
			wsprintfW(chKillStreak, L" Godlike %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is ");
			killStreakColor = 0xB89C16FF;
			killFadeOffset = 5.9f;
		}
		else
		{
			wsprintfW(chKillStreak, L" Godlike %i", killCounterTemp);
			wcscpy_s(chKillStreakMessage, L" is still ");
			killStreakColor = 0xB89C16FF;
			killFadeOffset = 5.9f;
		}
#endif
		if (killDelta > killFadeOffset)
			return;

		killDelta = killFadeOffset - killDelta;

		DWORD nameColor = (gPlayerVars.iTeamNum == TEAM_BLUE) ? TF2_BLUE_TEAM : TF2_RED_TEAM;
		DWORD killStreakMessageColor = 0xABA493FF;

		if (killDelta < 2.0)
		{
			BYTE alphaChannel = (BYTE)(255 * killDelta / 2.0f);
			nameColor = (nameColor & 0xFFFFFF00) | alphaChannel;
			killStreakMessageColor = (killStreakMessageColor & 0xFFFFFF00) | alphaChannel;
			killStreakColor = (killStreakColor & 0xFFFFFF00) | alphaChannel;
		}

		int width = gDrawManager->GetScreenSizeWidth() / 2;
		int drawHeight = gDrawManager->GetScreenSizeHeight() / 8;
		int playerNameWidth = gDrawManager->GetPixelTextSize(chName);
		int killStreakMessageWidth = gDrawManager->GetPixelTextSize(chKillStreakMessage);

		wchar_t chTotalMessage[69];
		wsprintfW(chTotalMessage, L"%s%s%s", chName, chKillStreakMessage, chKillStreak);
		int totalWidth = gDrawManager->GetPixelTextSize(chTotalMessage);
		int drawWidth = width - (totalWidth / 2);

		gDrawManager->DrawStringActual(drawWidth, drawHeight, nameColor, chName);
		drawWidth += playerNameWidth;
		gDrawManager->DrawStringActual(drawWidth, drawHeight, killStreakMessageColor, chKillStreakMessage);
		drawWidth += killStreakMessageWidth;
		wchar_t chKillstreakMessage[69];
		wsprintfW(chKillstreakMessage, chKillStreak, killCounter);
		gDrawManager->DrawStringActual(drawWidth, drawHeight, killStreakColor, chKillstreakMessage);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed Render");
	}
}
//=================================================================================
void CAnnouncer::PlaySound(const char* soundName)
{
	try
	{
		gInts.Surface->PlaySound(soundName);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed PlaySound");
	}
}
//=================================================================================
void CAnnouncer::Reset(void)
{
	killCounter = 0;
	lastKillTime = 0.0f;
	#ifdef _DEBUG
	//gBaseAPI.LogToFile("CAnnouncer Reset");
	#endif
}