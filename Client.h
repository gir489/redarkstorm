#pragma once

#include "SDK.h"

CUserCmd* __fastcall Hooked_GetUserCmd(PVOID pInput, int edx, int sequence_number);
void __fastcall Hooked_HLClientCreateMove(PVOID pClient, int edx, int sequence_number, float input_sample_frametime, bool active);
bool __fastcall Hooked_ClientModeCreateMove(PVOID ClientMode, int edx, float input_sample_frametime, CUserCmd* pCommand);
int __fastcall Hooked_IN_KeyEvent(PVOID pClient, int edx, int eventcode, int keynum, const char *pszCurrentBinding);
bool __fastcall Hooked_IsPlayingBack(PVOID DemoPlayer, int edx);
void __fastcall Hooked_HudUpdate(PVOID ClientMode, int edx, bool active);