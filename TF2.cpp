﻿#include "SDK.h"
#include "Client.h"
#include "COffsets.h"
#include "CAimbot.h"
//===================================================================================
CMiscFuncs gMiscFuncs;
//===================================================================================
bool CMiscFuncs::bIsKey(int iKey)
{
static int iKeys[] =
			{
				0, //Off
				VK_LBUTTON, //Left Mouse
				VK_RBUTTON, //Right Mouse
				VK_MBUTTON, //Scroll wheel button press
				VK_XBUTTON1, //Mouse Back
				VK_XBUTTON2, //Mouse Forward
				VK_LSHIFT, //Left Shift
				VK_RSHIFT, //Right Shift
				VK_LCONTROL, //Left Control
				VK_TAB, //TAB
				0x46, //F
				0x47, //G
				VK_F12, //F12
				VK_LMENU, //LALT
				VK_RMENU, //RALT
				VK_RCONTROL, //Right Control
				0, //Always On
			};
	return ( gPlayerVars.bIsRageMode || iKey == 17 || bIsKeyPressed(iKeys[iKey]) );
}
//===================================================================================
bool CMiscFuncs::bIsKeyPressed( int vk )
{
	return (GetAsyncKeyState(vk) & 0x8000);
}
//===================================================================================
bool CMiscFuncs::bIsStreamWeapon( int iWeaponID )
{
	switch(iWeaponID)
	{
		case WPN_EngineerPistol:
		case WPN_ScoutPistol:
		case WPN_NewPistol:
		case WPN_SMG:
		case WPN_NewSMG:
		case WPN_Minigun:
		case WPN_NewMinigun:
		case WPN_FestiveMinigun:
		case WPN_Lugermorph:
		case WPN_Lugermorph2:
		case WPN_Natascha:
		case WPN_Degreaser:
		case WPN_Flamethrower:
		case WPN_NewFlamethrower:
		case WPN_FestiveFlamethrower:
		case WPN_IronCurtain:
		case WPN_Shortstop:
		case WPN_BrassBeast:
		case WPN_Tomislav:
		case WPN_Winger:
		case WPN_SyringeGun:
		case WPN_NewSyringeGun:
		case WPN_Blutsauger:
		case WPN_Overdose:
		case WPN_PocketPistol:
		case WPN_CleanersCarbine:
			return true;
		default:
			return false;
	}
}
//===================================================================================
bool CMiscFuncs::bIsNoCrits ( int iWeaponID )
{
	switch(iWeaponID)
	{
		case WPN_Gunslinger:
		case WPN_SouthernHospitality:
		case WPN_Sword:
		case WPN_Headless:
		case WPN_MarketGardener:
		case WPN_Ullapool:
		case WPN_Claidheamhmor:
		case WPN_PersainPersuader:
		case WPN_Golfclub:
		case WPN_NeonAnnihilator1:
		case WPN_NeonAnnihilator2:
		case WPN_Katana:
			return true;
		default:
			return false;
	}
}
//===================================================================================
bool CMiscFuncs::bFiresProjectile()
{
	switch (gPlayerVars.iClass)
	{
		case TF2_Soldier:
		{
			return (gPlayerVars.iWeaponSlot == 0);
		}
		case TF2_Demoman:
		{
			return (gPlayerVars.iWeaponSlot != 2);
		}
	}
	return false;
}
//===================================================================================
//bool CMiscFuncs::bPlayerHasCrocSet( CBaseEntity* pBaseEntity )
//{
//	return (gOffsets.iGetPlayerClass(pBaseEntity) == TF2_Sniper && 
//			iGetItemBySlotIndex(pBaseEntity, 7) == HAT_Snaggletooth && 
//			iGetItemBySlotIndex(pBaseEntity, 0) == WPN_SydneySleeper && 
//			iGetItemBySlotIndex(pBaseEntity, 1) == WPN_DarwinDangerShield && 
//			iGetItemBySlotIndex(pBaseEntity, 2) == WPN_Bushwacka 
//			);
//}
//===================================================================================
bool CMiscFuncs::bisProjectileAirblastable( CBaseEntity* pProj )
{
	int iLine = __LINE__;
	try
	{
		if (pProj != NULL && pProj->GetModel() != NULL )
		{
			Vector center = pProj->GetAbsOrigin();


			iLine = __LINE__;

		
			Vector vecSpeed;
			gMiscFuncs.EstimateAbsVelocity(pProj,vecSpeed);


			iLine = __LINE__;


			center.x=center.x+vecSpeed.x*gAimbot.GetInterp();
			center.y=center.y+vecSpeed.y*gAimbot.GetInterp();
			center.z=center.z+vecSpeed.z*gAimbot.GetInterp();


			iLine = __LINE__;


			float flDistance = gAimbot.flGetDistance( center );

			if (CV( aim_airblast ))
			{
				if (flDistance < 20)
				{
					gAimbot.AimAtVector(pProj->GetAbsOrigin());
				}
			}


			iLine = __LINE__;

			if ( ( flDistance < 13 ) && ( gAimbot.flGetFOV( center ) < 50 ) )
			{
				Ray_t ray;
				trace_t pTrace;
				TraceFilter filter;

				ray.Init( gPlayerVars.vecEyePos, pProj->GetAbsOrigin() );

				gInts.Trace->TraceRay( ray, MASK_TF2, &filter, &pTrace );

				iLine = __LINE__;


				if (pTrace.m_pEnt == NULL)
					return true;
	
				if ( pTrace.m_pEnt )
				{
					return (pTrace.m_pEnt == pProj);
				}

				return false;
			}
		}

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed bisProjectileAirblastable %s %i", pProj->GetClientClass()->chName, iLine);
	}

	return false;
}
//===================================================================================
bool CMiscFuncs::bAirblastFrame( void )
{
	CBaseEntity* pLocalBaseEntity = GetBaseEntity(me);

	if( pLocalBaseEntity == NULL )
		return false;

	for ( int iIndex = 1; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++ )
	{
		CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

		if (pBaseEntity == NULL)
			continue;

		if (pBaseEntity->IsDormant())
			continue;

		if (gOffsets.iGetTeamNum(pBaseEntity) == gPlayerVars.iTeamNum )
			continue;

		int classID = pBaseEntity->GetClientClass()->iClassID;

		if (CV(aim_airblast))
		{
			if ( classID == gEntIDs.iFlareID)
			{
				continue;
			}
		}

		if ( classID == gEntIDs.iRocketID )
		{
			if (gOffsets.bIsRocketJumperRocket(pBaseEntity))
				continue;

			if (bisProjectileAirblastable(pBaseEntity))
				return true;

			continue;
		}

		if (classID == gEntIDs.iSentryRocketID				||
			classID == gEntIDs.iJarateID					||
			classID == gEntIDs.iMilkID						||
			classID == gEntIDs.iEnergyBall					||
			classID == gEntIDs.iFlareID
			)
		{
			if (bisProjectileAirblastable(pBaseEntity))
				return true;

				continue;
		}
		if (classID == gEntIDs.iArrowID || classID == gEntIDs.iMedicArrowID )
		{
			Vector vecVelocity;
			EstimateAbsVelocity(pBaseEntity, vecVelocity);
			if ( vecVelocity.IsZero() == false )
			{
				if (bisProjectileAirblastable(pBaseEntity))
					return true;
			}
			continue;
		}
		if (classID == gEntIDs.iBaseballID || classID == gEntIDs.iOrnamentID || classID == gEntIDs.iCleverID  )
		{
			if (gOffsets.bIsTouched(pBaseEntity) == 0)
			{
				if (bisProjectileAirblastable(pBaseEntity))
					return true;
			}
			continue;
		}

		if (classID == gEntIDs.iCTFPlayerID)
		{
			if (gOffsets.iGetPlayerCond(pBaseEntity) & TFCond_Charging)
			{
				if (bisProjectileAirblastable(pBaseEntity))
					return true;
			}
			continue;
		}

		if (classID == gEntIDs.iGrenadeID)
		{
			if (gOffsets.iGetStickyType(pBaseEntity) != 2 )
			{
				if (bisProjectileAirblastable(pBaseEntity))
					return true;
			}
			continue;
		}
	}
	return false;
}
//===================================================================================
void CMiscFuncs::RunAutoStickyFrame( CUserCmd* pCommand )
{

	//iLine is used in the catch block.
	int iLine;
	try
	{

		//Grab the local player's entity pointer.
		CBaseEntity* pLocalEntity = GetBaseEntity(me);

		//Check for if the entity was destoried or is no longer valid.
		if ( pLocalEntity == NULL )
			return;

		iLine = __LINE__;
		//If the player has a shield, they obviously don't have a sticky launcher, return.
		if ( gOffsets.bHasShield( pLocalEntity ) )
			return;

		//Grab the player's weapon pointer in slot 2.
		CBaseEntity *pBaseWeapon = gOffsets.pGetBaseCombatWeapon(pLocalEntity, 1);

		if ( pBaseWeapon == NULL || //If the weapon was destoried or is no longer valid.
			gOffsets.iGetPipebombCount(pBaseWeapon) == 0 || //Or if the player doesn't have any stickies out
			gOffsets.iGetItemIndex(pBaseWeapon) == WPN_StickyJumper ) //Or if the player has a sticky jumper
		{
			return;
		}

		iLine = __LINE__;
		//Loop through stickies first.
		for ( int iStickyIndex = gInts.Globals->maxclients + 1;
				  iStickyIndex <= gInts.EntList->GetHighestEntityIndex();
				  iStickyIndex++ )
		{
			//Grab entity pointer from looped index.
			CBaseEntity* pSticky = GetBaseEntity(iStickyIndex);

			//Check for if the entity was destoried or is no longer valid.
			if (pSticky == NULL)
			{
				continue;
			}

			iLine = __LINE__;
			//Check to make sure the entity is a type of baseclass CProjectileGreande.
			if (pSticky->GetClientClass()->iClassID == gEntIDs.iGrenadeID)
			{
				//Check to make sure the local player threw it.
				CBaseEntity* pStickyThrower = gOffsets.GetThrowerEntity(pSticky);
				if (pStickyThrower != NULL && pStickyThrower->GetIndex() == gInts.Engine->GetLocalPlayer())
				{
					//Check to make sure it's a sticky that does damage.
					if ( gOffsets.iGetStickyType(pSticky) == 1 )
					{
						iLine = __LINE__;
						//Grab the result of timeGetTime and store it in a variable.
						float curtime = gInts.Globals->curtime;
						if ( gOffsets.GetSpawnTime(pSticky) == NULL )
						{
							//Store the result inside the entity.
							//This will ensure that when the entity is destroyed, the start time is destroyed with it.
							gOffsets.SetSpawnTime(pSticky, curtime); //CBaseEntity::m_flSpawnTime
						}
					}
					else
					{
						return;
					}

					//If it's a sticky that can do damage, has spawned and it's from us, look for targets.
					for ( int iIndex = 1; iIndex <= gInts.Globals->maxclients; iIndex++ )
					{
						//Grab entity pointer from looped index.
						CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

						//Check for if the entity was destoried or is no longer valid.
						if ( pBaseEntity == NULL )
						{
							continue;
						}

						//Check to make sure it's a target we want to detonate the sticky for.
						if ( gAimbot.bStateAndFriendCheck(pBaseEntity) == false || //Don't want to detonate on ubercharged or friends.
							pBaseEntity->IsDormant() || //Dormant means the entity information is obsolete and we wouldn't want to deonate on such information.
							gAimbot.bCheckTeam(iIndex) == false ) //Make sure we don't waste stickies on teammates.
						{
							continue;
						}
						iLine = __LINE__;
						float iDetTime = .800;

						//If the m_bDefensiveBomb netvar is set, use a different detonation time.
						if (gOffsets.bIsScottishResistanceBomb(pSticky))
						{
							iDetTime = 1.600;
						}
						iLine = __LINE__;
						//Check to make sure the sticky was out long enough to detonate it.
						if ( ( gInts.Globals->curtime - gOffsets.GetSpawnTime(pSticky) ) >= iDetTime )
						{
							
							iLine = __LINE__;
							//If the sticky can see the player, detonate the sticky.
							if (gAimbot.checkStickyVis(pSticky, pBaseEntity))
							{
								iLine = __LINE__;

								if ( gMiscFuncs.bIsKey( CV( triggerbot ) ) )
								{
									iLine = __LINE__;

									//If the m_bDefensiveBomb netvar is set, the player has to be aiming at the sticky.
									if (gOffsets.bIsScottishResistanceBomb(pSticky))
									{
										iLine = __LINE__;
										//Silently aim at the sticky.
										gAimbot.AimAtSticky(pSticky, pCommand);
									}
									pCommand->buttons |= IN_ATTACK2;
								}
							}
						}
					}
				}
			}
		}
		return;

	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed CAutoStickyFrame %i", iLine );
		return;
	}

}
//===================================================================================
bool CMiscFuncs::bIsBadIndex( int iIndex )
{
	if( iIndex == me )
		return true;

	CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

	if( gMiscFuncs.bIsValidPlayer(pBaseEntity) == false )
		return true;

	if ( gOffsets.iGetTeamNum(pBaseEntity) != gPlayerVars.iTeamNum || gOffsets.iGetTeamNum(pBaseEntity) < 2 )
		return true;

	if ( gMiscFuncs.bHasFriend(pBaseEntity) )
		return true;

	const char* chName = pBaseEntity->GetPlayerName();

	int iLength = strlen(chName);
	
	if ( iLength > 28 )
		return true;

	sprintf_s(gPlayerVars.chName, "%sâ€Ž", chName);
	gConVars.name->SetValue("0");
	return false;
}
//===================================================================================
bool CMiscFuncs::bIsDueleing( CBaseEntity *pBaseEntity )
{
	try
	{
		typedef bool(__cdecl *IsDuelingPlayerFn)(CBaseEntity*);
		static IsDuelingPlayerFn CallIsDuelingLocalPlayer = (IsDuelingPlayerFn)gOffsets.dwIsDuelingLocalPlayerFn;

		return CallIsDuelingLocalPlayer(pBaseEntity);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed bIsDueleing");
		return false;
	}
}
//===================================================================================
bool CMiscFuncs::bIsCritBoosted( CBaseEntity *pBaseEntity )
{
	typedef bool ( __thiscall *CritBoostedFn )( PVOID );
	static CritBoostedFn CallIsCritBoosted = (CritBoostedFn)gOffsets.dwCritBoostedFn;

	return CallIsCritBoosted( ( PVOID )( gOffsets.dwGetCritBoostedAddress(pBaseEntity) ) );
}
//===================================================================================
bool CMiscFuncs::bHasFriend( CBaseEntity *pBaseEntity )
{
	try
	{
		if (pBaseEntity == NULL)
			return false;

		if (gOffsets.bIsBotPlayer(pBaseEntity))
			return false;

		if (CV(aim_rage) == 2 && CV(esp_rage) == pBaseEntity->GetIndex())
			return true;

		CSteamID steamId;
		pBaseEntity->GetSteamID(&steamId);
		return gInts.Friends->HasFriend(steamId);
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed bHasFriend");
		return false;
	}
}
//===================================================================================
bool CMiscFuncs::bIsValidPlayer( int iIndex )
{
	if ( iIndex == 0 || iIndex > gInts.Globals->maxclients )
		return false;

	return bIsValidPlayer(GetBaseEntity(iIndex));
}
//===================================================================================
bool CMiscFuncs::bIsValidPlayer( CBaseEntity* pBaseEntity )
{
	if ( pBaseEntity == NULL )
		return false;
	
	return ( pBaseEntity->GetClientClass()->iClassID == gEntIDs.iCTFPlayerID );
}
//===================================================================================
bool CMiscFuncs::bIsLocalPlayerInDuel()
{
	try
	{
		for (int iCounter = 0; iCounter <= gInts.Globals->maxclients; iCounter++)
		{
			if (iCounter == me)
			{
				continue;
			}
		
			CBaseEntity* pEntity = GetBaseEntity(iCounter);
			if (!bIsValidPlayer(pEntity))
			{
				continue;
			}

			if (bIsDueleing(pEntity))
			{
				return true;
			}
		}
		return false;
	}
	catch (...)
	{
		gBaseAPI.LogToFile("Failed bIsLocalPlayerInDuel");
		return false;
	}
}
//===================================================================================
float CMiscFuncs::GetLerpAmount( void ) 
{
	typedef float ( __cdecl *GetLerpFn )( ); 
	static GetLerpFn GetLerp = (GetLerpFn)gOffsets.dwGetLerpFn;

	return GetLerp( );
}
//===================================================================================
float CMiscFuncs::RandomFloat( float flMinVal, float flMaxVal )
{
	typedef float ( __cdecl *RandomFloatFn )( float, float );
	static RandomFloatFn RandomFloatCall = ( RandomFloatFn )gOffsets.dwRandomFloat;

	return RandomFloatCall( flMinVal, flMaxVal );
}
//===================================================================================
float CMiscFuncs::GetReloadTime()
{
	if ( gPlayerVars.pBaseWeapon == NULL )
		return 0.0f;

	if ( gPlayerVars.iClipMax == 0 )
		return 0.0f;

	if ( gPlayerVars.iClipMax == gPlayerVars.iClip )
		return 0.0f;

	float flLastFireTime = gOffsets.flGetLastFireTime(gPlayerVars.pBaseWeapon);
	float flReloadPriorNextFire = gOffsets.flGetReloadPriorNextFire(gPlayerVars.pBaseWeapon);

	if ( flLastFireTime > flReloadPriorNextFire )
		return 0.0f;

	return (gInts.Globals->curtime - flReloadPriorNextFire);
}
//===================================================================================
void CMiscFuncs::RandomSeed( int iSeed )
{
	typedef void ( __cdecl *RandomSeedFn )( int );
	static RandomSeedFn RandomSeed = ( RandomSeedFn )gOffsets.dwRandomSeed;

	RandomSeed( iSeed );
}
//===================================================================================
void CMiscFuncs::EstimateAbsVelocity( CBaseEntity* pBaseEntity, Vector& vecVelocity )
{
	typedef void (__thiscall *EstimateAbsVelocityFn)( CBaseEntity*, Vector& vel );
	static EstimateAbsVelocityFn CallEstimateAbsVelocity = (EstimateAbsVelocityFn)gOffsets.dwEstAbsVelocity;

	CallEstimateAbsVelocity( pBaseEntity, vecVelocity );
}
//============================================================================================
float Clamp(float v, float mmin, float mmax)
{
	if (v > mmax) return mmax;
	if (v < mmin) return mmin;

	return v;
}
//===================================================================================
void CMiscFuncs::DoBunnyHop( CUserCmd* pCommand, CBaseEntity* pBaseEntity ) 
{
	static bool bJumpReleased;

	if ( pCommand->buttons & IN_JUMP )
	{
		if (bJumpReleased == false)
		{
			if ( (gOffsets.iGetWaterLevel(pBaseEntity) & 0x2) == 0 )
			{
				pCommand->buttons &= ~IN_JUMP;
				if ( (gPlayerVars.iFlags & FL_ONGROUND) )
				{
					if ( gPlayerVars.iClass == TF2_Scout )
					{
						if (gInts.Globals->curtime - gOffsets.GetSpawnTime(pBaseEntity) > .025)
						{
							pCommand->buttons |= IN_JUMP;
							gOffsets.SetSpawnTime(pBaseEntity, gInts.Globals->curtime);
						}
					}
					else
					{
						pCommand->buttons |= IN_JUMP;
					}
				}
			}
		}
		else
		{
			bJumpReleased = false;
		}
	}
	else if (bJumpReleased == false)
	{
		bJumpReleased = true;
	}
}
//===================================================================================
void CMiscFuncs::Spinbot(CUserCmd* pCommand, bool bSpinX)
{
	if (pCommand->buttons & IN_ATTACK)
		return;

	Vector vecViewAngles;

	gInts.Engine->GetViewAngles(vecViewAngles);

	float fTime = gInts.Engine->Time();
	if (bSpinX)
		pCommand->viewangles.x = (float)(fmod(fTime / 0.1f * 180.0f, 180.0f));
	pCommand->viewangles.y = (float)(fmod(fTime / 0.1f * 360, 360));

	gAimbot.ClampAngle(pCommand->viewangles);

	gAimbot.SilentAimFix(pCommand, vecViewAngles);
}
//=================================================================================== 
int CMiscFuncs::RandomInt( int iMin, int iMax )
{
	typedef int ( __cdecl *RandomIntTm )( int, int );
	static RandomIntTm RandomIntFn = ( RandomIntTm )gOffsets.dwRandomInt;

	return RandomIntFn( iMin, iMax );
}
//===================================================================================
int CMiscFuncs::iGetItemBySlotIndex( CBaseEntity* pBaseEntity, int iIndex )
{
	typedef DWORD* ( __cdecl *ThisFn )( PVOID, int, int );
	static ThisFn function = (ThisFn)gOffsets.dwGetSlotItem;

	DWORD* dwItemLocation = function(pBaseEntity, iIndex, 0);

	if ( dwItemLocation == NULL )
		return NULL;

	return *(PINT)((DWORD)dwItemLocation+0x10);
}
//===================================================================================
unsigned int CMiscFuncs::MD5_PseudoRandom( unsigned int sequence_number ) 
{
	typedef unsigned int ( __cdecl *MD5PesudoRandomFn )( unsigned int );
	static MD5PesudoRandomFn MD5PesudoRandom = (MD5PesudoRandomFn)gOffsets.dwMD5PseudoRandom;

	return MD5PesudoRandom(sequence_number);
}
//===================================================================================
IGameResources* CMiscFuncs::GetGameResources( void )
{
	typedef IGameResources* ( __stdcall* GetGameResources_t )( void );
	static GetGameResources_t GetGameResourcesCall = (GetGameResources_t)gOffsets.dwGetGameResources;

	return GetGameResourcesCall();
}
//=================================================================================
const char* CBaseEntity::GetPlayerName( )
{
	try 
	{
		IGameResources* gameResources = gMiscFuncs.GetGameResources();
		return gameResources->GetPlayerName(this->GetIndex());
	}
	catch (...)
	{
		return "";
	}
}
//=================================================================================
DWORD CMiscFuncs::dwGetCallFunctionLocation(DWORD instructionPointer ) 
{
	DWORD jumpMask = instructionPointer + 1;
	return ((*(PDWORD)(jumpMask)) + jumpMask+4);
}
//=================================================================================
PVOID CMiscFuncs::InitKeyValue(void)
{
	typedef PDWORD(__cdecl* Init_t)(int);
	static Init_t InitKeyValues = (Init_t)gOffsets.dwWellGoodInitM8;

	return InitKeyValues(32);
}
//=================================================================================
void CMiscFuncs::ChangePlayerName(const char* chName)
{
	static auto NET_SetConVar = reinterpret_cast<BYTE * (__thiscall*)(void*, const char*, const char*)>(gBaseAPI.GetEngineSignature("55 8B EC 81 EC ? ? ? ? 56 57 8B F9 68")); // Search for Custom user info value 
	uint32_t temp_mem[50];
	NET_SetConVar(&temp_mem, "name", chName);
	if(gInts.Engine->GetNetChannelInfo())
		gInts.Engine->GetNetChannelInfo()->SendNetMsg(&temp_mem);
}
//=================================================================================
void CMiscFuncs::SetName(PVOID kv)
{
	char chCommand[30] = "use_action_slot_item_server";
	typedef int(__cdecl* HashFunc_t)(const char*, bool);
	static HashFunc_t s_pfGetSymbolForString = (HashFunc_t)gOffsets.dwSetKeyValuesName;
	*(PDWORD)((DWORD)kv + 0x4) = 0;
	*(PDWORD)((DWORD)kv + 0x8) = 0;
	*(PDWORD)((DWORD)kv + 0xC) = 0;
	*(PDWORD)((DWORD)kv + 0x10) = 0x10000;
	*(PDWORD)((DWORD)kv + 0x14) = 0;
	*(PDWORD)((DWORD)kv + 0x18) = 0; //Extra one the game isn't doing, but if you don't zero this out, the game crashes.
	*(PDWORD)((DWORD)kv + 0x1C) = 0;
	*(PDWORD)((DWORD)kv + 0) = s_pfGetSymbolForString(chCommand, 1);
}
//=================================================================================
void CMiscFuncs::DoNoiseMaker(void)
{
	PVOID kv = InitKeyValue();
	if (kv != NULL)
	{
		SetName(kv);
		gInts.Engine->ServerCmdKeyValues(kv);
	}
}
//=================================================================================
void new_BaseViewModel_flPlaybackRate(const CRecvProxyData *pData, void *pStruct, void *pOut)
{
	if (gPlayerVars.iClass == TF2_Pyro)
	{
		*(PFLOAT)pOut = 1.0f;
	}
	else
	{
		*(PFLOAT)pOut = pData->m_Value.m_Float;
	}
}
//=================================================================================
int NewShowScoresOnCallback(int param1)
{
	gPlayerVars.bInScore = true;
	typedef int(__cdecl *OriginalFn)(int);
	static OriginalFn ShowScoresOn = (OriginalFn)gOffsets.dwShowScoresOn;
	return ShowScoresOn(param1);
}
//=================================================================================
int NewShowScoresOffCallback(int param1)
{
	gPlayerVars.bInScore = false;
	typedef int(__cdecl *OriginalFn)(int);
	static OriginalFn ShowScoresOff = (OriginalFn)gOffsets.dwShowScoresOff;
	return ShowScoresOff(param1);
}
//=================================================================================
void NewQuitCallback(void)
{
	gInts.Engine->ClientCmd_Unrestricted("exit");
}
//=================================================================================
void NewExitCallback(void)
{
	gPlayerVars.bIsQuitting = true;
	typedef bool(__cdecl *ExitFunc)(void);
	static ExitFunc Exit = (ExitFunc)gOffsets.dwExit;
	Exit();
}