#pragma once
//===================================================================================
#include "SDK.h"
//===================================================================================
class CRadar
{
public:
	void DrawRadarBack( );
	void DrawRadarBlip( CBaseEntity *pBaseEntity, DWORD dwTeamColor );
};
//===================================================================================
extern CRadar gRadar;